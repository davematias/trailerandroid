package com.grubot.trailerview.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.presenters.ISearchScreenPresenter;
import com.grubot.trailerview.presenters.SearchScreenPresenter;
import com.grubot.trailerview.ui.adapters.ContentDataAdapter;
import com.grubot.trailerview.utils.Animations;
import com.grubot.trailerview.utils.UINotifications;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment implements ISearchView, IMovieView {

    @Bind(R.id.ivSearchPlaceholder)
    ImageView ivSearchPlaceholder;

    @Bind(R.id.pbLoad)
    ProgressBar pbLoad;

    @Bind(R.id.pbReload)
    ProgressBar pbReload;

    @Bind(R.id.etSearch)
    EditText searchText;

    @Bind(R.id.resultsView)
    RecyclerView rView;

    @Bind(R.id.clContainer)
    CoordinatorLayout clContainer;

    @Bind(R.id.rlNoResults)
    RelativeLayout rlNoResults;

//    @Bind(R.id.rlNoResultsContainer)
//    RelativeLayout rlNoResultsContainer;

    @Bind(R.id.llSearch)
    LinearLayout llSearch;

    @Bind(R.id.llSearch2)
    LinearLayout llSearch2;

    @Bind(R.id.cbMovies)
    CheckBox cbMovies;

    @Bind(R.id.cbGames)
    CheckBox cbGames;

    @Bind(R.id.cbShows)
    CheckBox cbShows;

    private ContentDataAdapter mAdapter;

    private static final float HIDE_THRESHOLD = 30;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;

    private ISearchScreenPresenter presenter;
    Animation animation1;
    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        presenter = new SearchScreenPresenter(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        TrailerDen.getInstance().loadNativeAd();
        View v = inflater.inflate(R.layout.fragment_search, container, false);

        ButterKnife.bind(this, v);

        float distance = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 140,
                getResources().getDisplayMetrics()
        );

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rView.setLayoutManager(llm);

        rView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                switch (newState)
                {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        int qwe = 0;
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        int qwe2 = 0;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                //show views if first item is first visible position and views are hidden
                if (firstVisibleItem == 0) {
                    if(!controlsVisible) {
                        llSearch.animate().translationY(0).setInterpolator(new AccelerateInterpolator(2)).start();
                        controlsVisible = true;
                    }
                } else {
                    if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {

                        llSearch.animate().translationY(-distance).setInterpolator(new AccelerateInterpolator(2)).start();
                        controlsVisible = false;
                        scrolledDistance = 0;
                    } else if (scrolledDistance < -150 && !controlsVisible) {
                        llSearch.animate().translationY(0).setInterpolator(new AccelerateInterpolator(2)).start();
                        controlsVisible = true;
                        scrolledDistance = 0;
                    }
                }

                if((controlsVisible && dy>0) || (!controlsVisible && dy<0)) {
                    scrolledDistance += dy;
                }
            }
        });

        pbReload.setVisibility(View.VISIBLE);

        searchText.requestFocus();

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                showLoadingInterface();
            }
        });

        presenter.bindSearch(searchText);

        String types = TrailerDen.getInstance().getStringPreference(TrailerDen.PREFERENCE_SEARCH_TYPE_SELECTION);

        if(types == null || types.isEmpty())
        {
            types = "0,1,2";
        }

        cbMovies.setChecked(types.contains("0"));
        cbShows.setChecked(types.contains("1"));
        cbGames.setChecked(types.contains("2"));

        presenter.setSearchFilters(cbMovies.isChecked(),cbGames.isChecked(),cbShows.isChecked());

        cbGames.setOnCheckedChangeListener((e,b) -> changedTarget(cbGames));

        cbMovies.setOnCheckedChangeListener((e,b) -> changedTarget(cbMovies));

        cbShows.setOnCheckedChangeListener((e,b) -> changedTarget(cbShows));

        return v;
    }

    public void changedTarget(CheckBox changedCb)
    {
        if(!cbGames.isChecked() && !cbMovies.isChecked() && !cbShows.isChecked())
        {
            changedCb.setChecked(true);
        }

        List<String> newTypes = new ArrayList<>();

        if(cbMovies.isChecked())
            newTypes.add("0");

        if(cbShows.isChecked())
            newTypes.add("1");

        if(cbGames.isChecked())
            newTypes.add("2");

        TrailerDen.getInstance().saveStringPreference(TrailerDen.PREFERENCE_SEARCH_TYPE_SELECTION, TextUtils.join(",",newTypes));

        presenter.setSearchFilters(cbMovies.isChecked(),cbGames.isChecked(),cbShows.isChecked());
        searchText.setText(searchText.getText());

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume()
    {
        super.onResume();

    }

    @Override
    public void onDestroy()
    {
        presenter.destroySubscription();

        super.onDestroy();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void handleFavoriteError(Throwable error) {

        // TODO: 17/05/2016 add resources when implemented
        //UINotifications.showAsSnackbar(clContainer, "favorites error",Snackbar.LENGTH_LONG);

    }

    @Override
    public void handleSearchError(Throwable error)
    {
        pbLoad.setVisibility(View.INVISIBLE);
        if(mAdapter != null && mAdapter.getItemCount() > 0)
        {
            UINotifications.showAsSnackbar(clContainer, getString(R.string.searchError), Snackbar.LENGTH_LONG);
        }
        else
        {
            Animations.crossFade(rlNoResults,pbReload);
        }

    }

    @Override
    public void handleSearchSuccess(List<IVideoData> result)
    {
        if(result == null || result.size() == 0)
        {
            if(mAdapter != null && mAdapter.getItemCount() > 0)
            {
                rlNoResults.setVisibility(View.VISIBLE);
            }
            else
                Animations.crossFade(rlNoResults,pbReload);
        }
        else
        {
            rlNoResults.setVisibility(View.GONE);
            Movie header = new Movie();
            header.setTitle(TrailerDen.HEADERTITLE);

            result.add(0,header);

            if(TrailerDen.nativeAd != null) {

                if(result.size() < 4)
                {
                    result.add(result.size(), null);
                }

                for (int i = 4; i <= result.size(); i += 10) {
                    result.add(i, null);
                }
            }

            if(mAdapter == null || mAdapter.getItemCount() == 0)
            {
                Animations.crossFade(rView,pbReload);
            }
        }

        pbLoad.setVisibility(View.INVISIBLE);

        mAdapter = new ContentDataAdapter(getActivity(),result,this, ContentDataAdapter.MODE.SEARCH);

        rView.setAdapter(mAdapter);

    }

    @Override
    public void showLoadingInterface() {

        if(mAdapter != null && mAdapter.getItemCount() > 0)
        {
            pbLoad.setVisibility(View.VISIBLE);
        }
        else
        {
            if(rlNoResults.getVisibility() == View.VISIBLE)
                Animations.crossFade(pbReload,rlNoResults);
            else
                pbReload.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void updateContext(IVideoData videoContext, boolean isFavorite, boolean isCacheDirty) {

    }
}

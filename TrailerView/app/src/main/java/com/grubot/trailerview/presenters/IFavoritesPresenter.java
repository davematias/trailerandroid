package com.grubot.trailerview.presenters;

import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.models.IChangeObserved;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by dave on 16-03-2016.
 */
public interface IFavoritesPresenter {

    List<IVideoData> getFavorites(IChangeObserved observer);

    void destroySubscription();

    void syncUserData();

    void updateContext(IVideoData videoContext, boolean isFavorite, boolean isCacheDirty);

}

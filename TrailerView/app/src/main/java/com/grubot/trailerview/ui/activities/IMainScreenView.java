package com.grubot.trailerview.ui.activities;

import android.view.View;

import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Trailer;

import java.util.List;

/**
 * Created by dave on 25-02-2016.
 */
public interface IMainScreenView
{
    void handleSyncSuccess();

    void handleSyncError();

    void updateGenre(Genre genre, boolean filterOnTrailer);

    void GoogleSignIn();

    void GoogleSignOut();

    void handleTrailerData(Trailer result);

    void handleVideoData(IVideoData result);

    void handleAppLinkLoadError(Throwable t);

}

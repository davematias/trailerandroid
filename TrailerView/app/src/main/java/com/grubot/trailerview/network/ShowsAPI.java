package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.ShowCollection;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 08-01-2016.
 */
public interface ShowsAPI
{
    @GET("/shows/")
    Observable<ShowCollection> getShows(@Query("ordering") String sort);

    @GET("/shows/")
    Observable<ShowCollection> searchShows(@Query("search") String param,@Query("pageSize") int pageSize);

    @GET("/shows/")
    Observable<ShowCollection> filterShows(@Query("ordering") String sort, @Query("language") String language, @Query("country") String country, @Query("imdbID") String imdbID, @Query("slug") String slug);

    @GET("/shows/")
    Observable<ShowCollection> searchShowsByGenre(@Query("ordering") String sort, @Query("searchgenre") String genre);

    @GET("/shows/{id}")
    Observable<Show> getShow(@Path("id") long id);
}

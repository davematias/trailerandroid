package com.grubot.trailerview.models;

import android.media.Rating;

import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.GameCollection;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Platform;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.network.GamesAPI;
import com.grubot.trailerview.network.MoviesAPI;
import com.grubot.trailerview.network.ServerManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 25-02-2016.
 */
public class GamesModel extends BaseModel implements IGamesModel
{
    public GamesModel(CompositeSubscription mCompositeSubscription)
    {
        super(mCompositeSubscription);
    }

    @Override
    public Game getByExternalIDFromDB(String id) {
        return realm.where(Game.class).equalTo("externalID", id).notEqualTo("cacheDirty",true).findFirst();
    }

    @Override
    public void updateGame(Game game, boolean isFavorite, boolean isCacheDirty) {

        realm.executeTransaction(e ->
        {
            game.setFavorite(isFavorite);
            game.setFavoriteDateMillis(System.currentTimeMillis());

            game.setCacheDirty(isCacheDirty);

        });

    }

    @Override
    public RealmResults<Game> getFavorites(IChangeObserved observer) {

        RealmResults<Game> games = realm.where(Game.class).equalTo("isFavorite",true).findAll();

        if (observer != null)
        {
            // Tell Realm to notify our listener when the customers results
            // have changed (items added, removed, updated, anything of the sort).
            games.addChangeListener(new RealmChangeListener<RealmResults<Game>>() {
                @Override
                public void onChange(RealmResults<Game> element) {
                    observer.onChange();
                }
            });
        }

        return games;
    }

    @Override
    public Game copyOrUpdate(Game game) {

        final Game[] result = new Game[1];

        realm.executeTransaction(e ->
        {
            result[0] = realm.copyToRealmOrUpdate(game);

            for (String localGenre: game.getString_genres()) {

                Genre gen = realm.where(Genre.class).equalTo("name",localGenre.toUpperCase()).findFirst();

                if(gen != null)
                    result[0].getGenresList().add(gen);
            }

            for (String localPlatform: game.getStrings_platforms()) {

                Platform plat = realm.where(Platform.class).equalTo("name",localPlatform).findFirst();

                if(plat == null)
                {
                    plat = new Platform();
                    plat.setName(localPlatform);
                }

                result[0].getPlatformsList().add(plat);
            }

            for (String localRating: game.getStrings_ratings()) {

                String[] rating = localRating.split(":");

                if(rating.length == 2)
                {
                    com.grubot.trailerview.entities.Rating rat = new com.grubot.trailerview.entities.Rating();
                    rat.setName(rating[0]);
                    rat.setRating(rating[1]);
                    rat.setGuid(UUID.randomUUID().toString());

                    result[0].getRatingList().add(rat);
                }
            }

        });

        return result[0];

    }

    @Override
    public void addToGame(Game game, Trailer trailer) {

        if(game.getTrailers().contains(trailer))
            return;

        realm.executeTransaction(e ->
        {
            game.getTrailers().add(trailer);
        });
    }

    @Override
    public RealmResults<Game> getUnfavorited() {
        return realm.where(Game.class).equalTo("isFavorite",false).greaterThan("favoriteDateMillis",0).findAll();
    }

    @Override
    public Observable<GameCollection> getGameByExternalIDObservable(String param) {
        GamesAPI gameService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(GamesAPI.class);
        return gameService.filterGames(null,null,null,param,null);
    }

    @Override
    public Observable<GameCollection> getGameBySlugObservable(String param) {
        GamesAPI gameService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(GamesAPI.class);
        return gameService.filterGames(null,null,null,null,param);
    }

    @Override
    public Observable<GameCollection> doSearchGameObservable(String param, int page, boolean returnEmpty) {

        if(returnEmpty)
            return Observable.<GameCollection>just(new GameCollection(0, new ArrayList<Game>()));

        GamesAPI gameService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(GamesAPI.class);

        return gameService.searchGames(param,page);
    }

    @Override
    public Observable<Game> getGameByIdObservable(Long id) {

        GamesAPI gameService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(GamesAPI.class);
        return gameService.getGame(id);
    }

    @Override
    public Game getGameByIdFromDB(Long id) {
        return realm.where(Game.class).equalTo("id",id).findFirst();
    }
    @Override
    public Game getGameBySlugFromDB(String slug) {
        return realm.where(Game.class).equalTo("slug",slug).findFirst();
    }

    @Override
    public List<Observable<Game>> getGamesByIdObservableList(List<String> ids) {

        List<Observable<Game>> getRequests = new ArrayList<>();

        for (String id: ids) {

            getRequests.add(getGameByIdObservable(Long.parseLong(id)));
        }

        return getRequests;

    }

}

package com.grubot.trailerview.presenters;

import android.view.View;
import android.widget.Button;

import com.grubot.trailerview.entities.Trailer;

import java.util.List;

/**
 * Created by dave on 24-02-2016.
 */
public interface IHomeScreenPresenter
{
    void getFromCache();

    void clearCache();

    void getTrailers(int page, String order, String types);

    void removeNonUsedFilters(List<String> currentFilterList);

    //activities and fragments must call this to clear the observable subscripions from rxjava
    void destroySubscription();
}

package com.grubot.trailerview.presenters;

import com.grubot.trailerview.entities.TraktUserList;
import com.grubot.trailerview.entities.TraktUserSettings;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.models.IChangeObserved;
import com.grubot.trailerview.network.TraktAPI;

import io.realm.RealmResults;

/**
 * Created by dave on 10-04-2016.
 */
public interface ISettingsPresenter {

    User createOrUpdateUser(User currentUser);

    User loadCurrentUser();

    void syncUserData();

    void destroyCurrentUser();

    void getTraktToken();

    void cancelTraktToken();

    void poolForToken();

    void refreshUserList();

    void deleteTraktData();

    TraktUserSettings getTraktData();

    RealmResults<TraktUserSettings> getTrakSyncing(IChangeObserved observer);

    RealmResults<TraktUserList> getTraktLists(IChangeObserved observer);

    void updateTraktUser(TraktUserSettings settings, int selectedList);

    //activities and fragments must call this to clear the observable subscripions from rxjava
    void destroySubscription();
}

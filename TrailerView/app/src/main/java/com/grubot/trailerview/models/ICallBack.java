package com.grubot.trailerview.models;

/**
 * Created by dave on 24-02-2016.
 */
public interface ICallBack<T>
{
    void onFailure(Throwable error);

    void onSuccess(T arg);
}

package com.grubot.trailerview.entities;

import java.util.Map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MovieCollection {

    private long count;
    private List<Movie> results = new ArrayList<Movie>();

    /**
     * No args constructor for use in serialization
     *
     */
    public MovieCollection() {
    }

    /**
     *
     * @param results
     * @param count
     */
    public MovieCollection(long count, List<Movie> results) {
        this.count = count;
        this.results = results;
    }

    /**
     *
     * @return
     * The count
     */
    public long getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The results
     */
    public List<Movie> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<Movie> results) {
        this.results = results;
    }

}

package com.grubot.trailerview.network;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.models.GamesModel;
import com.grubot.trailerview.models.IGamesModel;
import com.grubot.trailerview.models.IMoviesModel;
import com.grubot.trailerview.models.IShowsModel;
import com.grubot.trailerview.models.MoviesModel;
import com.grubot.trailerview.models.ShowsModel;
import com.grubot.trailerview.ui.activities.MainActivity;
import com.grubot.trailerview.ui.activities.VideoActivity;

import org.parceler.Parcels;

/**
 * Created by dave on 24-03-2016.
 */
public class TrailersFcmListenerService extends FirebaseMessagingService {

    private static final String LAST_UPDATE_TOPIC = "NEW_CONTENT";
    private static final String NEW_TRAILER_TOPIC = "NEW_TRAILER";
    private static final String UPDATED_DATA_TOPIC = "CONTENT_UPDATED";

    private static final String TAG = "TrailersFcmLis";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        boolean showNotifications = TrailerDen.getInstance().getBooleanPreference(TrailerDen.PREFERENCE_SHOWNOTIFICATIONS,true);

        if(!showNotifications)
            return;

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        String message = null;

        if(remoteMessage.getNotification() != null)
        {
            Log.d(TAG, "Message: " + remoteMessage.getNotification().getBody());
            message = remoteMessage.getNotification().getBody();
        }
        else if(remoteMessage.getData().containsKey("message"))
        {
            Log.d(TAG, "Message: " + remoteMessage.getData().get("message"));
            message = remoteMessage.getData().get("message");
        }

        try {

            if (remoteMessage.getFrom().startsWith("/topics/") && message != null)
            {
                if(message.equals(LAST_UPDATE_TOPIC))
                {
                    Intent resultIntent = new Intent(this, MainActivity.class);
                    resultIntent.putExtra(MainActivity.ACTIVITY_BUNDLE_REFRESH_TRAILERS_KEY,true);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

                    stackBuilder.addNextIntent(resultIntent);
                    sendNotification(message,getString(R.string.db_update_notification),stackBuilder);
                }
                else
                {
                    String contentId = remoteMessage.getFrom().replace("/topics/","");

                    IMoviesModel moviesModel = new MoviesModel(null);
                    IGamesModel gamesModel = new GamesModel(null);
                    IShowsModel showsModel = new ShowsModel(null);

                    IVideoData content;

                    long id = Long.parseLong(contentId);

                    Movie mov = moviesModel.getMovieByIdFromDb(id);

                    if(mov == null)
                    {
                        Game game = gamesModel.getGameByIdFromDB(id);

                        if(game == null)
                        {
                            content = showsModel.getShowByIdFromDb(id);
                        }
                        else
                            content = game;
                    }
                    else
                      content = mov;

                    if(content != null) {

                        Intent resultIntent = new Intent(this, VideoActivity.class);

                        if(content.getType() == TrailerDen.VIDEO_TYPE.MOVIE)
                            resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_MOVIE_KEY, Parcels.wrap((Movie)content));
                        else if(content.getType() == TrailerDen.VIDEO_TYPE.GAME)
                            resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_GAME_KEY, Parcels.wrap((Game)content));
                        else if(content.getType() == TrailerDen.VIDEO_TYPE.SHOW)
                            resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_SHOW_KEY, Parcels.wrap((Show)content));

                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        // Adds the back stack
                        stackBuilder.addParentStack(VideoActivity.class);
                        // Adds the Intent to the top of the stack
                        stackBuilder.addNextIntent(resultIntent);

                        if (message.equals(NEW_TRAILER_TOPIC)) {
                            sendNotification(message, getString(R.string.trailer_update_notification, content.getTitle()), stackBuilder);
                        } else if (message.equals(UPDATED_DATA_TOPIC))
                            sendNotification(message, getString(R.string.movie_update_notification, content.getTitle()), stackBuilder);
                    }


                    moviesModel.closeDB();
                    showsModel.closeDB();
                    gamesModel.closeDB();
                }

            } else
            {
                // normal downstream message.

                //notification debug
              /*  if (message.equals("debug-not1"))
                {
                   *//* Intent resultIntent = new Intent(this, MainActivity.class);
                    resultIntent.putExtra(MainActivity.ACTIVITY_BUNDLE_REFRESH_TRAILERS_KEY,true);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

                    stackBuilder.addNextIntent(resultIntent);
                    sendNotification(message,getString(R.string.db_update_notification),stackBuilder);*//*

                    Intent resultIntent = new Intent(this, VideoActivity.class);

                    IShowsModel showsModel = new ShowsModel(null);

                    long id = 1193;

                    IVideoData content = showsModel.getShowByIdFromDb(id);

                    resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_SHOW_KEY, Parcels.wrap((Show)content));

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                    // Adds the back stack
                    stackBuilder.addParentStack(VideoActivity.class);
                    // Adds the Intent to the top of the stack
                    stackBuilder.addNextIntent(resultIntent);

                    sendNotification(message, getString(R.string.movie_update_notification, content.getTitle()), stackBuilder);

                }*/
            }


        }catch (Exception ex)
        {
            Log.d(TAG, ex.toString());
        }

    }
    // [END receive_message]

    private void sendNotification(String notificationTag, String message,TaskStackBuilder stackBuilder) {

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_CANCEL_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(getNotificationIcon())
                .setColor(getResources().getColor(R.color.colorAccent))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationTag, 0, notificationBuilder.build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_notify_lollipop : R.mipmap.ic_launcher;
    }

}

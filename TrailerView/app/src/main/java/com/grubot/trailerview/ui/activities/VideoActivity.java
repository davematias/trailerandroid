package com.grubot.trailerview.ui.activities;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.Space;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.transition.TransitionManager;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessaging;
import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Platform;
import com.grubot.trailerview.entities.Rating;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.presenters.IVideoScreenPresenter;
import com.grubot.trailerview.presenters.VideoScreenPresenter;
import com.grubot.trailerview.ui.adapters.ShareIntentListAdapter;
import com.grubot.trailerview.ui.adapters.TrailersListAdapter;
import com.grubot.trailerview.ui.customviews.ExpandableHeightGridView;
import com.grubot.trailerview.ui.dialogs.ReportErrorDialog;
import com.grubot.trailerview.utils.Animations;
import com.grubot.trailerview.utils.CustomTabsActivityHelper;
import com.grubot.trailerview.utils.LinearLayoutWeightAnimationWrapper;
import com.grubot.trailerview.utils.UINotifications;
import com.grubot.trailerview.utils.Utils;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VideoActivity extends AppCompatActivity implements ObservableScrollViewCallbacks, IVideoScreenView {

    //workaround
    public static Activity activity;

    public static boolean isMovie = true;

    private InterstitialAd mInterstitialAd;

    public static String ACTIVITY_BUNDLE_TRAILER_KEY = "TRAILER";
    public static String ACTIVITY_BUNDLE_MOVIE_KEY = "MOVIE";
    public static String ACTIVITY_BUNDLE_SHOW_KEY = "SHOW";
    public static String ACTIVITY_BUNDLE_GAME_KEY = "GAME";

    private YouTubePlayer mPlayer;
    private static final int RECOVERY_REQUEST = 1;
    private String videoID = "";
    private YouTubePlayerFragment youTubePlayerFragment;

    private boolean isFirstLoad = true;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @Bind(R.id.osMovie)
    ObservableScrollView osMovie;

    @Bind(R.id.lldislike)
    LinearLayout lldislike;

    @Bind(R.id.lllike)
    LinearLayout lllike;

    @Bind(R.id.tvDislikeCount)
    TextView tvDislikeCount;

    @Bind(R.id.tvLikeCount)
    TextView tvLikeCount;

    @Bind(R.id.lineNeutral)
    View lineNeutral;

    @Bind(R.id.lineDislike)
    View lineDislike;

    @Bind(R.id.lineLike)
    View lineLike;

    @Bind(R.id.rlMovieInfo)
    LinearLayout rlMovieInfo;

    @Bind(R.id.rlTrailerInfo)
    RelativeLayout rlTrailerInfo;

    @Bind(R.id.rlTrailerVote)
    RelativeLayout rlTrailerVote;

    @Bind(R.id.iViewDisLike)
    ImageView iViewDisLike;

    @Bind(R.id.iViewLike)
    ImageView iViewLike;

    @Bind(R.id.rvAllTrailers)
    RecyclerView rvAllTrailers;

    @Bind(R.id.rlAllTrailers)
    LinearLayout rlAllTrailers;

    @Bind(R.id.rlTrailerPlaceholder)
    RelativeLayout rlTrailerPlaceholder;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.tvDate)
    TextView tvDate;

    @Bind(R.id.tvPlot)
    TextView tvPlot;

    @Bind(R.id.tvDirector)
    TextView tvDirector;
    @Bind(R.id.tvDirectorLabel)
    TextView tvDirectorLabel;

    @Bind(R.id.tvCast)
    TextView tvCast;
    @Bind(R.id.tvCastLabel)
    TextView tvCastLabel;

    @Bind(R.id.tvSeasons)
    TextView tvSeasons;
    @Bind(R.id.tvSeasonsLabel)
    TextView tvSeasonsLabel;

    @Bind(R.id.tvNetwork)
    TextView tvNetwork;
    @Bind(R.id.tvNetworkLabel)
    TextView tvNetworkLabel;

    @Bind(R.id.llRatings)
    LinearLayout llRatings;
    @Bind(R.id.tvRatingsLabel)
    TextView tvRatingsLabel;
    @Bind(R.id.tvExternalsLabel)
    TextView tvExternalsLabel;

    @Bind(R.id.llGenres)
    LinearLayout llGenres;

    @Bind(R.id.tvGenresLabel)
    TextView tvGenresLabel;

    @Bind(R.id.tvStudio)
    TextView tvStudio;
    @Bind(R.id.tvStudioLabel)
    TextView tvStudioLabel;

    @Bind(R.id.tvLanguage)
    TextView tvLanguage;
    @Bind(R.id.tvLanguageLabel)
    TextView tvLanguageLabel;

    @Bind(R.id.tvCountry)
    TextView tvCountry;
    @Bind(R.id.tvCountryLabel)
    TextView tvCountryLabel;

    @Bind(R.id.tvPlatformsLabel)
    TextView tvPlatformsLabel;

    @Bind(R.id.llPlatforms)
    LinearLayout llPlatforms;

    @Bind(R.id.tvTrailerName)
    TextView tvTrailerName;

    @Bind(R.id.btnRequest)
    RelativeLayout btnRequest;

    @Bind(R.id.tvRequestTrailer)
    TextView tvRequestTrailer;

    @Bind(R.id.tvAllTrailers)
    TextView tvAllTrailers;

    @Bind(R.id.ivPoster)
    ImageView ivPoster;

    @Bind(R.id.ibViewTrailer)
    ImageButton ibViewTrailer;

    @Bind(R.id.fab)
    FloatingActionButton fab;

    @Bind(R.id.pbInfoLoad)
    ProgressBar pbInfoLoad;

    @Bind(R.id.ivError)
    ImageView ivError;

    @Bind(R.id.shoppingCenter)
    LinearLayout shoppingCenter;

    @Bind(R.id.amazonSpace)
    Space amazonSpace;

    @Bind(R.id.amazonBuy)
    RelativeLayout amazonBuy;

    @Bind(R.id.amazonStream)
    RelativeLayout amazonStream;

    private Animation fabMakeInAnimation;
    private Animation fabMakeOutAnimation;
    private Animation movieInfoInAnimation;

    private ObjectAnimator lineAnim;

    private IVideoScreenPresenter presenter;

    private OrientationEventListener orientationEventListener;

    private boolean fullScreen = false;

    private Trailer contextTrailer;

    private IVideoData sourceContext;

    private CallbackManager callbackManager;

    private String itemTitle;
    private long itemId;
    private long dataId;
    private boolean prepareVideo;
    private boolean allowRotation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //load modern youtube ui on fragment
        //getLayoutInflater().setFactory(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        int videoViewCounter = TrailerDen.getInstance().getIntPreference("VideoViewCounter");

        if (videoViewCounter >= TrailerDen.MAX_TRAILER_VIEWS) {
            TrailerDen.getInstance().saveIntPreference("VideoViewCounter", 0);
        } else {
            TrailerDen.getInstance().saveIntPreference("VideoViewCounter", videoViewCounter + 1);
        }

        if (videoViewCounter >= TrailerDen.MAX_TRAILER_VIEWS) {
            // Create the InterstitialAd and set the adUnitId.
            mInterstitialAd = new InterstitialAd(this);
            // Defined in res/values/strings.xml
            mInterstitialAd.setAdUnitId(TrailerDen.AD_UNIT_ID);

            if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
                AdRequest adRequest = new AdRequest.Builder().build();
                mInterstitialAd.loadAd(adRequest);
            }
        }

        ButterKnife.bind(this);

        initializeComponents();

        presenter = new VideoScreenPresenter(this);

        activity = this;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(ACTIVITY_BUNDLE_TRAILER_KEY)) {
                contextTrailer = Parcels.unwrap(extras.getParcelable(ACTIVITY_BUNDLE_TRAILER_KEY));
                videoID = contextTrailer.getLink();
                tvTrailerName.setText(contextTrailer.getTitle());
                setPoster(contextTrailer.getPosterSm());
                presenter.loadVideoContext(contextTrailer.getImdbID(), contextTrailer.getId(), contextTrailer.getTypeAsEnum());
                itemTitle = contextTrailer.getTitle();
                itemId = contextTrailer.getId();

                if (contextTrailer.getType() == 0) {
                    isMovie = true;
                } else if (contextTrailer.getType() == 1) {
                    isMovie = true;
                } else {
                    isMovie = false;
                }
            } else if (extras.containsKey(ACTIVITY_BUNDLE_MOVIE_KEY)) {
                Movie contextMovie = Parcels.unwrap(extras.getParcelable(ACTIVITY_BUNDLE_MOVIE_KEY));
                setPoster(contextMovie.getPosterSm());
                presenter.loadVideoContext(contextMovie.getImdbID(),-1, TrailerDen.VIDEO_TYPE.MOVIE);
                isMovie = true;
                dataId = contextMovie.getId();
                itemTitle = contextMovie.getTitle();
            } else if (extras.containsKey(ACTIVITY_BUNDLE_SHOW_KEY)) {
                Show contextShow = Parcels.unwrap(extras.getParcelable(ACTIVITY_BUNDLE_SHOW_KEY));
                setPoster(contextShow.getPosterSm());
                presenter.loadVideoContext(contextShow.getImdbID(),-1, TrailerDen.VIDEO_TYPE.SHOW);
                isMovie = true;
                dataId = contextShow.getId();
                itemTitle = contextShow.getTitle();
            } else if (extras.containsKey(ACTIVITY_BUNDLE_GAME_KEY)) {
                Game contextGame = Parcels.unwrap(extras.getParcelable(ACTIVITY_BUNDLE_GAME_KEY));
                setPoster(contextGame.getPosterSm());
                presenter.loadVideoContext(contextGame.getExternalID(),-1, TrailerDen.VIDEO_TYPE.GAME);
                isMovie = false;
                dataId = contextGame.getId();
                itemTitle = contextGame.getTitle();
            }
        }

        if (isMovie) {
            amazonBuy.setTag("https://www.amazon.com/gp/search?tag=" + TrailerDen.AMAZON_TAG + "&keywords=" + itemTitle.toLowerCase().split("trailer #")[0] + "&url=search-alias%3Dmovies-tv");
            amazonSpace.setVisibility(View.VISIBLE);
            amazonStream.setVisibility(View.VISIBLE);
            amazonStream.setTag("https://www.amazon.com/gp/video/primesignup?ie=UTF8&rw_useCurrentProtocol=1&tag=" + TrailerDen.AMAZON_TAG);
        } else {
            amazonBuy.setTag("https://www.amazon.com/gp/search?tag=" + TrailerDen.AMAZON_TAG + "&keywords=" + itemTitle.toLowerCase().split("trailer #")[0] + "&url=search-alias%3Dvideogames");
        }

        orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_FASTEST) {
            @Override
            public void onOrientationChanged(int arg0) {
                if (arg0 == 90 || arg0 == 270) {
                    if (mPlayer == null && !videoID.equals("") && allowRotation) {
                        initializeVideo();
                    }
                }

            }

        };

        if (orientationEventListener.canDetectOrientation())
            orientationEventListener.enable();
    }

    @Override
    protected void onPause() {
        allowRotation = false;
        super.onPause();
    }

    @Override
    protected void onResume() {
        allowRotation = true;

        if (mPlayer != null) {
            mPlayer.setFullscreen(false);
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        presenter.destroySubscription();

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mPlayer != null && fullScreen) {
            mPlayer.setFullscreen(false);
        } else {
            prepareExit();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:

                prepareExit();

                supportFinishAfterTransition();

                return true;

            case R.id.action_report:
                openDialog(ReportErrorDialog.newInstance(Long.toString(itemId), coordinatorLayout), "tag");
                return true;

            case R.id.action_share:
                share(this, itemTitle, contextTrailer.getWebLink());
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private void prepareExit() {
        showInterstitial();

        if (mPlayer != null) {
            mPlayer.release();

            youTubePlayerFragment.getView().setVisibility(View.GONE);
            rlTrailerPlaceholder.setVisibility(View.VISIBLE);
        }

        showToolbar();
    }

    private void showInterstitial() {

        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    private void initializeComponents() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setShowHideAnimationEnabled(true);

        rlMovieInfo.setVisibility(View.INVISIBLE);

        movieInfoInAnimation = AnimationUtils.makeInChildBottomAnimation(getBaseContext());
        movieInfoInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                rlMovieInfo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });

        osMovie.setScrollViewCallbacks(this);

        youTubePlayerFragment = (YouTubePlayerFragment) this.getFragmentManager()
                .findFragmentById(R.id.youtube_fragment);

        youTubePlayerFragment.getView().setVisibility(View.GONE);

        ibViewTrailer.setOnClickListener(e -> {
            playVideo(contextTrailer);
        });

        iViewLike.setOnClickListener(e -> {

            handleVote(true);

        });

        iViewDisLike.setOnClickListener(e -> {

            handleVote(false);

        });

        fab.setVisibility(View.GONE);

        fab.setOnClickListener(e -> changeContentFavorite());

        fabMakeInAnimation = AnimationUtils.makeInAnimation(getBaseContext(), false);
        fabMakeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
                fab.setVisibility(View.VISIBLE);
            }
        });

        fabMakeOutAnimation = AnimationUtils.makeOutAnimation(getBaseContext(), true);
        fabMakeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                fab.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        rvAllTrailers.setLayoutManager(layoutManager);

    }

    private void setupLikesCounter(Boolean isLike) {

        if (contextTrailer != null) {
            if (contextTrailer.getDislikes() == 0 && contextTrailer.getLikes() == 0) {
                lineNeutral.setVisibility(View.VISIBLE);
                lineDislike.setVisibility(View.GONE);
                lineLike.setVisibility(View.GONE);

                tvLikeCount.setText("0");

                tvDislikeCount.setText("0");

                iViewDisLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_down_gray_24dp));
                iViewLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_up_gray_24dp));
            } else {
                lineNeutral.setVisibility(View.GONE);


                lineDislike.setVisibility(View.VISIBLE);
                lineLike.setVisibility(View.VISIBLE);

                float total = (contextTrailer.getDislikes() + contextTrailer.getLikes());

                float likePerc = (contextTrailer.getLikes() / total);

                float dislikesPerc = (contextTrailer.getDislikes() / total);

                LinearLayout.LayoutParams lParam = (LinearLayout.LayoutParams) lineLike.getLayoutParams();

                //if(isLike == null || !isLike.booleanValue())
                //{
                lParam.weight = likePerc;
                //}

                if (likePerc == 0 || dislikesPerc == 0)
                    lParam.leftMargin = 0;
                else
                    lParam.leftMargin = 5;

                lineLike.setLayoutParams(lParam);

                LinearLayout.LayoutParams dlParam = (LinearLayout.LayoutParams) lineDislike.getLayoutParams();
                if (likePerc == 0 || dislikesPerc == 0)
                    dlParam.rightMargin = 0;
                else
                    dlParam.rightMargin = 5;

                if (isLike == null || dislikesPerc == 0 || likePerc == 0) {
                    dlParam.weight = dislikesPerc;
                }

                lineDislike.setLayoutParams(dlParam);

                    /*if(isLike != null && isLike.booleanValue())
                    {
                        animateBarWeight(lineLike,likePerc);
                    }
                    else
                    {*/
                if (dislikesPerc > 0 && likePerc > 0 && isLike != null)
                    animateBarWeight(lineDislike, dislikesPerc);
                //}

                tvLikeCount.setText(contextTrailer.getLikes() + "");

                tvDislikeCount.setText(contextTrailer.getDislikes() + "");

                if (contextTrailer.getLiked() != null) {
                    if (!contextTrailer.getLiked().booleanValue()) {
                        iViewDisLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_down_white_24dp));
                        iViewLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_up_gray_24dp));
                    } else {
                        iViewDisLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_down_gray_24dp));
                        iViewLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_up_white_24dp));
                    }
                } else {
                    iViewDisLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_down_gray_24dp));
                    iViewLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_up_gray_24dp));
                }
            }

            if (rlTrailerInfo.getVisibility() != View.VISIBLE) {
                Animations.expand(rlTrailerInfo, new Animations.IAnimationEnd() {
                    @Override
                    public void onEnd() {
                        playIt();
                    }
                });
            } else
                playIt();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video, menu);

        MenuItem item_genres = menu.findItem(R.id.action_genres);

        //MenuItem item_search = menu.findItem(R.id.action_search);


        return true;
    }

    private void animateBarWeight(View bar, float newWeight) {
        if (lineAnim != null)
            lineAnim.cancel();

        LinearLayoutWeightAnimationWrapper animationWrapper = new LinearLayoutWeightAnimationWrapper(bar);
        lineAnim = ObjectAnimator.ofFloat(
                animationWrapper,
                "weight",
                animationWrapper.getWeight(),
                newWeight);
        lineAnim.setDuration(1000);

        lineAnim.start();
    }

    private void initializeVideo() {
        try {

            youTubePlayerFragment.getView().setVisibility(View.VISIBLE);
            rlTrailerPlaceholder.setVisibility(View.GONE);


            youTubePlayerFragment.initialize(TrailerDen.YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {

                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {

                    mPlayer = player;

                    //YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT avoids rebuffer but we have to handle all view changes manually
                    player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI | YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);

                    if (!wasRestored) {

                        player.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                            @Override
                            public void onFullscreen(boolean b) {

                                fullScreen = b;

                                if (!b) {
                                    int orientation = getApplicationContext().getResources().getConfiguration().orientation;
                                    if (orientation != Configuration.ORIENTATION_PORTRAIT) {
                                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                                    }
                                }

                                // doLayout();

                            }
                        });

                        player.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                            @Override
                            public void onLoading() {

                            }

                            @Override
                            public void onLoaded(String s) {

                            }

                            @Override
                            public void onAdStarted() {

                            }

                            @Override
                            public void onVideoStarted() {
                                presenter.trailerAddView(contextTrailer.getId());
                            }

                            @Override
                            public void onVideoEnded() {

                            }

                            @Override
                            public void onError(YouTubePlayer.ErrorReason errorReason) {

                            }
                        });

                        player.loadVideo(videoID);

                    } else
                        player.play();


                }


                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
                    if (errorReason.isUserRecoverableError()) {
                        errorReason.getErrorDialog(VideoActivity.activity, RECOVERY_REQUEST).show();
                    } else {
                        String error = String.format(getString(R.string.player_error), errorReason.toString());
                        UINotifications.showAsSnackbar(coordinatorLayout, error, Snackbar.LENGTH_LONG);
                    }
                }

            });

        } catch (Exception e) {
            Log.d("", e.getMessage());
        }
    }

    private void setPoster(String url) {
        Picasso.with(getApplicationContext())
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(ivPoster);
    }

    private void loadContextInfo() {
        //Mandatory
        tvTitle.setText(sourceContext.getTitle());
        tvDate.setText(Utils.convertTimestampToDate(sourceContext.getReleaseDate()));
        tvPlot.setText(sourceContext.getPlot());

        //Not Mandatory
        handleGenres(tvGenresLabel, llGenres, sourceContext.getGenresList());

        handleInfo(tvDirectorLabel, tvDirector, sourceContext.getDirector());

        handleInfo(tvCastLabel, tvCast, sourceContext.getCast());

        handleInfo(tvCountryLabel, tvCountry, sourceContext.getCountry());

        handleInfo(tvLanguageLabel, tvLanguage, sourceContext.getLanguage());

        handleSeasons(tvSeasonsLabel, tvSeasons, sourceContext.getSeasons());

        handleInfo(tvNetworkLabel, tvNetwork, sourceContext.getNetwork());

        handleRating(tvRatingsLabel, llRatings, sourceContext, tvExternalsLabel);

        handleInfo(tvStudioLabel, tvStudio, sourceContext.getStudio());

        handlePlatforms(tvPlatformsLabel, llPlatforms, sourceContext.getPlatformsList());

        fab.setVisibility(View.VISIBLE);

        changeContextFavoriteImage();

        TrailersListAdapter adapter = new TrailersListAdapter(this, this, sourceContext.getTrailers());

        rvAllTrailers.setAdapter(adapter);

        if (sourceContext.getTrailers().size() == 1) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) rvAllTrailers.getLayoutParams();
            lp.width = getResources().getDimensionPixelSize(R.dimen.single_trailer_list_width);
            rvAllTrailers.setLayoutParams(lp);
        }

        if (sourceContext.getTrailers().size() > 0) {
            rlAllTrailers.setVisibility(View.VISIBLE);
            ibViewTrailer.setVisibility(View.VISIBLE);
            tvTrailerName.setVisibility(View.VISIBLE);
        }
        else {
            rlAllTrailers.setVisibility(View.GONE);
            tvAllTrailers.setVisibility(View.GONE);
            ibViewTrailer.setVisibility(View.GONE);
            tvTrailerName.setVisibility(View.GONE);
            tvRequestTrailer.setVisibility(View.VISIBLE);
            btnRequest.setVisibility(View.VISIBLE);
        }

        rlMovieInfo.setVisibility(View.VISIBLE);
    }

    private void handleInfo(TextView label, TextView tvinfo, String info) {
        if (info != null && !info.isEmpty()) {
            if (!info.toLowerCase().equals("n/a")) {
                label.setVisibility(View.VISIBLE);
                tvinfo.setText(info.replaceAll("%;%", ","));//confia em mim, alguem cometeu erros a inserir jogos e agora n lhe apetece procurar onde causou erros
                //you lazy noooob
                tvinfo.setVisibility(View.VISIBLE);
            }
        }
    }

    private void handleRating(TextView label, LinearLayout ll, IVideoData source, TextView external) {
        if (source.getRatingList() != null && !source.getRatingList().isEmpty()) {
            ll.setVisibility(View.VISIBLE);
            external.setVisibility(View.VISIBLE);
            LayoutInflater inflater = LayoutInflater.from(this);
            int averageRating = 0;
            int counts = 0;
            int spacing = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.movie_detail_spacer), getResources().getDisplayMetrics());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(1, spacing / 2);
            Space space;

            for (Rating rating : source.getRatingList()) {

                space = new Space(getApplicationContext());
                space.setLayoutParams(layoutParams);
                View v = inflater.inflate(R.layout.ratingsbase, null, false);
                Drawable drawable = null;
                //TextView tvRatingName = (TextView) v.findViewById(R.id.tvRatingName);
                TextView tvRating = (TextView) v.findViewById(R.id.tvRating);
                ImageView logoView = (ImageView) v.findViewById(R.id.ivLogo);

                switch (rating.getName().toLowerCase()) {
                    case "imdb":
                        //tvRatingName.setText("Imdb");
                        drawable = getResources().getDrawable(R.drawable.imdblogo);
                        logoView.setTag("http://www.imdb.com/title/" + source.getExternalID());
                        if (!rating.getRating().equals("0")) {
                            averageRating += Float.parseFloat(rating.getRating()) * 10;
                            tvRating.setText(rating.getRating() + "/10");
                            counts += 1;
                        }
                        break;
                    case "trakt":
                        //tvRatingName.setText("Trakt");
                        drawable = getResources().getDrawable(R.drawable.traktlogo);
                        if (source.getType().compareTo(TrailerDen.VIDEO_TYPE.MOVIE) == 0) {
                            logoView.setTag("https://trakt.tv/movies/" + source.getTraktID());
                        } else {
                            logoView.setTag("https://trakt.tv/shows/" + source.getTraktID());
                        }
                        if (!rating.getRating().equals("0")) {
                            averageRating += (int) Float.parseFloat(rating.getRating());
                            tvRating.setText(rating.getRating() + "%");
                            counts += 1;
                        }
                        break;
                    case "giantbomb":
                        //tvRatingName.setText("GiantBomb");
                        drawable = getResources().getDrawable(R.drawable.giantbomblogo);
                        logoView.setTag("http://www.giantbomb.com/" + source.getExternalID());
                        if (!rating.getRating().equals("0")) {
                            averageRating += (int) Float.parseFloat(rating.getRating());
                            tvRating.setText(rating.getRating() + "%");
                            counts += 1;
                        }
                        break;
                    default:
                        break;
                }

                if (drawable != null) {
                    logoView.setImageDrawable(drawable);

                    ll.addView(space);
                    ll.addView(v);
                }
            }
            if (averageRating != 0) {
                label.setVisibility(View.VISIBLE);
                label.append(" " + (averageRating / counts) + "/100");
            }
        }
    }

    public void followExternalURL(View view) {

        if (view.getTag() == null)
            return;

        try {
            CustomTabsActivityHelper.launchUrl(view.getTag().toString(), activity, coordinatorLayout);
        } catch (Exception e) {
            UINotifications.showAsSnackbar(coordinatorLayout, activity.getResources().getString(R.string.url_open_failed), Snackbar.LENGTH_LONG);
        }
    }

    private void handleSeasons(TextView label, TextView tvinfo, List info) {
        if (info != null && !info.isEmpty()) {
//            if(!info.toLowerCase().equals("n/a"))
//            {
//                label.setVisibility(View.VISIBLE);
//                tvinfo.setText(sourceContext.getTitle());
//                tvinfo.setVisibility(View.VISIBLE);
//            }
        }
    }

    private void handlePlatforms(TextView label, LinearLayout ll, List<Platform> info) {
        if (info != null && !info.isEmpty()) {
            ll.setVisibility(View.VISIBLE);
            label.setVisibility(View.VISIBLE);

            List<String> platforms = new ArrayList<>();
            for (Platform platform : info) {
                platforms.add(platform.getName());
            }

            ExpandableHeightGridView gridView = new ExpandableHeightGridView(this);

            gridView.setNumColumns(2);
            gridView.setPadding(8, 8, 8, 8);
            gridView.setHorizontalSpacing(8);
            gridView.setVerticalSpacing(8);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.platform_gridview_item, platforms);

            gridView.setAdapter(adapter);
            gridView.setExpanded(true);
            gridView.setFocusable(false);
            ll.addView(gridView);
        }
    }

    private void handleGenres(TextView label, LinearLayout ll, List<Genre> info) {
        if (info != null && !info.isEmpty()) {
            ll.setVisibility(View.VISIBLE);
            label.setVisibility(View.VISIBLE);

            List<String> genreNames = new ArrayList<>();
            for (Genre genre : info) {
                genreNames.add(genre.getName());
            }
            ExpandableHeightGridView gridView = new ExpandableHeightGridView(this);

            gridView.setNumColumns(2);
            gridView.setPadding(8, 8, 8, 8);
            gridView.setHorizontalSpacing(8);
            gridView.setVerticalSpacing(8);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.movie_genre_gridview_item, genreNames);

            gridView.setAdapter(adapter);
            gridView.setExpanded(true);
            gridView.setFocusable(false);
            ll.addView(gridView);
        }
    }

    private void changeContentFavorite() {
        presenter.updateContext(sourceContext, !sourceContext.isFavorite(), sourceContext.isCacheDirty());

        if (sourceContext.isFavorite())
            FirebaseMessaging.getInstance().subscribeToTopic(sourceContext.getId() + "");
        else
            FirebaseMessaging.getInstance().unsubscribeFromTopic(sourceContext.getId() + "");

        changeContextFavoriteImage();
    }

    private void changeContextFavoriteImage() {
        if (sourceContext.isFavorite())
            fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp));
        else
            fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white_24dp));
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll,
                                boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {


    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (scrollState == ScrollState.UP) {
            if (fab.getVisibility() == View.VISIBLE)
                fab.startAnimation(fabMakeOutAnimation);

            if (toolbarIsShown()) {
                hideToolbar();
            }


        } else if (scrollState == ScrollState.DOWN) {
            if (fab.getVisibility() == View.INVISIBLE)
                fab.startAnimation(fabMakeInAnimation);

            if (toolbarIsHidden()) {
                showToolbar();
            }


        }
    }

    private boolean toolbarIsShown() {
        // Toolbar is 0 in Y-axis, so we can say it's shown.
        return toolbar.getTranslationY() == 0;
    }

    private boolean toolbarIsHidden() {
        // Toolbar is outside of the screen and absolute Y matches the height of it.
        // So we can say it's hidden.

        return toolbar.getTranslationY() == -toolbar.getHeight();
    }

    private void showToolbar() {
        moveToolbar(0);
    }

    private void hideToolbar() {
        moveToolbar(-toolbar.getHeight());
    }

    protected int getScreenHeight() {
        return findViewById(android.R.id.content).getHeight();
    }

    private void moveToolbar(float toTranslationY) {
        // Check the current translationY
        if (toolbar.getTranslationY() == toTranslationY) {
            return;
        }

//        ValueAnimator animator = ValueAnimator.ofFloat(toolbar.getTranslationY(), toTranslationY).setDuration(200);
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                float translationY = (float) animation.getAnimatedValue();
//                toolbar.setTranslationY(translationY);
//                ((View) osMovie).setTranslationY(translationY);
//                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) ((View) osMovie).getLayoutParams();
//                lp.height = (int) -translationY + getScreenHeight() - lp.topMargin;
//                ((View) osMovie).requestLayout();
//            }
//        });
//        animator.start();
    }

    @Override
    public void handleFavoriteError(Throwable error) {

        // TODO: 16/05/2016 add resources after favorites sync implementation
        //UINotifications.showAsSnackbar(coordinatorLayout, "error", "favorite error", e -> reloadContext());
    }

    @Override
    public void handleGetContextSuccess(IVideoData videoContext) {
        sourceContext = videoContext;

        if (contextTrailer != null) {
            contextTrailer = sourceContext.getTrailer(contextTrailer.getId()); //  movie.getTrailers().where().equalTo("id",contextTrailer.getId()).findFirst();
            setupLikesCounter(null);
        } else {
            contextTrailer = sourceContext.getFirstTrailer();

            if (contextTrailer != null) {
                videoID = contextTrailer.getLink();
                tvTrailerName.setText(contextTrailer.getTitle());

                setupLikesCounter(null);
            }

        }

        loadContextInfo();
        pbInfoLoad.setVisibility(View.GONE);
    }

    @Override
    public void handleGetContextError(Throwable error) {
        ivError.setVisibility(View.VISIBLE);
        pbInfoLoad.setVisibility(View.GONE);

        UINotifications.showAsSnackbar(coordinatorLayout, getString(R.string.error_movieDownload), getString(R.string.reloadMovieButton), e -> reloadContext());
    }

    public void reloadContext() {
        ivError.setVisibility(View.GONE);
        pbInfoLoad.setVisibility(View.VISIBLE);

        if (contextTrailer == null)
            presenter.loadVideoContext(sourceContext.getExternalID(),-1, sourceContext.getType());
        else
            presenter.loadVideoContext(contextTrailer.getImdbID(), contextTrailer.getId(), contextTrailer.getTypeAsEnum());
    }

    private void playIt() {
        if (prepareVideo) {
            if (mPlayer == null) {
                initializeVideo();
            } else {
                mPlayer.loadVideo(videoID);
            }

            prepareVideo = false;
        }
    }

    @Override
    public void playVideo(Trailer trailer) {
        this.prepareVideo = true;
        this.videoID = trailer.getLink();
        contextTrailer = trailer;

        setupLikesCounter(null);

        //osMovie.smoothScrollTo(0, 0);

        //showToolbar();
        //fab.setVisibility(View.VISIBLE);

    }

    @Override
    public void handleVote(boolean isLike) {

        //Animations.collapse(rlTrailerVote, null);

        //UINotifications.showAsSnackbar(coordinatorLayout, activity.getResources().getString(R.string.trailerVoteEnd), Snackbar.LENGTH_SHORT);

        if (contextTrailer != null) {
            if (isLike)
                presenter.addLike(contextTrailer);
            else
                presenter.addDislike(contextTrailer);

            setupLikesCounter(isLike);
        }
    }

    public void share(Context context, String title, String trailer) {
        Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        List<ResolveInfo> activitiesAux = context.getPackageManager().queryIntentActivities(sendIntent, 0);
        List<ResolveInfo> activities = new ArrayList<>();

        for (ResolveInfo ri : activitiesAux) {
            if (ri.activityInfo.name.equals("com.google.android.libraries.social.gateway.GatewayActivity"))
                activities.add(ri);
            if (ri.activityInfo.name.equals("com.facebook.composer.shareintent.ImplicitShareIntentHandlerDefaultAlias"))
                activities.add(ri);
            if (ri.activityInfo.name.equals("com.twitter.android.composer.ComposerActivity"))
                activities.add(ri);
            if (ri.activityInfo.name.equals("com.whatsapp.ContactPicker"))
                activities.add(ri);
            if (ri.activityInfo.name.equals("com.facebook.messenger.intents.ShareIntentHandler"))
                activities.add(ri);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(((Activity) context).getResources().getText(R.string.share_action_title));
        final ShareIntentListAdapter adapter = new ShareIntentListAdapter((Activity) context, R.layout.share_list_item, activities.toArray());
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ResolveInfo info = (ResolveInfo) adapter.getItem(which);
                if (info.activityInfo.packageName.contains("com.facebook.katana")) {
                    try {

                        FacebookSdk.sdkInitialize(context);
                        callbackManager = CallbackManager.Factory.create();
                        ShareDialog shareDialog = new ShareDialog(VideoActivity.activity);

                        if (ShareDialog.canShow(ShareLinkContent.class)) {
                            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                    .setContentTitle(title)
                                    .setContentUrl(Uri.parse(trailer))
                                    .build();

                            shareDialog.show(linkContent);
                        }

                    } catch (Exception e) {
                        Log.d("fb", "fb");
                    }

                } else {
                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, TextUtils.htmlEncode(title + "\n" + trailer + "\n\n"+ getString(R.string.share_action_text) +"\n" + "http://www.trailer-den.com/"));
                    context.startActivity(intent);
                }
            }
        });
        builder.create().show();

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void openDialog(DialogFragment dialog, String dialogTag) {

        try {

            FragmentManager fm = getFragmentManager();
            dialog.show(fm, dialogTag);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void requestTrailers(View view)
    {
        Utils.reportTrailerError(dataId + " -- " + itemTitle, "REQUEST");
        UINotifications.showAsSnackbar(coordinatorLayout,getString(R.string.report_thxforhelp_fav), Snackbar.LENGTH_LONG);
    }


    /*private void doLayout() {
        if (fullScreen) {
            // YouTube library automatically deals with orientation changes and hiding system UI
            youTubePlayerFragment.getView().setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
           // otherViews.setVisibility(View.GONE);
        } else { *//* non-fullscreen layout *//* }
    }*/

}

package com.grubot.trailerview.ui.fragments;

import com.grubot.trailerview.entities.IVideoData;

public interface IMovieView
{
    void updateContext(IVideoData videoContext, boolean isFavorite, boolean isCacheDirty);
}

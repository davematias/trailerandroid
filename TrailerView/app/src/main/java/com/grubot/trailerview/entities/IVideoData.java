package com.grubot.trailerview.entities;

import com.grubot.trailerview.TrailerDen;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by Dave on 19/05/2016.
 */
public interface IVideoData {

    public Trailer getFirstTrailer();

    public Trailer getTrailer(long id);

    public String getPoster();

    public String getPosterMed();

    public String getPosterSm();

    public long getId();

    public long getFavoriteDateMillis();

    public boolean isCacheDirty();

    public RealmList<Genre> getGenresList();

    public List<String> getString_genres();

    public RealmList<Trailer> getTrailers();

    public String getExternalID();

    public String getTraktID();

    public boolean isFavorite();

    public String getTitle();

    public String getReleaseDate();

    public String getPlot();

    public String getDirector();

    public String getCast();

    public String getCountry();

    public String getLanguage();

    public RealmList<Season> getSeasons();

    public String getNetwork();

    public RealmList<Rating> getRatingList();

    public String getStudio();

    public RealmList<Platform> getPlatformsList();

    public TrailerDen.VIDEO_TYPE getType();

    public String getSlug();
}
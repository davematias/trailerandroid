package com.grubot.trailerview.ui.dialogs;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.grubot.trailerview.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dave on 19-04-2016.
 */
public class ConfirmDialog extends DialogFragment {

    private String tag;

    private boolean resultVal = false;

    @Bind(R.id.tvMsg)
    TextView tvMsg;

    @Bind(R.id.tvMsgSecondary)
    TextView tvMsgSecondary;


    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.btnConfirm)
    Button btnConfirm;

    public ConfirmDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static ConfirmDialog newInstance(String title, String msg, String msg2, String tag) {
        ConfirmDialog frag = new ConfirmDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("text", msg);
        args.putString("text2", msg2);
        args.putString("tag", tag);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.confirm_dialog, container);
        ButterKnife.bind(this, v);

        String title = getArguments().getString("title", "");
        String txt = getArguments().getString("text", "");
        String txt2 = getArguments().getString("text2", "");

        tag = getArguments().getString("tag", "");

        tvTitle.setText(title);
        tvMsg.setText(txt);

        if(txt2.isEmpty())
            tvMsgSecondary.setVisibility(View.GONE);
        else
            tvMsgSecondary.setText(txt2);

        btnConfirm.setOnClickListener(e -> {
            resultVal = true;
            dismiss();
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDismiss(DialogInterface iFace)
    {
        returnValue();
        super.onDismiss(iFace);
    }



    public void returnValue()
    {
        // Return input text back to activity through the implemented listener
        IDialogListener listener = (IDialogListener) getTargetFragment();
        listener.onFinishDialog(tag, resultVal);
    }
}

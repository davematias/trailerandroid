package com.grubot.trailerview.ui.activities;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.presenters.ISplashPresenter;
import com.grubot.trailerview.presenters.SplashPresenter;

import org.parceler.Parcels;

public class SplashActivity extends AppCompatActivity implements ISplashScreenView {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        loadMain();

        finish();
    }

    private void loadMain(){
        TrailerDen.getInstance().saveIntPreference("appUsesCounter", TrailerDen.getInstance().getIntPreference("appUsesCounter") + 1);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }



    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    public void finalizeAppLink(Intent resultIntent){

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack
        stackBuilder.addParentStack(VideoActivity.class);
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);

        try {
            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_CANCEL_CURRENT).send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
            loadMain();
        }
    }


}

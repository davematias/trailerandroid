package com.grubot.trailerview.ui.activities;

import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Trailer;

/**
 * Created by dave on 25-02-2016.
 */
public interface IVideoScreenView
{
    void handleFavoriteError(Throwable error);

    void handleGetContextSuccess(IVideoData videoContext);

    void handleGetContextError(Throwable error);

    void playVideo(Trailer trailer);

    void handleVote(boolean isLike);
}

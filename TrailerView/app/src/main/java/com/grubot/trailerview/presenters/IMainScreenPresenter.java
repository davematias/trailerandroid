package com.grubot.trailerview.presenters;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.models.IChangeObserved;

import io.realm.RealmResults;

/**
 * Created by dave on 25-02-2016.
 */
public interface IMainScreenPresenter {

    RealmResults<Genre> getGenres(IChangeObserved observer);

    void syncGenres();

    void updateGenres(boolean filterOnTrailer);

    void updateGenre(Genre genre, boolean filterOnTrailer);

    User getCurrentUser();

    User copyOrUpdateCurrentUser(User userData);

    RealmResults<User> getUserSyncing(IChangeObserved observer);

    void loadTrailer(long id);

    void loadVideoData(String slug, TrailerDen.VIDEO_TYPE contexType);

    //activities and fragments must call this to clear the observable subscripions from rxjava
    void destroySubscription();
}

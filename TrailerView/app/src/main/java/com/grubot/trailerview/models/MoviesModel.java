package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.GameCollection;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.network.MoviesAPI;
import com.grubot.trailerview.network.ServerManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 25-02-2016.
 */
public class MoviesModel extends BaseModel implements IMoviesModel
{
    public MoviesModel(CompositeSubscription mCompositeSubscription)
    {
        super(mCompositeSubscription);
    }

    @Override
    public Movie getMovieByImdbIDFromDB(String id)
    {
        return realm.where(Movie.class).equalTo("imdbID", id).notEqualTo("cacheDirty",true).findFirst();
    }

    @Override
    public void updateMovie(Movie movie, boolean isFavorite, boolean isCacheDirty)
    {
        realm.executeTransaction(e ->
        {

           movie.setIsFavorite(isFavorite);
           movie.setFavoriteDateMillis(System.currentTimeMillis());

           movie.setCacheDirty(isCacheDirty);

        });
    }

    @Override
    public RealmResults<Movie> getFavorites(IChangeObserved observer) {

        RealmResults<Movie> movies = realm.where(Movie.class).equalTo("isFavorite",true).findAll();

        if (observer != null)
        {
            // Tell Realm to notify our listener when the customers results
            // have changed (items added, removed, updated, anything of the sort).
            movies.addChangeListener(new RealmChangeListener<RealmResults<Movie>>() {
                @Override
                public void onChange(RealmResults<Movie> element) {
                    observer.onChange();
                }
            });
        }

        return movies;
    }

    @Override
    public Movie copyOrUpdate(Movie movie) {

        final Movie[] result = new Movie[1];

        realm.executeTransaction(e ->
        {
            result[0] = realm.copyToRealmOrUpdate(movie);

            for (String localGenre: movie.getString_genres()) {

                Genre gen = realm.where(Genre.class).equalTo("name",localGenre.toUpperCase()).findFirst();

                if(gen != null)
                    result[0].getGenresList().add(gen);
            }

            for (String localRating: movie.getStrings_ratings()) {

                String[] rating = localRating.split(":");

                if(rating.length == 2)
                {
                    com.grubot.trailerview.entities.Rating rat = new com.grubot.trailerview.entities.Rating();
                    rat.setName(rating[0]);
                    rat.setRating(rating[1]);
                    rat.setGuid(UUID.randomUUID().toString());

                    result[0].getRatingList().add(rat);
                }
            }

            /*for (Trailer trailer: movie.getTrailers()) {

                Trailer realmTrailer = realm.copyToRealmOrUpdate(trailer);
                result[0].getTrailers().add(realmTrailer);
            }
            */
        });

        return result[0];
    }

    @Override
    public void addToMovie(Movie mov, Trailer trailer) {

        if(mov.getTrailers().contains(trailer))
            return;

        realm.executeTransaction(e ->
        {
            mov.getTrailers().add(trailer);
        });
    }

    @Override
    public RealmResults<Movie> getUnfavorited() {
        return realm.where(Movie.class).equalTo("isFavorite",false).greaterThan("favoriteDateMillis",0).findAll();
    }

    @Override
    public Observable<MovieCollection> getMovieByImdbIDObservable(String id)
    {
        MoviesAPI movieService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(MoviesAPI.class);
        return movieService.filterMovies(null,null,null,id,null);
    }

    @Override
    public Observable<MovieCollection> doSearchMovieObservable(String param, int pageCount, boolean returnEmpty) {

        if(returnEmpty)
            return Observable.<MovieCollection>just(new MovieCollection(0, new ArrayList<Movie>()));

        MoviesAPI movieService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(MoviesAPI.class);

        return movieService.searchMovie(param,pageCount);
    }

    @Override
    public Observable<MovieCollection> getMovieBySlugObservable(String param) {
        MoviesAPI movieService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(MoviesAPI.class);
        return movieService.filterMovies(null,null,null,null,param);
    }

    @Override
    public Observable<Movie> getMovieByIdObservable(Long id) {

        MoviesAPI movieService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(MoviesAPI.class);

        return movieService.getMovie(id);
    }

    @Override
    public Movie getMovieByIdFromDb(Long id) {
        return realm.where(Movie.class).equalTo("id",id).findFirst();
    }

    @Override
    public Movie getMovieBySlugFromDB(String slug) {
        return realm.where(Movie.class).equalTo("slug",slug).findFirst();
    }

    @Override
    public List<Observable<Movie>> getMoviesByIdObservableList(List<String> ids) {

        List<Observable<Movie>> getMovieRequests = new ArrayList<>();

        for (String id: ids) {

            getMovieRequests.add(getMovieByIdObservable(Long.parseLong(id)));
        }

        return getMovieRequests;
    }
}

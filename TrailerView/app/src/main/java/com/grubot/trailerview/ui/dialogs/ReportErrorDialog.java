package com.grubot.trailerview.ui.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.ui.activities.VideoActivity;
import com.grubot.trailerview.utils.UINotifications;
import com.grubot.trailerview.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by dave on 19-04-2016.
 */
public class ReportErrorDialog extends DialogFragment {

    @Bind(R.id.llreportPlayer)
    LinearLayout btn_reportPlayer;

    @Bind(R.id.llreportRepeated)
    LinearLayout btn_reportRepeated;

    @Bind(R.id.llreportWrong)
    LinearLayout btn_reportWrong;

    private static CoordinatorLayout clContainer;

    public ReportErrorDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static ReportErrorDialog newInstance(String id, CoordinatorLayout clContainer) {
        ReportErrorDialog frag = new ReportErrorDialog();
        ReportErrorDialog.clContainer = clContainer;
        Bundle args = new Bundle();
        args.putString("id", id);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.report_error_dialog, container,true);
        ButterKnife.bind(this, v);

        String id = getArguments().getString("id", "");

        btn_reportRepeated.setOnClickListener(e -> {
            //sendMail(TrailerDen.ERRORS_MAIL, "REPEATED TRAILER : " + id, "REPEATED TRAILER : " + id);

            //FirebaseCrash.report(new Exception("ERROR_REPORT -- REPEATED TRAILER : " + id));

            Utils.reportTrailerError(id, "REPEATED TRAILER");

            UINotifications.showAsSnackbar(clContainer,getString(R.string.report_thxforhelp), Snackbar.LENGTH_SHORT);
            this.dismiss();
        });

        btn_reportWrong.setOnClickListener(e -> {
            //sendMail(TrailerDen.ERRORS_MAIL, "WRONG TRAILER : " + id, "WRONG TRAILER : " + id);
            //FirebaseCrash.report(new Exception("ERROR_REPORT -- WRONG TRAILER : " + id));

            Utils.reportTrailerError(id, "WRONG TRAILER");

            UINotifications.showAsSnackbar(clContainer,getString(R.string.report_thxforhelp), Snackbar.LENGTH_SHORT);
            this.dismiss();
        });

        btn_reportPlayer.setOnClickListener(e -> {
//            sendMail(TrailerDen.ERRORS_MAIL, "PLAYER DOES NOT WORK : " + id, "PLAYER DOES NOT WORK : " + id);
            //FirebaseCrash.report(new Exception("ERROR_REPORT -- PLAYER DOES NOT WORK : " + id));

            Utils.reportTrailerError(id, "PLAYER DOES NOT WORK");

            UINotifications.showAsSnackbar(clContainer,getString(R.string.report_thxforhelp), Snackbar.LENGTH_SHORT);
            this.dismiss();
        });

        return v;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface iFace)
    {
        super.onDismiss(iFace);
    }

//    private Session createSessionObject() {
//        Properties properties = new Properties();
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.smtp.host", "smtp.gmail.com");
//        properties.put("mail.smtp.port", "587");
//
//        return Session.getInstance(properties, new javax.mail.Authenticator() {
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(TrailerDen.ERRORS_MAIL, TrailerDen.ERRORS_MAIL_PASS);
//            }
//        });
//    }
//
//    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
//        Message message = new MimeMessage(session);
//        message.setFrom(new InternetAddress(TrailerDen.ERRORS_MAIL));
//        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
//        message.setSubject(subject);
//        message.setText(messageBody);
//        return message;
//    }

//    private void sendMail(String email, String subject, String messageBody) {
//            Observable<String> fetchFromAmazon = Observable.create((Observable.OnSubscribe<String>) subscriber -> {
//                try {
//                    Session session = createSessionObject();
//                    Message message = createMessage(email, subject, messageBody, session);
//                    Transport.send(message);
//
//                }catch(Exception ex){
//                    subscriber.onError(ex); // In case there are network errors
//                }
//            });
//
//            fetchFromAmazon.subscribeOn(Schedulers.newThread()) // Create a new Thread
//                    .observeOn(AndroidSchedulers.mainThread()) // Use the UI thread
//                    .subscribe((Action1<String>) link -> {
//                    });
//
//    }
}

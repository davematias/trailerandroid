package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.GenreCollection;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 08-01-2016.
 */
public interface GenresAPI
{
    @GET("/genres/")
    Observable<GenreCollection> getList(@Query("ordering") String sort, @Query("page") int page, @Query("page_size") int pagesize);


}

package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.Configuration;
import com.grubot.trailerview.entities.ConfigurationCollection;
import com.grubot.trailerview.entities.GenreCollection;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 10-03-2016.
 */
public interface ConfigurationsAPI {

    @GET("/configurations/")
    Observable<ConfigurationCollection> getList(@Query("name") String name, @Query("ordering") String sort, @Query("page") int page, @Query("page_size") int pagesize);

    @GET("/configurations/{id}")
    Observable<Configuration> get(@Path("id") String name);
}

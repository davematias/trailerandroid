package com.grubot.trailerview.ui.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Rating;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.IChangeObserved;
import com.grubot.trailerview.presenters.IMainScreenPresenter;
import com.grubot.trailerview.presenters.MainScreenPresenter;
import com.grubot.trailerview.ui.adapters.GenreAdapter;
import com.grubot.trailerview.ui.dialogs.RatingDialog;
import com.grubot.trailerview.ui.dialogs.ReportErrorDialog;
import com.grubot.trailerview.ui.fragments.FavoritesFragment;
import com.grubot.trailerview.ui.fragments.FragmentDrawer;
import com.grubot.trailerview.ui.fragments.HomeFragment;
import com.grubot.trailerview.ui.fragments.IHomeScreenView;
import com.grubot.trailerview.ui.fragments.ISettingsView;
import com.grubot.trailerview.ui.fragments.SearchFragment;
import com.grubot.trailerview.ui.fragments.SettingsFragment;
import com.grubot.trailerview.utils.CustomTabsActivityHelper;
import com.grubot.trailerview.utils.Utils;

import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.codetail.animation.ViewAnimationUtils;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, IMainScreenView, GoogleApiClient.OnConnectionFailedListener {

    public static String ACTIVITY_BUNDLE_REFRESH_TRAILERS_KEY = "REFRESH";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int RC_SIGN_IN = 9001;

    private static final int RESULT_OK = -1;

    private static final String TAG = "MainActivity";

    private static final String STATE_RESOLVING_ERROR = "resolving_error";

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;

    private GoogleApiClient mGoogleApiClient;

    private GoogleSignInOptions gso;

    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.container_genres)
    RelativeLayout container_genres;

    @Bind(R.id.llGenresError)
    LinearLayout llGenresError;

    @Bind(R.id.rvGenres)
    RecyclerView rvGenres;

    @Bind(R.id.btnReload)
    Button btnReload;

    @Bind(R.id.ivLoadError)
    ImageView ivLoadError;

    @Bind(R.id.ibSelectAll)
    ImageButton ibSelectAll;

    @Bind(R.id.pbReload)
    ProgressBar pbReload;

    DrawerLayout mDrawerLayout;

    private View genresBtnView;

    private FragmentDrawer drawerFragment;
    private TextView mTitle;
    private ProgressBar toolbar_pb;

    private boolean hideMenu;

    private IMainScreenPresenter presenter;

    private GenreAdapter adapter;

    private RealmResults<Genre> genres;

    private RealmResults<User> users;

    private boolean changedGenres = false;

    private boolean lastSelection = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TrailerDen.getInstance().loadNativeAd();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainScreenPresenter(this);

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        if (data != null) {
            String uri = this.getIntent().getDataString();

            uri = uri.replace("http://","").replace("https://","");

            if(uri.endsWith("/"))
                uri = uri.substring(0,uri.length()-1);

            String[] components = uri.split("/");

            if(!(components.length < 2))
                handleAppLink(components);
        }

        ButterKnife.bind(this);



        setSupportActionBar(toolbar);

        try {

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }


        toolbar.setNavigationIcon(R.drawable.ic_home);
        //toolbar.setNavigationIcon(R.mipmap.arrow_white);
        //toolbar.invalidate();       // restore toolbar

        mTitle = ButterKnife.findById(toolbar, R.id.toolbar_title);
        toolbar_pb = ButterKnife.findById(toolbar, R.id.toolbar_pb);

        // Set layout manager to position the items
        rvGenres.setLayoutManager(new LinearLayoutManager(this));

        container_genres.setOnClickListener(e -> reloadTrailers());

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, toolbar);
        drawerFragment.setDrawerListener(this);

        toolbar.setNavigationOnClickListener(e -> drawerFragment.showDrawer());

        btnReload.setOnClickListener(e -> reloadGenres());

        ibSelectAll.setOnClickListener(e -> changeGenreListSelection());

        lastSelection = TrailerDen.getInstance().getBooleanPreference(TrailerDen.PREFERENCE_GENRESELECTION);

        reloadGenres();

        //subscribe to db updates notification
        FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("global");

        //google login setup
/*
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN),new Scope(Scopes.PROFILE),new Scope(Scopes.PLUS_ME))
                .requestEmail().requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        //start periodic sync task
        UserSyncScheduler.scheduleSync();

        //get the user query to monitor if there is a sync thread
        users = presenter.getUserSyncing(new IChangeObserved() {
            @Override
            public void onChange() {

                RealmResults<User> auxUsers = presenter.getUserSyncing(null);
                setSyncIndicator((auxUsers.size() > 0 && auxUsers.get(0).isSyncing()));

            }
        });

        setSyncIndicator((users.size() > 0 && users.get(0).isSyncing()));
`*/
        // display the first navigation drawer view on app launch

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            if(extras.getBoolean(ACTIVITY_BUNDLE_REFRESH_TRAILERS_KEY))
                TrailerDen.getInstance().setTrailerListCurrentItem(-1);
        }

        displayView(0);
    }

    @Override
    public void onStart() {
        super.onStart();

        CustomTabsActivityHelper.bindCustomTabsService(this);
        /*OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            //showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    //hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }*/
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
        // TODO: 17/05/2016 check out to avoid onsaveinstance exceptions, it is happening after trakt login
    }


    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item_genres = menu.findItem(R.id.action_genres);

        //MenuItem item_search = menu.findItem(R.id.action_search);

        item_genres.setVisible(!hideMenu);

        return true;
    }

    @Override
    protected void onDestroy()
    {
        CustomTabsActivityHelper.unbindCustomTabsService(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }

        if(container_genres.getVisibility() == View.VISIBLE) {
            reloadTrailers();
            return;
        }

        if(getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1).getName().toLowerCase().equals(getString(R.string.title_home).toLowerCase()))
        {
            if(TrailerDen.getInstance().getIntPreference("appUsesCounter") >= 10 && !TrailerDen.getInstance().getBooleanPreference("showRatingDialog"))
            {
                openDialog(RatingDialog.newInstance(coordinatorLayout,this), "tag");
            }else {
                finish();
                return;
            }
        } else {
            hideMenu = false;
            invalidateOptionsMenu();
            getFragmentManager().beginTransaction().setCustomAnimations(R.animator.fade_in, R.animator.fade_out).replace(R.id.container_body, new HomeFragment()).addToBackStack("Home").commit();
        }
    }

    private void openDialog(DialogFragment dialog, String dialogTag) {

        try {

            FragmentManager fm = getFragmentManager();
            dialog.show(fm, dialogTag);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        } else if (requestCode == RC_SIGN_IN) {
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if(acct != null)
            {
                TrailerDen.getInstance().saveStringPreference(TrailerDen.PREFERENCE_USER_LOGIN_NAME,acct.getDisplayName());
                TrailerDen.getInstance().saveStringPreference(TrailerDen.PREFERENCE_USER_LOGIN_TYPE,"G");

                User userData = presenter.getCurrentUser();

                if(userData == null)
                {
                    // G+
                    Plus.PeopleApi.load( mGoogleApiClient,acct.getId()).setResultCallback(new ResultCallback<People.LoadPeopleResult>() {
                        @Override
                        public void onResult(@NonNull People.LoadPeopleResult loadPeopleResult) {

                            User newUserData = new User();
                            newUserData.setLocalId(1);

                            newUserData.setExternalId(acct.getEmail());
                            newUserData.setExternalIdType("0");

                            try {

                                Person person = loadPeopleResult.getPersonBuffer().get(0);

                                if(person.getBirthday() != null)
                                {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                    try {
                                        Date bdate = format.parse(person.getBirthday());
                                        newUserData.setAge(Utils.getDiffYears(bdate,new Date()));

                                    } catch (Exception e) {

                                        e.printStackTrace();
                                    }
                                }

                                String gender = "";
                                switch (person.getGender())
                                {
                                    case 0: gender = "M";
                                        break;
                                    case 1: gender = "F";
                                        break;
                                    case 2: gender = "O";
                                        break;
                                }

                                newUserData.setGender(gender);
                                newUserData.setLocation(person.getCurrentLocation());
                                newUserData.setName(person.getDisplayName());


                            }catch (Exception e) {

                                e.printStackTrace();
                            }

                            presenter.copyOrUpdateCurrentUser(newUserData);

                            Fragment frag = getFragmentManager().findFragmentById(R.id.container_body);

                            if(frag != null && frag instanceof ISettingsView)
                            {
                                ((ISettingsView)frag).syncUser();
                            }

                        }
                    });
                }

                Fragment frag = getFragmentManager().findFragmentById(R.id.container_body);

                if(frag != null && frag instanceof ISettingsView)
                {
                    ((ISettingsView)frag).handleSignIn(acct.getDisplayName(),false);
                }

            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_search)
        {
            displayView(1);

            return true;
        }
        */

        if (id == R.id.action_genres)
        {
            genresBtnView = findViewById(R.id.action_genres);

            if(container_genres.getVisibility() != View.VISIBLE)
            {
                //if (adapter == null)
                //{
                genres = presenter.getGenres(new IChangeObserved() {
                    @Override
                    public void onChange()
                    {
                        if(adapter != null)
                            adapter.notifyDataSetChanged();
                    }
                });

                adapter = new GenreAdapter(getApplicationContext(), genres,this);

                rvGenres.setAdapter(adapter);

                if(genres.size() == 0)
                {
                    llGenresError.setVisibility(View.VISIBLE);
                }
                //}
            }

            if(container_genres.getVisibility() == View.VISIBLE)
                reloadTrailers();
            else
            {
                //Animations.expand(container_genres);
                enterReveal();
            }


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void enterReveal() {

        // get the center for the clipping circle
        /*int cx = (container_genres.getLeft() + container_genres.getRight()) / 2;
        int cy = (container_genres.getTop() + container_genres.getBottom()) / 2;

        // get the final radius for the clipping circle
        int dx = Math.max(cx, container_genres.getWidth() - cx);
        int dy = Math.max(cy, container_genres.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        // Android native animator
        //ignore the warning this works with the circular reveal lib
        Animator animator =
                ViewAnimationUtils.createCircularReveal(container_genres, cx, cy, 0, finalRadius);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(500);
        container_genres.setVisibility(View.VISIBLE);
        animator.start();
        */

        int finalRadius = Math.max(container_genres.getWidth(), container_genres.getHeight());

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(container_genres, container_genres.getWidth(), (int) genresBtnView.getY(), 0, finalRadius);
        //anim.setDuration(500);
        // make the view visible and start the animation
        container_genres.setVisibility(View.VISIBLE);
        anim.start();
    }


    private void exitReveal() {
        // previously visible view

        // get the center for the clipping circle
        int cx = container_genres.getMeasuredWidth();
        int cy = (int)genresBtnView.getY();

        // get the initial radius for the clipping circle
        int initialRadius = container_genres.getWidth();

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(container_genres, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                container_genres.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        anim.start();
    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void setSyncIndicator(boolean show){

        if(show)
            toolbar_pb.setVisibility(View.VISIBLE);
        else
            toolbar_pb.setVisibility(View.GONE);
    }

    private void reloadGenres(){

        pbReload.setVisibility(View.VISIBLE);
        btnReload.setVisibility(View.GONE);
        ivLoadError.setVisibility(View.GONE);
        presenter.syncGenres();

    }

    private void changeGenreListSelection()
    {
        changedGenres = true;
        lastSelection = !lastSelection;
        TrailerDen.getInstance().saveBooleanPreference(TrailerDen.PREFERENCE_GENRESELECTION, lastSelection);

        presenter.updateGenres(lastSelection);
    }

    public void handleSyncSuccess()
    {
        llGenresError.setVisibility(View.GONE);
    }

    public void handleSyncError(){

        pbReload.setVisibility(View.GONE);
        btnReload.setVisibility(View.VISIBLE);
        ivLoadError.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateGenre(Genre genre, boolean filterOnTrailer) {

        changedGenres = true;
        presenter.updateGenre(genre, filterOnTrailer);
    }

    @Override
    public void GoogleSignIn() {

            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void GoogleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                        Fragment frag = getFragmentManager().findFragmentById(R.id.container_body);

                        if(status.isSuccess()){

                            if(frag != null && frag instanceof ISettingsView)
                            {
                                ((ISettingsView)frag).handleSignOut();
                            }

                        }
                        else{

                            if(frag != null && frag instanceof ISettingsView)
                            {
                                ((ISettingsView)frag).handleError(status.getStatusMessage());
                            }

                        }

                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void reloadTrailers()
    {
        exitReveal();
        //container_genres.setVisibility(View.INVISIBLE);
        //Animations.collapse(container_genres);
        Fragment frag = getFragmentManager().findFragmentById(R.id.container_body);

        if(frag != null && frag instanceof IHomeScreenView && changedGenres)
        {
            changedGenres = false;
            ((IHomeScreenView)frag).reloadTrailers();
        }
    }

    private void displayView(int position)
    {
        hideMenu = true;
        //hideSearch = false;

        container_genres.setVisibility(View.INVISIBLE);
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.title_home);
                hideMenu = false;
                break;
            case 1:
                fragment = new SearchFragment();
                title = getString(R.string.title_search);
                //hideSearch = true;
                break;
            case 2:
                fragment = new FavoritesFragment();
                title = getString(R.string.title_favorites);
                break;
            case 3:
                fragment = new SettingsFragment();
                title = getString(R.string.title_settings);
                break;
            default:
                break;
        }

        invalidateOptionsMenu();

        if (fragment != null)
        {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(title).commit();

            // set the toolbar title
            mTitle.setText(title);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // getIntent() should always return the most recent
        setIntent(intent);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows usersModel to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GoogleApiAvailability.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }
    }

    // The rest of this code is all about building the error dialog

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    public GoogleSignInOptions getGso() {
        return gso;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GoogleApiAvailability.getInstance().getErrorDialog(
                    this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            //this.onDialogDismissed();
        }
    }

    public void sendSuggestion(View view)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","info@grubot.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Suggestion: ");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    private void handleAppLink(String[] components)
    {
        String kind = "", itemId = "", trailerId = "";

        if(components.length >= 2)
            kind = components[1];

        if(components.length >= 3)
            itemId = components[2];

        if(components.length >= 4)
        {
            trailerId = components[3].replace("?trailerID=","");
        }

        if(kind.isEmpty() && trailerId.isEmpty())
           return;
        else if(!trailerId.isEmpty())
        {
            try {
                long result = Long.parseLong(trailerId);

                presenter.loadTrailer(result);

            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            if(itemId.isEmpty())
               return;
            else{

                if(kind.toLowerCase().equals("movie"))
                    presenter.loadVideoData(itemId, TrailerDen.VIDEO_TYPE.MOVIE);
                else if(kind.toLowerCase().equals("tvshow"))
                    presenter.loadVideoData(itemId, TrailerDen.VIDEO_TYPE.SHOW);
                else if(kind.toLowerCase().equals("game"))
                    presenter.loadVideoData(itemId, TrailerDen.VIDEO_TYPE.GAME);               ;
            }
        }


    }

    @Override
    public void handleTrailerData(Trailer result) {

        Intent resultIntent = new Intent(this, VideoActivity.class);

        resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_TRAILER_KEY, Parcels.wrap((Trailer)result));

        // finalizeAppLink(resultIntent);

        startActivity(resultIntent);
    }

    @Override
    public void handleVideoData(IVideoData result) {

        Intent resultIntent = new Intent(this, VideoActivity.class);

        if(result.getType() == TrailerDen.VIDEO_TYPE.MOVIE)
            resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_MOVIE_KEY, Parcels.wrap((Movie)result));
        else if(result.getType() == TrailerDen.VIDEO_TYPE.GAME)
            resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_GAME_KEY, Parcels.wrap((Game)result));
        else if(result.getType() == TrailerDen.VIDEO_TYPE.SHOW)
            resultIntent.putExtra(VideoActivity.ACTIVITY_BUNDLE_SHOW_KEY, Parcels.wrap((Show)result));

        //finalizeAppLink(resultIntent);

        startActivity(resultIntent);
    }

    @Override
    public void handleAppLinkLoadError(Throwable t) {

    }
}

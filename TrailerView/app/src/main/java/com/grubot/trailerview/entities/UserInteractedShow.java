package com.grubot.trailerview.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 15-04-2016.
 */
public class UserInteractedShow extends RealmObject {

    @PrimaryKey
    private long showId;

    public long getShowId() {
        return showId;
    }

    public void setShowId(long showId) {
        this.showId = showId;
    }
}

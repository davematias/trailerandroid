package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.TrailerCollection;

import java.util.List;

import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by dave on 24-02-2016.
 */
public interface ITrailersModel
{
    void getTrailers(int page, String order, String types, ICallBack<List<Trailer>> callBack);

    void getTrailersByImdbID(String id, ICallBack<List<Trailer>> callBack);

    RealmResults<Trailer> getTrailersByImdbFromCache(String id);

    Observable<TrailerCollection> getTrailersByImdbIDObservable(String id);

    Observable<TrailerCollection> getTrailersByParentSlugDObservable(String parentSlug);

    Trailer getTrailerById(long id);

    void getTrailerById(long id,ICallBack<Trailer> callBack);

    Trailer copyOrUpdate(Trailer trailer);

    Trailer copyIfNew(Trailer trailer);

    void addView(long id, ICallBack<Void> callBack);

    void addLikeOnDb(Trailer trailer);

    void addDislikeOnDb(Trailer trailer);

    void addLikes(long id, ICallBack<Void> callBack);

    void addDislikes(long id, ICallBack<Void> callBack);

    void takeLikes(long id, ICallBack<Void> callBack);

    void takeDislikes(long id, ICallBack<Void> callBack);

    RealmResults<Trailer> getTrailerListCache();

    void clearTrailerListCache();

    void clearVideoTrailerCache(String imdbID);

    void clearResources();
}

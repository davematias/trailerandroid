package com.grubot.trailerview.network;

import android.util.Base64;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.grubot.trailerview.TrailerDen;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by dave on 08-01-2016.
 */
public class ServerManager
{
    private String TAG = "ServerManager";

    public enum MANAGER_TYPE{
        TRAILERS,
        TRAKT
    }

    private OkHttpClient client;
    private Gson gson;
    private Retrofit retrofit;
    private static String traktUserId = null;

    private static ServerManager trailerServerManager;
    private static ServerManager traktServerManager;

    public static ServerManager getInstance(MANAGER_TYPE type)
    {
        switch (type){

            case TRAKT:
                if(traktServerManager == null)
                    traktServerManager = new ServerManager(MANAGER_TYPE.TRAKT);

                return traktServerManager;
            default:
                if(trailerServerManager == null)
                    trailerServerManager = new ServerManager(MANAGER_TYPE.TRAILERS);

                return trailerServerManager;
        }
    }

    private ServerManager(MANAGER_TYPE type)
    {
        client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);
        client.setWriteTimeout(15, TimeUnit.SECONDS);

        GsonBuilder builder = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                });

        switch (type){

            case TRAKT:

                gson = builder.create();

                if(traktUserId != null)
                {
                    Interceptor interceptor = new Interceptor()
                    {
                        @Override
                        public Response intercept(Interceptor.Chain chain) throws IOException
                        {
                            Request original = chain.request();

                            Log.d(TAG, "Sent request to " + original.urlString());

                            Request.Builder requestBuilder = original.newBuilder()
                                    .header("Authorization", "Bearer " +  traktUserId)
                                    .header("trakt-api-version", "2")
                                    .header("trakt-api-key", TrailerDen.TRAKT_API_KEY)
                                    .method(original.method(), original.body());

                            Request request = requestBuilder.build();
                            return chain.proceed(request);

                        }
                    };

                    client.interceptors().add(interceptor);
                }

                retrofit = new Retrofit.Builder()
                        .baseUrl(TrailerDen.TRAKT_API_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client)
                        .build();
                break;
            default:

                Interceptor interceptor = new Interceptor()
                {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException
                    {
                        String credentials = TrailerDen.APPUSER_MAIL + ":" + TrailerDen.APPUSER_PASSWORD;
                        final String basic =
                                "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                        Request original = chain.request();

                        Log.d(TAG, "Sent request to " + original.urlString());

                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Authorization", basic)
                                .method(original.method(), original.body());

                        Request request = requestBuilder.build();
                        return chain.proceed(request);

                    }
                };

                client.interceptors().add(interceptor);

                // Register adapter to builder
                //https://gist.github.com/Retistic/9afece43e3d01f017f8b
                //builder.registerTypeAdapter(UserRealmProxy.class, new UserSerializer());
                //builder.registerTypeAdapter(User.class, new UserSerializer());

                gson = builder.create();

                retrofit = new Retrofit.Builder()
                        .baseUrl(TrailerDen.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client)
                        .build();
        }

    }

    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public static void setTraktUserId(String id) {
        traktUserId = id;
        traktServerManager = null;
    }

    /*
    public class UserSerializer implements JsonSerializer<User> {
        @Override
        public JsonElement serialize(User src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject object = new JsonObject();
            Gson gson = new Gson();
            object.addProperty("name", src.getName());
            object.addProperty("gender", src.getGender());
            object.addProperty("externalIdType", src.getExternalIdType());
            object.addProperty("externalId", src.getExternalId());
            object.addProperty("location", src.getLocation());
            object.addProperty("trailers_liked", gson.toJson(src.getTrailersLiked()));
            object.addProperty("trailers_disliked", gson.toJson(src.getTrailersDisliked()));
            object.addProperty("favorite_games", gson.toJson(src.getFavoriteGames()));
            object.addProperty("favorite_shows", gson.toJson(src.getFavoriteShows()));
            object.addProperty("favorite_movies", gson.toJson(src.getFavoriteMovies()));
            object.addProperty("trailers_viewed", gson.toJson(src.getTrailersViewed()));
            return object;
        }
    }*/
}

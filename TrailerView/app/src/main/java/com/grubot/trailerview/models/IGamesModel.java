package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.GameCollection;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.ShowCollection;
import com.grubot.trailerview.entities.Trailer;

import java.util.List;

import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by dave on 25-02-2016.
 */
public interface IGamesModel
{
    Game getByExternalIDFromDB(String id);

    void updateGame(Game game, boolean isFavorite, boolean isCacheDirty);

    RealmResults<Game> getFavorites(IChangeObserved observer);

    Game copyOrUpdate(Game game);

    void addToGame(Game game, Trailer trailer);

    RealmResults<Game> getUnfavorited();

    Observable<GameCollection> getGameByExternalIDObservable(String param);

    Observable<GameCollection> getGameBySlugObservable(String param);

    Observable<GameCollection> doSearchGameObservable(String id, int page, boolean returnEmpty);

    Observable<Game> getGameByIdObservable(Long id);

    Game getGameByIdFromDB(Long id);

    Game getGameBySlugFromDB(String slug);

    List<Observable<Game>> getGamesByIdObservableList(List<String> ids);

    void clearResources();

    void closeDB();

}

package com.grubot.trailerview.models;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.TrailerCollection;
import com.grubot.trailerview.network.ServerManager;
import com.grubot.trailerview.network.TrailersAPI;
import com.grubot.trailerview.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.SerialSubscription;

/**
 * Created by dave on 24-02-2016.
 */
public class TrailersModel extends BaseModel implements ITrailersModel
{
    //serial subscription discards previous observables on the sabe sub
    private SerialSubscription trailersSub = new SerialSubscription();

    public TrailersModel(CompositeSubscription mCompositeSubscription)
    {
        super(mCompositeSubscription);
    }

    private String getSelectedGenres()
    {
        String genres = "";

        RealmResults<Genre> genresList = realm.where(Genre.class).equalTo("filterOnTrailer",true).findAll();

        for (Genre genre: genresList)
        {
            genres += genre.getName() + ",";
        }

        if(genres.endsWith(","))
            genres = genres.substring(0,genres.length()-1);

        return genres;
    }

    @Override
    public void getTrailers(int page, String order, String types, ICallBack<List<Trailer>> callBack)
    {
        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        Observable<TrailerCollection> callT;

        String genres = getSelectedGenres();

        callT = trailerService.getTrailers(page, order,genres,types);

        trailersSub.set(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<TrailerCollection>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

                if (e instanceof HttpException) {
                    HttpException response = (HttpException) e;

                    if(response.code() == 404)
                        callBack.onSuccess(new ArrayList<>());
                    else
                        callBack.onFailure(e);
                }
                else
                    callBack.onFailure(e);
            }

            @Override
            public void onNext(TrailerCollection trailerCol) {

                if(trailerCol.getCount() > 0)
                {
                    for (Trailer t:trailerCol.getResults()) {

                        Trailer cachedTrailer = getTrailerById(t.getId());

                        if(cachedTrailer != null)
                        {
                            t.setLiked(cachedTrailer.getLiked());
                            t.setVideoCache(cachedTrailer.isVideoCache());
                        }

                        t.setTrailerListCache(true);
                    }

                    realm.executeTransaction(e ->
                    {

                        realm.copyToRealmOrUpdate(trailerCol.getResults());

                    });
                }

                callBack.onSuccess(trailerCol.getResults());
            }
        }));

    }

    @Override
    public void getTrailersByImdbID(String id, ICallBack<List<Trailer>> callBack)
    {
        Observable<TrailerCollection> callT = getTrailersByImdbIDObservable(id);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<TrailerCollection>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                // cast to retrofit.HttpException to get the response code
                if (e instanceof HttpException) {
                    HttpException response = (HttpException) e;
                    int code = response.code();
                }


                callBack.onFailure(e);
            }

            @Override
            public void onNext(TrailerCollection trailerCol) {
                callBack.onSuccess(trailerCol.getResults());
            }
        }));
    }

    @Override
    public RealmResults<Trailer> getTrailersByImdbFromCache(String id) {
        return realm.where(Trailer.class).equalTo("imdbID",id).findAll();
    }

    @Override
    public Observable<TrailerCollection> getTrailersByImdbIDObservable(String id)
    {
        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        return trailerService.filterTrailers(null,null,null,id,null,1000);
    }

    @Override
    public Observable<TrailerCollection> getTrailersByParentSlugDObservable(String parentSlug) {

        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        return trailerService.filterTrailers(null,null,null,null,parentSlug,1000);
    }

    @Override
    public Trailer getTrailerById(long id) {
        return realm.where(Trailer.class).equalTo("id",id).findFirst();
    }

    @Override
    public void getTrailerById(long id, ICallBack<Trailer> callBack) {

        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        Observable<Trailer> callT = trailerService.getTrailer(id);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Trailer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                // cast to retrofit.HttpException to get the response code
                if (e instanceof HttpException) {
                    HttpException response = (HttpException) e;
                    int code = response.code();
                }


                callBack.onFailure(e);
            }

            @Override
            public void onNext(Trailer result) {

                Trailer cachedTrailer = getTrailerById(result.getId());

                if(cachedTrailer != null)
                {
                    result.setLiked(cachedTrailer.getLiked());
                    result.setVideoCache(cachedTrailer.isVideoCache());
                    result.setTrailerListCache(cachedTrailer.isTrailerListCache());
                }

                realm.executeTransaction(e ->
                {
                    realm.copyToRealmOrUpdate(result);

                });

                callBack.onSuccess(result);
            }
        }));
    }

    @Override
    public Trailer copyOrUpdate(Trailer trailer) {

        final Trailer[] result = new Trailer[1];

        realm.executeTransaction(e ->
        {

            result[0] = realm.copyToRealmOrUpdate(trailer);

        });

        return result[0];
    }

    @Override
    public Trailer copyIfNew(Trailer trailer) {

        Trailer auxTrailer = realm.where(Trailer.class).equalTo("id", trailer.getId()).findFirst();

        if(auxTrailer != null)
            return auxTrailer;

        final Trailer[] result = new Trailer[1];

        realm.executeTransaction(e ->
        {

            result[0] = realm.copyToRealm(trailer);

        });

        return result[0];

    }

    @Override
    public void addView(long id, ICallBack<Void> callBack) {

        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        TrailersAPI.AddViewRequest request = new TrailersAPI.AddViewRequest();
        request.addView = true;

        Observable<Trailer> callT = trailerService.addView(id, request);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Trailer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                // cast to retrofit.HttpException to get the response code
                if (e instanceof HttpException) {
                    HttpException response = (HttpException) e;
                    int code = response.code();
                }


                callBack.onFailure(e);
            }

            @Override
            public void onNext(Trailer trailer) {

                realm.executeTransaction(e ->
                {
                    Trailer t = realm.where(Trailer.class).equalTo("id", id).findFirst();

                    t.setViews((t.getViews() + 1));

                });

                callBack.onSuccess(null);
            }
        }));

    }

    @Override
    public void addLikeOnDb(Trailer trailer) {

        realm.executeTransaction(e ->
        {
            if(trailer.getLiked() != null)
            {
                if(trailer.getLiked().booleanValue())
                {
                    trailer.setLiked(null);
                    trailer.setLikes(trailer.getLikes()-1);
                }
                else
                {
                    trailer.setLiked(true);
                    trailer.setDislikes(trailer.getDislikes()-1);
                    trailer.setLikes(trailer.getLikes()+1);
                }
            }
            else
            {
                trailer.setLiked(true);
                trailer.setLikes(trailer.getLikes()+1);
            }

        });
    }

    @Override
    public void addDislikeOnDb(Trailer trailer) {

        realm.executeTransaction(e ->
        {
            if(trailer.getLiked() != null)
            {
                if(trailer.getLiked().booleanValue())
                {
                    trailer.setLiked(false);
                    trailer.setLikes(trailer.getLikes()-1);
                    trailer.setDislikes(trailer.getDislikes()+1);
                }
                else
                {
                    trailer.setLiked(null);
                    trailer.setDislikes(trailer.getDislikes()-1);
                }
            }
            else
            {
                trailer.setLiked(false);
                trailer.setDislikes(trailer.getDislikes()+1);
            }
        });

    }

    @Override
    public void addLikes(long id, ICallBack<Void> callBack) {

        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        TrailersAPI.AddLikesRequest request = new TrailersAPI.AddLikesRequest();
        request.addLike = true;

        Observable<Trailer> callT = trailerService.addLikes(id, request);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Trailer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

                callBack.onFailure(e);
            }

            @Override
            public void onNext(Trailer trailer) {

                callBack.onSuccess(null);
            }
        }));

    }

    @Override
    public void addDislikes(long id, ICallBack<Void> callBack) {

        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        TrailersAPI.AddDisLikesRequest request = new TrailersAPI.AddDisLikesRequest();
        request.addDislike = true;

        Observable<Trailer> callT = trailerService.addDisLikes(id, request);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Trailer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

               Utils.logRetrofitError(e);

                callBack.onFailure(e);
            }

            @Override
            public void onNext(Trailer trailer) {

                callBack.onSuccess(null);
            }
        }));

    }

    @Override
    public void takeLikes(long id, ICallBack<Void> callBack) {

        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        TrailersAPI.TakeLikesRequest request = new TrailersAPI.TakeLikesRequest();
        request.takeLikes = true;

        Observable<Trailer> callT = trailerService.takeLikes(id, request);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Trailer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

                callBack.onFailure(e);
            }

            @Override
            public void onNext(Trailer trailer) {

                callBack.onSuccess(null);
            }
        }));


    }

    @Override
    public void takeDislikes(long id, ICallBack<Void> callBack) {

        TrailersAPI trailerService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(TrailersAPI.class);

        TrailersAPI.TakeDisLikesRequest request = new TrailersAPI.TakeDisLikesRequest();
        request.takeDislikes = true;

        Observable<Trailer> callT = trailerService.takeDisLikes(id, request);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Trailer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

                callBack.onFailure(e);
            }

            @Override
            public void onNext(Trailer trailer) {

                callBack.onSuccess(null);
            }
        }));


    }

    @Override
    public RealmResults<Trailer> getTrailerListCache() {

        RealmResults<Trailer> result = realm.where(Trailer.class).equalTo("isTrailerListCache",true).findAll();

        String currentSort = TrailerDen.getInstance().getCurrentTrailerListSort();

        if(currentSort.toLowerCase().contains("releasedate"))
            result = result.sort(new String[]{"releaseDate","id"}, new Sort[]{Sort.DESCENDING,Sort.DESCENDING});
        else
            result = result.sort(new String[]{"views","id"}, new Sort[]{Sort.DESCENDING,Sort.DESCENDING});

        return result;
    }

    @Override
    public void clearTrailerListCache() {

        realm.executeTransaction(e ->
        {
            realm.where(Trailer.class).equalTo("isTrailerListCache",true).equalTo("isVideoCache",false).findAll().deleteAllFromRealm();

            RealmResults<Trailer> lockedTrailers = realm.where(Trailer.class).equalTo("isTrailerListCache",true).equalTo("isVideoCache",true).findAll();

            for (Trailer t:lockedTrailers) {
                t.setTrailerListCache(false);
            }
        });

    }

    @Override
    public void clearVideoTrailerCache(String imdbID) {

        realm.executeTransaction(e ->
        {
            realm.where(Trailer.class).equalTo("isTrailerListCache",false).equalTo("imdbID",imdbID).findAll().deleteAllFromRealm();

            RealmResults<Trailer> lockedTrailers = realm.where(Trailer.class).equalTo("isTrailerListCache",true).equalTo("imdbID",imdbID).findAll();

            for (Trailer t:lockedTrailers) {
                t.setVideoCache(false);
            }

        });
    }

    @Override
    public void clearResources()
    {
        super.clearResources();
        trailersSub.unsubscribe();
    }
}

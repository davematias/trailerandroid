package com.grubot.trailerview.presenters;

import com.grubot.trailerview.entities.TraktUserList;
import com.grubot.trailerview.entities.TraktUserSettings;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.models.IChangeObserved;
import com.grubot.trailerview.models.TraktModel;
import com.grubot.trailerview.network.TraktAPI;
import com.grubot.trailerview.network.UserSyncScheduler;
import com.grubot.trailerview.ui.fragments.ISettingsView;

import io.realm.RealmResults;

/**
 * Created by dave on 10-04-2016.
 */
public class SettingsPresenter extends BasePresenter implements ISettingsPresenter {

    private ISettingsView view;

    private TraktModel traktModel;

    private TraktAPI.AuthCodeResponse currentAuthCodeData;

    public SettingsPresenter (ISettingsView view){

        this.view = view;
        traktModel = new TraktModel(mCompositeSubscription);
    }

    @Override
    public User createOrUpdateUser(User currentUser) {

        return usersModel.createOrUpdateUser(currentUser);
    }

    @Override
    public void syncUserData() {

        User currentUser = usersModel.getCurrentUserData();

        if(currentUser != null && !currentUser.isSyncing())
        {
            usersModel.setSyncing();

            usersModel.syncUserData(new ICallBack<User>() {
                @Override
                public void onFailure(Throwable error) {

                    usersModel.setSyncError();

                }

                @Override
                public void onSuccess(User arg) {

                    syncRemoteAndDb(arg, new ICallBack<Void>() {
                        @Override
                        public void onFailure(Throwable error) {

                            usersModel.setSyncError();
                            UserSyncScheduler.scheduleSync();
                        }

                        @Override
                        public void onSuccess(Void arg) {

                            usersModel.setSyncDone();

                            UserSyncScheduler.scheduleSync();
                        }
                    });

                }
            });
        }
    }

    @Override
    public void destroyCurrentUser() {

        usersModel.clearUserData();
    }

    @Override
    public void getTraktToken() {

        traktModel.getAccessToken(new ICallBack<TraktAPI.AuthCodeResponse>() {
            @Override
            public void onFailure(Throwable error) {
                view.handleTraktError();
            }

            @Override
            public void onSuccess(TraktAPI.AuthCodeResponse arg) {
                currentAuthCodeData = arg;
                view.requestTraktLogin(arg.verificationUrl,arg.userCode);
            }
        });
    }

    @Override
    public void cancelTraktToken() {
        traktModel.cancelUserAuthToken();
    }

    @Override
    public void poolForToken() {

        traktModel.pollForUserData(currentAuthCodeData.interval, currentAuthCodeData.expiresIn, currentAuthCodeData.deviceCode, new ICallBack<Void>() {
            @Override
            public void onFailure(Throwable error) {
                view.handleTraktError();
            }

            @Override
            public void onSuccess(Void arg) {
               //do nothing, realm is dealing with it on the fragment
            }
        });

    }

    @Override
    public void refreshUserList() {

        traktModel.refreshToken(new ICallBack<Void>() {
            @Override
            public void onFailure(Throwable error) {
                view.handleTraktError();
            }

            @Override
            public void onSuccess(Void arg) {
                traktModel.refreshUserLists(new ICallBack<Void>() {
                    @Override
                    public void onFailure(Throwable error) {
                        view.handleTraktError();
                    }

                    @Override
                    public void onSuccess(Void arg) {
                        //do nothing, refresh is handled on the fragment
                    }
                });
            }
        });

    }

    @Override
    public void deleteTraktData() {
        traktModel.clearTraktData();
    }

    @Override
    public TraktUserSettings getTraktData() {
        return traktModel.getSavedTraktData();
    }

    @Override
    public RealmResults<TraktUserSettings> getTrakSyncing(IChangeObserved observer) {
        return traktModel.getTrakSyncing(observer);
    }

    @Override
    public RealmResults<TraktUserList> getTraktLists(IChangeObserved observer) {
        return traktModel.getTraktLists(observer);
    }

    @Override
    public void updateTraktUser(TraktUserSettings settings, int selectedList) {
        traktModel.updateTraktUser(settings,selectedList);
    }

    @Override
    public void destroySubscription() {

        super.dispose();
        usersModel.clearResources();
        moviesModel.clearResources();
        traktModel.clearResources();
    }
}

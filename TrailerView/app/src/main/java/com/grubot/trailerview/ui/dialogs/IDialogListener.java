package com.grubot.trailerview.ui.dialogs;

import java.util.Objects;

/**
 * Created by dave on 19-04-2016.
 */
public interface IDialogListener {

    void onFinishDialog(String tag, Object result);

}

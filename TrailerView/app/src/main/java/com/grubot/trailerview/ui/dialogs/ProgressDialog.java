package com.grubot.trailerview.ui.dialogs;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.grubot.trailerview.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dave on 19-04-2016.
 */
public class ProgressDialog extends DialogFragment {

    private boolean resultVal = true;

    private String tag;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.btnConfirm)
    Button btnConfirm;

    public ProgressDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static ProgressDialog newInstance(String title, String tag) {
        ProgressDialog frag = new ProgressDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("tag", tag);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.progress_dialog, container);
        ButterKnife.bind(this, v);

        String title = getArguments().getString("title", "");
        String txt = getArguments().getString("text", "");
        tag = getArguments().getString("tag", "");

        tvTitle.setText(title);

        setCancelable(false);

        btnConfirm.setOnClickListener(e -> {
            resultVal = false;
            dismiss();
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDismiss(DialogInterface iFace)
    {
        returnValue();
        super.onDismiss(iFace);
    }




    private void returnValue()
    {
        // Return input text back to activity through the implemented listener
        IDialogListener listener = (IDialogListener) getTargetFragment();
        listener.onFinishDialog(tag, resultVal);
    }

    public void setResultVal(boolean resultVal) {
        this.resultVal = resultVal;
    }
}

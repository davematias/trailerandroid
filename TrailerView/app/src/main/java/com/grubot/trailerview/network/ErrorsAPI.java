package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.Trailer;

import retrofit.http.Body;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by david.matias on 02/08/2016.
 */
public interface ErrorsAPI {

    @POST("/report-error/")
    Observable<Void> reportError(@Body ReportErrorRequest request);

    class ReportErrorRequest
    {
        public String id;
        public String type;
    }

}

package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 30-04-2016.
 */
public class TraktUserList  extends RealmObject {

    @PrimaryKey
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("privacy")
    @Expose
    private String privacy;

    @SerializedName("display_numbers")
    @Expose
    private Boolean displayNumbers;

    @SerializedName("allow_comments")
    @Expose
    private Boolean allowComments;

    @SerializedName("sort_by")
    @Expose
    private String sortBy;

    @SerializedName("sort_how")
    @Expose
    private String sortHow;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("item_count")
    @Expose
    private Integer itemCount;

    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;

    @SerializedName("likes")
    @Expose
    private Integer likes;

    @Ignore
    @SerializedName("ids")
    @Expose
    private Ids ids;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The privacy
     */
    public String getPrivacy() {
        return privacy;
    }

    /**
     * @param privacy The privacy
     */
    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    /**
     * @return The displayNumbers
     */
    public Boolean getDisplayNumbers() {
        return displayNumbers;
    }

    /**
     * @param displayNumbers The display_numbers
     */
    public void setDisplayNumbers(Boolean displayNumbers) {
        this.displayNumbers = displayNumbers;
    }

    /**
     * @return The allowComments
     */
    public Boolean getAllowComments() {
        return allowComments;
    }

    /**
     * @param allowComments The allow_comments
     */
    public void setAllowComments(Boolean allowComments) {
        this.allowComments = allowComments;
    }

    /**
     * @return The sortBy
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * @param sortBy The sort_by
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * @return The sortHow
     */
    public String getSortHow() {
        return sortHow;
    }

    /**
     * @param sortHow The sort_how
     */
    public void setSortHow(String sortHow) {
        this.sortHow = sortHow;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The itemCount
     */
    public Integer getItemCount() {
        return itemCount;
    }

    /**
     * @param itemCount The item_count
     */
    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    /**
     * @return The commentCount
     */
    public Integer getCommentCount() {
        return commentCount;
    }

    /**
     * @param commentCount The comment_count
     */
    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * @return The likes
     */
    public Integer getLikes() {
        return likes;
    }

    /**
     * @param likes The likes
     */
    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    /**
     * @return The ids
     */
    public Ids getIds() {
        return ids;
    }

    /**
     * @param ids The ids
     */
    public void setIds(Ids ids) {
        this.ids = ids;
    }

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    public class Ids {

        @SerializedName("trakt")
        @Expose
        private Integer trakt;
        @SerializedName("slug")
        @Expose
        private String slug;

        /**
         *
         * @return
         * The trakt
         */
        public Integer getTrakt() {
            return trakt;
        }

        /**
         *
         * @param trakt
         * The trakt
         */
        public void setTrakt(Integer trakt) {
            this.trakt = trakt;
        }

        /**
         *
         * @return
         * The slug
         */
        public String getSlug() {
            return slug;
        }

        /**
         *
         * @param slug
         * The slug
         */
        public void setSlug(String slug) {
            this.slug = slug;
        }


    }


}

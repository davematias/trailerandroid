package com.grubot.trailerview.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.ui.activities.IVideoScreenView;
import com.grubot.trailerview.ui.activities.VideoActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dave on 01-03-2016.
 */
public class TrailersListAdapter extends RecyclerView.Adapter<TrailersListAdapter.ViewHolder>
{
    private List<Trailer> trailers;
    private Context ctx;
    private IVideoScreenView videoView;

    public TrailersListAdapter(Context ctx, IVideoScreenView videoView, List<Trailer> trailers)
    {
        this.trailers = trailers;
        this.videoView = videoView;
        this.ctx = ctx;
    }

    @Override
    public int getItemCount() {
        return trailers.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.trailer_list_item_video_activity, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.tvTrailerTitle.setText(trailers.get(i).getTitle());

        Picasso.with(ctx)
                .load(TrailerDen.YOUTUBE_THUMBNAIL_URL + trailers.get(i).getLink() + "/0.jpg")
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(viewHolder.ibPlay);


        viewHolder.ibPlay.setOnClickListener(e -> videoView.playVideo(trailers.get(i)));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View view;

        @Bind(R.id.ibPlay)
        ImageButton ibPlay;

        @Bind(R.id.tvTrailerTitle)
        TextView tvTrailerTitle;

        ViewHolder(View itemView) {
            super(itemView);

            view = itemView;

            ButterKnife.bind(this,itemView);
        }
    }
}

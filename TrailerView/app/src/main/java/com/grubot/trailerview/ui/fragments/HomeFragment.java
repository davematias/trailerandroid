package com.grubot.trailerview.ui.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.presenters.HomeScreenPresenter;
import com.grubot.trailerview.presenters.IHomeScreenPresenter;
import com.grubot.trailerview.ui.activities.VideoActivity;
import com.grubot.trailerview.ui.adapters.TrailerAdapter;
import com.grubot.trailerview.ui.customviews.IStackObserver;
import com.grubot.trailerview.ui.customviews.StackViewVertical;
import com.grubot.trailerview.utils.Animations;
import com.grubot.trailerview.utils.UINotifications;
import com.grubot.trailerview.utils.Utils;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements IHomeScreenView {

    private static final String LATESTORDER  = "-releaseDate,-id";
    private static final String VIEWORDER  = "-views,-id";

    @Bind(R.id.stack)
    StackViewVertical stack;

    @Bind(R.id.tvLatestTrailers)
    TextView tvLatestTrailers;

    @Bind(R.id.tvMostViewed)
    TextView tvMostViewed;

    @Bind(R.id.tvTrailerName)
    TextView tvTrailerName;

    @Bind(R.id.tvTrailerDate)
    TextView tvTrailerDate;

    @Bind(R.id.pbLoad)
    ProgressBar pbInfoLoad;

    @Bind(R.id.pbReload)
    ProgressBar pbReload;

    @Bind(R.id.clContainer)
    CoordinatorLayout clContainer;

    @Bind(R.id.ivError)
    ImageView ivLoadError;

    @Bind(R.id.cbMovies)
    CheckBox cbMovies;

    @Bind(R.id.cbGames)
    CheckBox cbGames;

    @Bind(R.id.cbShows)
    CheckBox cbShows;

    @Bind(R.id.rlNoResults)
    RelativeLayout rlNoResults;

    private TrailerAdapter stackAdapter;

    private IHomeScreenPresenter presenter;

    private Snackbar currentSnack;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new HomeScreenPresenter(this);

        //first run erase cache
        if(TrailerDen.getInstance().getTrailerListCurrentItem() == -1)
        {
            TrailerDen.getInstance().setCurrentTrailerListPage(1);
            TrailerDen.getInstance().setCurrentTrailerListSort(LATESTORDER);
            presenter.clearCache();
        }

       /* final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                TrailerDen.getInstance().loadNativeAd(1);
                TrailerDen.getInstance().loadNativeAd(2);
            }
        },1000);
        */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        View qwe = toolbar.findViewById(R.id.action_genres);
        if(qwe != null) {
            qwe.setVisibility(View.VISIBLE);
        }

        ButterKnife.bind(this, rootView);

        stack.setObserver(new IStackObserver() {
            @Override
            public void stackNavigatedNext(int itemIndex) {
                Log.d("navigatedNext", "item idx " + itemIndex );

                if (stackAdapter != null && stackAdapter.getCount() > 0) {

                    if ((stackAdapter.getCount() - itemIndex) <= 5) {
                        pbInfoLoad.setVisibility(View.VISIBLE);
                        TrailerDen.getInstance().setCurrentTrailerListPage(TrailerDen.getInstance().getCurrentTrailerListPage()+1);
                        presenter.getTrailers(TrailerDen.getInstance().getCurrentTrailerListPage(), TrailerDen.getInstance().getCurrentTrailerListSort(),TrailerDen.getInstance().getStringPreference(TrailerDen.PREFERENCE_TRAILER_TYPE_SELECTION));

                    }
                }

                TrailerDen.getInstance().setTrailerListCurrentItem(itemIndex);
                displayTailer(itemIndex);
            }

            @Override
            public void stackNavigatedPrevious(int itemIndex) {
                Log.d("navigatedPrevious", "item idx " + itemIndex );

                TrailerDen.getInstance().setTrailerListCurrentItem(itemIndex);
                displayTailer(itemIndex);
            }
        });

        stack.setOnItemClickListener((parent, view, position, id) -> showTrailer(view,position) );

        stackAdapter = new TrailerAdapter(getActivity(),new ArrayList<Trailer>());

        pbReload.setVisibility(View.VISIBLE);

        if(TrailerDen.getInstance().getTrailerListCurrentItem() > -1)
        {
            getFromCache();
        }
        else
            presenter.getTrailers(TrailerDen.getInstance().getCurrentTrailerListPage(),TrailerDen.getInstance().getCurrentTrailerListSort(),TrailerDen.getInstance().getStringPreference(TrailerDen.PREFERENCE_TRAILER_TYPE_SELECTION));


        if(TrailerDen.getInstance().getCurrentTrailerListSort().equals(LATESTORDER))
            setLatestTrailersView();
        else
            setMostViewedTrailersView();

        tvMostViewed.setOnClickListener(e ->
        {
            setMostViewedTrailersView();

            reloadTrailers();
        });

        tvLatestTrailers.setOnClickListener(e ->
        {
            setLatestTrailersView();

            reloadTrailers();
        });

        String types = TrailerDen.getInstance().getStringPreference(TrailerDen.PREFERENCE_TRAILER_TYPE_SELECTION);

        if(types == null || types.isEmpty())
        {
            types = "0,1,2";
        }

        cbMovies.setChecked(types.contains("0"));
        cbShows.setChecked(types.contains("1"));
        cbGames.setChecked(types.contains("2"));

        cbGames.setOnCheckedChangeListener((e,b) -> changedTarget(cbGames));

        cbMovies.setOnCheckedChangeListener((e,b) -> changedTarget(cbMovies));

        cbShows.setOnCheckedChangeListener((e,b) -> changedTarget(cbShows));

        // Inflate the layout for this fragment
        return rootView;
    }

    private void setLatestTrailersView()
    {
        TrailerDen.getInstance().setCurrentTrailerListSort(LATESTORDER);

        tvLatestTrailers.setTextColor(getResources().getColor(R.color.normalText));
        tvMostViewed.setTextColor(getResources().getColor(R.color.grayText));
    }

    private void setMostViewedTrailersView()
    {
        TrailerDen.getInstance().setCurrentTrailerListSort(VIEWORDER);

        tvMostViewed.setTextColor(getResources().getColor(R.color.normalText));
        tvLatestTrailers.setTextColor(getResources().getColor(R.color.grayText));
    }

    private void getFromCache()
    {
        presenter.getFromCache();
    }

    private void changedTarget(CheckBox changedCb)
    {
        if(!cbGames.isChecked() && !cbMovies.isChecked() && !cbShows.isChecked())
        {
            changedCb.setChecked(true);
        }

        List<String> newTypes = new ArrayList<>();

        if(cbMovies.isChecked())
            newTypes.add("0");

        if(cbShows.isChecked())
            newTypes.add("1");

        if(cbGames.isChecked())
            newTypes.add("2");

        TrailerDen.getInstance().saveStringPreference(TrailerDen.PREFERENCE_TRAILER_TYPE_SELECTION,TextUtils.join(",",newTypes));

        presenter.removeNonUsedFilters(newTypes);

        reloadTrailers();
    }

    private void showTrailer(View itemView,int position)
    {
        if(stackAdapter != null && stackAdapter.getCount() > 0 && stack.getDisplayedChild() == position)
        {
            ImageView poster = ButterKnife.findById(itemView, R.id.imPoster);

            Intent intent = new Intent(getActivity(), VideoActivity.class);
            intent.putExtra(VideoActivity.ACTIVITY_BUNDLE_TRAILER_KEY, Parcels.wrap(stackAdapter.getItem(position)));

            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(getActivity(), poster, "poster");

            startActivity(intent,options.toBundle());
        }
    }

    public void displayTailer(int idx)
    {
        if (stackAdapter != null && stackAdapter.getCount() > 0) {
            Trailer currentTrailer = stackAdapter.getItem(idx);

            if (tvTrailerDate.getVisibility() != View.VISIBLE)
                tvTrailerDate.setVisibility(View.VISIBLE);

            if (tvTrailerName.getVisibility() != View.VISIBLE)
                tvTrailerName.setVisibility(View.VISIBLE);

            tvTrailerDate.setText(Utils.convertTimestampToDate(currentTrailer.getReleaseDate()));
            tvTrailerName.setText(currentTrailer.getTitle());

        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume()
    {
        super.onResume();

    }

    @Override
    public void onDestroy()
    {
        presenter.destroySubscription();

        super.onDestroy();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void handleTrailerError(Throwable error, String mode)
    {
        try {

            if(TrailerDen.getInstance().getCurrentTrailerListSort().equals(mode))
            {
                TrailerDen.getInstance().setTrailerListCurrentItem(0);
                presenter.clearCache();

                pbInfoLoad.setVisibility(View.INVISIBLE);
                pbReload.setVisibility(View.INVISIBLE);

                if(stackAdapter.getCount() == 0)
                {
                    Animations.crossFade(ivLoadError,pbReload);
                }

                currentSnack = UINotifications.showAsSnackbar(clContainer, getString(R.string.loadTrailersError), getString(R.string.reloadTrailersButton), e -> retryTraileRequest());
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private void retryTraileRequest(){

        if(stack.getVisibility() == View.GONE)
        {
            Animations.crossFade(pbReload,ivLoadError);
        }
        else
        {
            pbInfoLoad.setVisibility(View.VISIBLE);
        }

        presenter.getTrailers(TrailerDen.getInstance().getCurrentTrailerListPage(),TrailerDen.getInstance().getCurrentTrailerListSort(),TrailerDen.getInstance().getStringPreference(TrailerDen.PREFERENCE_TRAILER_TYPE_SELECTION));
    }

    @Override
    public void handleTrailerSuccess(List<Trailer> result,String mode)
    {
        try {

            if(TrailerDen.getInstance().getCurrentTrailerListSort().equals(mode))
            {
                ivLoadError.setVisibility(View.INVISIBLE);

                if(currentSnack != null)
                    currentSnack.dismiss();

                int origCount = stackAdapter.getCount();

                stackAdapter.appendItems(result);
                if(origCount == 0)
                {
                    stack.setAdapter(stackAdapter);
                }

                stackAdapter.notifyDataSetChanged();

                if(origCount == 0 && result.size() == 0)
                {
                    if(rlNoResults.getVisibility() == View.GONE)
                    {
                        Animations.crossFade(rlNoResults,pbReload);
                    }
                }
                else
                {
                    if(stack.getVisibility() == View.GONE)
                    {
                        Animations.crossFade(stack,pbReload);
                    }
                }

                pbInfoLoad.setVisibility(View.INVISIBLE);

            }

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void applyCacheListToStack(List<Trailer> result) {

        if(result.size() == 0) // cache is empty, reload
        {
            prepareReload();

            return;
        }

        int toShow = TrailerDen.getInstance().getTrailerListCurrentItem();

        stackAdapter.appendItems(result);
        stack.setAdapter(stackAdapter);

        stack.setDisplayedChild(toShow);

        //list reverts to index 0 after load so we need to save the old cache index again
        TrailerDen.getInstance().setTrailerListCurrentItem(toShow);
        displayTailer(toShow);

        Animations.crossFade(stack,pbReload);
    }

    public void prepareReload()
    {
        if(currentSnack != null)
        {
            currentSnack.dismiss();
            currentSnack = null;
        }

        ivLoadError.setVisibility(View.GONE);
        rlNoResults.setVisibility(View.GONE);

        Animations.crossFade(pbReload,stack);

        tvTrailerDate.setVisibility(View.INVISIBLE);
        tvTrailerName.setVisibility(View.INVISIBLE);

        TrailerDen.getInstance().setTrailerListCurrentItem(-1);
        presenter.clearCache();
        TrailerDen.getInstance().setCurrentTrailerListPage(1);

        stackAdapter = new TrailerAdapter(getActivity(), new ArrayList<Trailer>());
        stack.setAdapter(stackAdapter);
    }

    @Override
    public void reloadTrailers()
    {
        prepareReload();
        presenter.getTrailers(TrailerDen.getInstance().getCurrentTrailerListPage(),TrailerDen.getInstance().getCurrentTrailerListSort(),TrailerDen.getInstance().getStringPreference(TrailerDen.PREFERENCE_TRAILER_TYPE_SELECTION));
    }

}

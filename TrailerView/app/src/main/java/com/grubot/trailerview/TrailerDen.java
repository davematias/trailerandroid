package com.grubot.trailerview;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.grubot.trailerview.entities.MigrationFactory;

import java.util.Random;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dave on 17-02-2016.
 */
public class TrailerDen extends Application {
    public int getTrailerListCurrentItem() {
        return trailerListCurrentItem;
    }

    public void setTrailerListCurrentItem(int trailerListCurrentItem) {
        this.trailerListCurrentItem = trailerListCurrentItem;
    }

    public enum VIDEO_TYPE {
        MOVIE,
        SHOW,
        GAME
    }

    public static final String ERRORS_MAIL = "trailer.den.errors@gmail.com";
    public static final String ERRORS_MAIL_PASS = "trailer.den.errors951";

    public static final String HEADERTITLE = "papadipopoheader";

    public static NativeContentAd nativeAd = null;
    public static NativeContentAd nativeAd1 = null;
    public static NativeContentAd nativeAd2 = null;

    public static long lastAdCall = 0;

    public static final String BASE_URL = "https://django-trailerden.rhcloud.com";//"https://trailers-app-backend.appspot.com";
    //public static final String BASE_URL = "https://trailers-app-backend.appspot.com";
    public static final String TRAKT_API_URL = "https://api-v2launch.trakt.tv";
    public static final String APPUSER_MAIL = "app@grubot.com";
    public static final String APPUSER_PASSWORD = "93jgoWHJ_%kfjj";

    public static final String YOUTUBE_API_KEY = "1086614786802-73n5psrvs74ne7vef7jjjog7phth9ujk.apps.googleusercontent.com";

    public static final String AD_UNIT_ID = "ca-app-pub-1579183908198460/9999474034";
    //    public static final String NATIVE_AD_UNIT_ID = "/6499/example/native";
    public static final String NATIVE_AD_UNIT_ID = "ca-app-pub-1579183908198460/8925073239";

    public static final String AMAZON_TAG = "grubot-20";

    public static final String TRAKT_API_KEY = "a25071f684892d7692585818427c06321aabd01ac2504dae304794f4aa35792a";
    public static final String TRAKT_API_SECRET = "1f98e541a11ff51188fe6a2504cc85b10137b4d7af317c344f8fe4d094ad9d49";

    public static final String GCM_SENDER_ID = "1086614786802";

    public static final String YOUTUBE_THUMBNAIL_URL = "http://img.youtube.com/vi/";

    public static final String MAIN_PREFERENCES = "TRAILERPREFS";

    public static final String PREFERENCE_SYNCDATE = "LAST_UPDATE";

    public static final String PREFERENCE_SHOWNOTIFICATIONS = "SHOW_NOTIFICATIONS";
    public static final String PREFERENCE_TRAKTKEY = "TRAKT_KEY";
    public static final String PREFERENCE_GENRESELECTION = "LAST_GENRESELECTION";
    public static final String PREFERENCE_SENT_TOKEN_TO_SERVER = "SENT_TOKEN_TO_SERVER";
    public static final String PREFERENCE_FAVORITE_NOTIFICATIONSSENT = "FAVORITE_NOTIFICATIONS";
    public static final String PREFERENCE_ALLSUBS = "ALL_SUBS";
    public static final String PREFERENCE_USER_LOGIN_TYPE = "LOGIN_TYPE";
    public static final String PREFERENCE_USER_LOGIN_NAME = "LOGIN_NAME";
    public static final String PREFERENCE_SEARCH_TYPE_SELECTION = "SEARCH_TYPES_SEL";
    public static final String PREFERENCE_TRAILER_TYPE_SELECTION = "TRAILER_TYPES_SEL";

    public static final int MAX_TRAILER_VIEWS = 4;//0 counts

    public static final String REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE";

    private static TrailerDen singleton;

    public static TrailerDen getInstance() {
        return singleton;
    }

    private int currentTrailerListPage = 0;

    private String currentTrailerListSort = "";

    private int trailerListCurrentItem = -1;

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        Realm.init(this);

        //setup the database configuration
        RealmConfiguration config = new RealmConfiguration.Builder().
                schemaVersion(1) // Must be bumped when the schema changes
                .migration(new MigrationFactory().getMigration()).build();
        Realm.setDefaultConfiguration(config);

    }

    public String getStringPreference(String name) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);
        return settings.getString(name, null);
    }

    public int getIntPreference(String name) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);
        return settings.getInt(name, 0);
    }

    public long getLongPreference(String name) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);
        return settings.getLong(name, 0);
    }

    public Boolean getBooleanPreference(String name) {
        return getBooleanPreference(name, false);
    }

    public Boolean getBooleanPreference(String name, boolean defaultVal) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);
        return settings.getBoolean(name, defaultVal);
    }

    public void saveStringPreference(String name, String value) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);

        // Commit the edits!
        editor.commit();

    }

    public void saveLongPreference(String name, long value) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);

        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(name, value);

        // Commit the edits!
        editor.commit();

    }

    public void saveIntPreference(String name, int value) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);

        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(name, value);

        // Commit the edits!
        editor.commit();

    }

    public void saveBooleanPreference(String name, boolean value) {
        SharedPreferences settings = getSharedPreferences(MAIN_PREFERENCES, 0);

        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(name, value);

        // Commit the edits!
        editor.commit();

    }

    public void loadNativeAd() {

        if (System.currentTimeMillis() - lastAdCall > 3000000) {
            final Observable operationObservable = Observable.create(
                    (Subscriber<Object> subscriber) -> {
                        subscriber.onNext(loadNativeAdImpl());
                        subscriber.onCompleted();
                    })
                    .subscribeOn(Schedulers.trampoline())
                    .observeOn(AndroidSchedulers.mainThread());

            operationObservable.subscribe();
        }
    }

    public Void loadNativeAdImpl() {

        AdLoader.Builder builder = new AdLoader.Builder(getApplicationContext(), TrailerDen.NATIVE_AD_UNIT_ID).withNativeAdOptions(new NativeAdOptions.Builder().setReturnUrlsForImageAssets(true).build());

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("nativeAd", "Failed to load native ad: " + errorCode);
            }
        }).forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
            @Override
            public void onContentAdLoaded(NativeContentAd ad) {
                TrailerDen.nativeAd = ad;
            }
        }).build();

        AdLoader adLoader1 = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("nativeAd", "Failed to load native ad: " + errorCode);
            }
        }).forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
            @Override
            public void onContentAdLoaded(NativeContentAd ad) {
                TrailerDen.nativeAd1 = ad;
            }
        }).build();

        AdLoader adLoader2 = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("nativeAd", "Failed to load native ad: " + errorCode);
            }
        }).forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
            @Override
            public void onContentAdLoaded(NativeContentAd ad) {
                TrailerDen.nativeAd2 = ad;
            }
        }).build();

        //adiciona o teu testdevice a frente do meu
        adLoader.loadAd(new AdRequest.Builder().addTestDevice("B1E5113AF6DE0CC9EB0261A4A3DB909F").addTestDevice("3E4C4732E68D78544E11C16A3E867921").build());
        adLoader1.loadAd(new AdRequest.Builder().addTestDevice("B1E5113AF6DE0CC9EB0261A4A3DB909F").addTestDevice("3E4C4732E68D78544E11C16A3E867921").build());
        adLoader2.loadAd(new AdRequest.Builder().addTestDevice("B1E5113AF6DE0CC9EB0261A4A3DB909F").addTestDevice("3E4C4732E68D78544E11C16A3E867921").build());

        lastAdCall = System.currentTimeMillis();
        return null;
    }

    public NativeContentAd nativeAdPicker() {
        Random rand = new Random(System.currentTimeMillis());
        NativeContentAd ad;
        switch (rand.nextInt((2 - 0) + 1)) {
            case 0:
                ad = nativeAd;
                break;
            case 1:
                if (nativeAd1 != null)
                    ad = nativeAd1;
                else
                    ad = nativeAd;
                break;
            case 2:
                if (nativeAd2 != null)
                    ad = nativeAd2;
                else
                    ad = nativeAd;
                break;
            default:
                ad = nativeAd;
                break;
        }
        return ad;
    }

    public int getCurrentTrailerListPage() {
        return currentTrailerListPage;
    }

    public void setCurrentTrailerListPage(int currentTrailerListPage) {
        this.currentTrailerListPage = currentTrailerListPage;
    }

    public String getCurrentTrailerListSort() {
        return currentTrailerListSort;
    }

    public void setCurrentTrailerListSort(String currentTrailerListSort) {
        this.currentTrailerListSort = currentTrailerListSort;
    }

}

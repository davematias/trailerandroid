package com.grubot.trailerview.entities;

import java.util.ArrayList;
import java.util.List;

public class ShowCollection {

    private long count;
    private List<Show> results = new ArrayList<Show>();

    /**
     * No args constructor for use in serialization
     *
     */
    public ShowCollection() {
    }

    /**
     *
     * @param results
     * @param count
     */
    public ShowCollection(long count, List<Show> results) {
        this.count = count;
        this.results = results;
    }

    /**
     *
     * @return
     * The count
     */
    public long getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The results
     */
    public List<Show> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<Show> results) {
        this.results = results;
    }

}

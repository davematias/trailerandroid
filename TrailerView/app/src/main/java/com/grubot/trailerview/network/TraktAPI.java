package com.grubot.trailerview.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grubot.trailerview.entities.GenreCollection;
import com.grubot.trailerview.entities.TraktUserList;
import com.grubot.trailerview.entities.TraktUserSettings;

import java.util.List;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 08-01-2016.
 */
public interface TraktAPI
{
    @POST("/oauth/device/code")
    Observable<AuthCodeResponse> getAuthCode(@Body AuthCodeRequest request);

    @POST("/oauth/device/token")
    Observable<PoolTokenResponse> getUserToken(@Body AuthTokenRequest request);

    @POST("/oauth/token")
    Observable<TokenRefreshResponse> getRefreshToken(@Body TokenRefreshRequest request);

    @GET("/users/{id}/lists")
    Observable<List<TraktUserList>> getUserLists(@Path("id") String username);

    @GET("/users/settings")
    Observable<SettingsResponse> getUserSettings();

    @POST("/users/{id}/lists/{list}/items")
    Observable<Void> addItemtoList(@Path("id") String username, @Path("list") int listId, @Body ListItemRequest request);

    @POST("/users/{id}/lists/{list}/items/remove")
    Observable<Void> removeItemFromList(@Path("id") String username, @Path("list") int listId, @Body ListItemRequest request);

    class AuthCodeResponse{

        @SerializedName("device_code")
        @Expose
        public String deviceCode;
        @SerializedName("user_code")
        @Expose
        public String userCode;
        @SerializedName("verification_url")
        @Expose
        public String verificationUrl;
        @SerializedName("expires_in")
        @Expose
        public Integer expiresIn;
        @SerializedName("interval")
        @Expose
        public Integer interval;

    }

    class AuthCodeRequest{

        @SerializedName("client_id")
        @Expose
        public String clientId;
    }

    class AuthTokenRequest{

        @SerializedName("code")
        @Expose
        public String code;
        @SerializedName("client_id")
        @Expose
        public String clientId;
        @SerializedName("client_secret")
        @Expose
        public String clientSecret;

    }

    class PoolTokenResponse{

        @SerializedName("access_token")
        @Expose
        public String accessToken;

        @SerializedName("token_type")
        @Expose
        public String tokenType;

        @SerializedName("expires_in")
        @Expose
        public Integer expiresIn;

        @SerializedName("refresh_token")
        @Expose
        public String refreshToken;

        @SerializedName("scope")
        @Expose
        public String scope;

    }

    class SettingsResponse
    {
        @SerializedName("user")
        @Expose
        public TraktUserSettings user;
    }

    class TokenRefreshRequest{

        @SerializedName("refresh_token")
        @Expose
        public String refreshToken;

        @SerializedName("client_id")
        @Expose
        public String clientId;

        @SerializedName("client_secret")
        @Expose
        public String clientSecret;

        @SerializedName("redirect_uri")
        @Expose
        public String redirectUri = "urn:ietf:wg:oauth:2.0:oob";

        @SerializedName("grant_type")
        @Expose
        public String grantType = "refresh_token";

    }

    class TokenRefreshResponse{

        @SerializedName("access_token")
        @Expose
        public String accessToken;

        @SerializedName("token_type")
        @Expose
        public String tokenType;

        @SerializedName("expires_in")
        @Expose
        public Integer expiresIn;

        @SerializedName("refresh_token")
        @Expose
        public String refreshToken;

        @SerializedName("scope")
        @Expose
        public String scope;
    }

    public class ListItemRequest
    {
        @SerializedName("movies")
        @Expose
        public List<TraktItem> movies;

        @SerializedName("shows")
        @Expose
        public List<TraktItem> shows;
    }

    public class TraktItem
    {
        @SerializedName("ids")
        @Expose
        public TraktId ids;
    }

    public class TraktId
    {
        @SerializedName("imdb")
        @Expose
        public String imdb;
    }

}

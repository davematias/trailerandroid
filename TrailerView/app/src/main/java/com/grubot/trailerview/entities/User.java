package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 06-04-2016.
 */
public class User extends RealmObject
{
    private long id;

    @PrimaryKey
    private Integer localId = 0;

    @Ignore
    @SerializedName("trailers_liked")
    @Expose
    private  List<String> trailersLiked = new ArrayList<String>();

    @Ignore
    @SerializedName("trailers_disliked")
    @Expose
    private List<String> trailersDisliked = new ArrayList<String>();

    @Ignore
    @SerializedName("trailers_viewed")
    @Expose
    private List<String> trailersViewed = new ArrayList<String>();

    @Ignore
    @SerializedName("favorite_games")
    @Expose
    private List<String> favoriteGames = new ArrayList<String>();

    @Ignore
    @SerializedName("favorite_shows")
    @Expose
    private List<String> favoriteShows = new ArrayList<String>();

    @Ignore
    @SerializedName("favorite_movies")
    @Expose
    private List<String> favoriteMovies = new ArrayList<String>();

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("externalId")
    @Expose
    private String externalId;

    @SerializedName("externalIdType")
    @Expose
    private String externalIdType;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("age")
    @Expose
    private Integer age = 0;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("updateDate")
    @Expose
    private long updateDate;

    private long lastSyncDateMillis = 0;

    private boolean hasSyncError;

    private boolean isSyncing = false;

    private RealmList<UserInteractedGame> cachedGames;

    private RealmList<UserInteractedMovie> cachedMovies;

    private RealmList<UserInteractedShow> cachedShows;

    private RealmList<UserInteractedTrailer> cachedTrailers;

    /**
     *
     * @return
     * The id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The trailersLiked
     */
    public List<String> getTrailersLiked() {
        return trailersLiked;
    }

    /**
     *
     * @param trailersLiked
     * The trailers_liked
     */
    public void setTrailersLiked(List<String> trailersLiked) {
        this.trailersLiked = trailersLiked;
    }

    /**
     *
     * @return
     * The trailersDisliked
     */
    public List<String> getTrailersDisliked() {
        return trailersDisliked;
    }

    /**
     *
     * @param trailersDisliked
     * The trailers_disliked
     */
    public void setTrailersDisliked(List<String> trailersDisliked) {
        this.trailersDisliked = trailersDisliked;
    }

    /**
     *
     * @return
     * The trailersViewed
     */
    public List<String> getTrailersViewed() {
        return trailersViewed;
    }

    /**
     *
     * @param trailersViewed
     * The trailers_viewed
     */
    public void setTrailersViewed(List<String> trailersViewed) {
        this.trailersViewed = trailersViewed;
    }

    /**
     *
     * @return
     * The favoriteGames
     */
    public List<String> getFavoriteGames() {
        return favoriteGames;
    }

    /**
     *
     * @param favoriteGames
     * The favorite_games
     */
    public void setFavoriteGames(List<String> favoriteGames) {
        this.favoriteGames = favoriteGames;
    }

    /**
     *
     * @return
     * The favoriteShows
     */
    public List<String> getFavoriteShows() {
        return favoriteShows;
    }

    /**
     *
     * @param favoriteShows
     * The favorite_shows
     */
    public void setFavoriteShows(List<String> favoriteShows) {
        this.favoriteShows = favoriteShows;
    }

    /**
     *
     * @return
     * The favoriteMovies
     */
    public List<String> getFavoriteMovies() {
        return favoriteMovies;
    }

    /**
     *
     * @param favoriteMovies
     * The favorite_movies
     */
    public void setFavoriteMovies(List<String> favoriteMovies) {
        this.favoriteMovies = favoriteMovies;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The age
     */
    public Integer getAge() {
        return age;
    }

    /**
     *
     * @param age
     * The age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The updateDate
     */
    public long getUpdateDate() {
        return updateDate;
    }

    /**
     *
     * @param updateDate
     * The updateDate
     */
    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public long getLastSyncDateMillis() {
        return lastSyncDateMillis;
    }

    public void setLastSyncDateMillis(long lastSyncDateMillis) {
        this.lastSyncDateMillis = lastSyncDateMillis;
    }

    public boolean isHasSyncError() {
        return hasSyncError;
    }

    public void setHasSyncError(boolean hasSyncError) {
        this.hasSyncError = hasSyncError;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getExternalIdType() {
        return externalIdType;
    }

    public void setExternalIdType(String externalIdType) {
        this.externalIdType = externalIdType;
    }

    public RealmList<UserInteractedGame> getCachedGames() {
        return cachedGames;
    }

    public void setCachedGames(RealmList<UserInteractedGame> cachedGames) {
        this.cachedGames = cachedGames;
    }

    public RealmList<UserInteractedMovie> getCachedMovies() {
        return cachedMovies;
    }

    public void setCachedMovies(RealmList<UserInteractedMovie> cachedMovies) {
        this.cachedMovies = cachedMovies;
    }

    public RealmList<UserInteractedShow> getCachedShows() {
        return cachedShows;
    }

    public void setCachedShows(RealmList<UserInteractedShow> cachedShows) {
        this.cachedShows = cachedShows;
    }

    public RealmList<UserInteractedTrailer> getCachedTrailers() {
        return cachedTrailers;
    }

    public void setCachedTrailers(RealmList<UserInteractedTrailer> cachedTrailers) {
        this.cachedTrailers = cachedTrailers;
    }

    public boolean isSyncing() {
        return isSyncing;
    }

    public void setSyncing(boolean syncing) {
        isSyncing = syncing;
    }
}

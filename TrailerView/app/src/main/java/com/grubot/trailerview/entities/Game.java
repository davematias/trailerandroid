package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grubot.trailerview.TrailerDen;

import org.parceler.Parcel;
import org.parceler.Transient;

import java.util.ArrayList;
import java.util.List;

import io.realm.GameRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dave on 18/05/2016.
 */
@Parcel(implementations = { GameRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Game.class })
public class Game extends RealmObject implements IVideoData {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private long id;

    private boolean cacheDirty = false;

    private boolean isFavorite = false;

    private long favoriteDateMillis = 0;

    @Transient
    private RealmList<Genre> genresList;

    @Transient
    private RealmList<Trailer> trailers;

    @Transient
    private RealmList<Platform> platformsList;

    @Transient
    private RealmList<Rating> ratingList;

    @Ignore
    @SerializedName("genres")
    @Expose
    private List<String> string_genres = new ArrayList<String>();

    @Ignore
    @SerializedName("ratings")
    @Expose
    private List<String> strings_ratings = new ArrayList<String>();

    @Ignore
    @SerializedName("platforms")
    @Expose
    private List<String> strings_platforms = new ArrayList<String>();

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("externalID")
    @Expose
    private String externalID;

    @SerializedName("plot")
    @Expose
    private String plot;

    @SerializedName("slug")
    @Expose
    private String slug;

    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;

    @SerializedName("updateDate")
    @Expose
    private String updateDate;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("poster")
    @Expose
    private String poster;

    @SerializedName("posterMed")
    @Expose
    private String posterMed;

    @SerializedName("posterSm")
    @Expose
    private String posterSm;

    @SerializedName("studio")
    @Expose
    private String studio;

    /**
     *
     * @return
     * The id
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     *
     * @return
     * The title
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Trailer getFirstTrailer() {

        if(getTrailers().size() == 0)
            return null;

        return getTrailers().first();
    }

    @Override
    public Trailer getTrailer(long id) {
       return getTrailers().where().equalTo("id",id).findFirst();
    }

    /**
     *
     * @return
     * The externalID
     */
    @Override
    public String getExternalID() {
        return externalID;
    }

    @Override
    public String getTraktID() {
        return null;
    }

    /**
     *
     * @param externalID
     * The externalID
     */
    public void setExternalID(String externalID) {
        this.externalID = externalID;
    }

    /**
     *
     * @return
     * The plot
     */
    @Override
    public String getPlot() {
        return plot;
    }

    @Override
    public String getDirector() {
        return null;
    }

    @Override
    public String getCast() {
        return null;
    }

    /**
     *
     * @param plot
     * The plot
     */
    public void setPlot(String plot) {
        this.plot = plot;
    }

    /**
     *
     * @return
     * The releaseDate
     */
    @Override
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     *
     * @param releaseDate
     * The releaseDate
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     *
     * @return
     * The updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     *
     * @param updateDate
     * The updateDate
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     *
     * @return
     * The language
     */
    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public RealmList<Season> getSeasons() {
        return null;
    }

    @Override
    public String getNetwork() {
        return null;
    }

    @Override
    public TrailerDen.VIDEO_TYPE getType() {
        return TrailerDen.VIDEO_TYPE.GAME;
    }



    /**
     *
     * @param language
     * The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     *
     * @return
     * The country
     */
    @Override
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     * The poster
     */
    @Override
    public String getPoster() {
        return poster;
    }

    /**
     *
     * @param poster
     * The poster
     */
    public void setPoster(String poster) {
        this.poster = poster;
    }

    /**
     *
     * @return
     * The posterMed
     */
    @Override
    public String getPosterMed() {
        return posterMed;
    }

    /**
     *
     * @param posterMed
     * The posterMed
     */
    public void setPosterMed(String posterMed) {
        this.posterMed = posterMed;
    }

    /**
     *
     * @return
     * The posterSm
     */
    @Override
    public String getPosterSm() {
        return posterSm;
    }

    /**
     *
     * @param posterSm
     * The posterSm
     */
    public void setPosterSm(String posterSm) {
        this.posterSm = posterSm;
    }

    /**
     *
     * @return
     * The studio
     */
    public String getStudio() {
        return studio;
    }

    /**
     *
     * @param studio
     * The studio
     */
    public void setStudio(String studio) {
        this.studio = studio;
    }

    @Transient
    public RealmList<Genre> getGenresList() {
        return genresList;
    }

    @Transient
    public void setGenresList(RealmList<Genre> genresList) {
        this.genresList = genresList;
    }

    @Override
    @Transient
    public RealmList<Trailer> getTrailers() {
        return trailers;
    }

    @Transient
    public void setTrailers(RealmList<Trailer> trailers) {
        this.trailers = trailers;
    }

    @Transient
    public RealmList<Platform> getPlatformsList() {
        return platformsList;
    }

    @Transient
    public void setPlatformsList(RealmList<Platform> platformsList) {
        this.platformsList = platformsList;
    }

    @Override
    public List<String> getString_genres() {
        return string_genres;
    }

    public void setString_genres(List<String> string_genres) {
        this.string_genres = string_genres;
    }

    public List<String> getStrings_ratings() {
        return strings_ratings;
    }

    public void setgetStrings_ratings(List<String> string_ratings) {
        this.strings_ratings = string_ratings;
    }

    public List<String> getStrings_platforms() {
        return strings_platforms;
    }

    public void setStrings_platforms(List<String> strings_platforms) {
        this.strings_platforms = strings_platforms;
    }

    @Override
    public boolean isCacheDirty() {
        return cacheDirty;
    }

    public void setCacheDirty(boolean cacheDirty) {
        this.cacheDirty = cacheDirty;
    }

    @Override
    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public long getFavoriteDateMillis() {
        return favoriteDateMillis;
    }

    public void setFavoriteDateMillis(long favoriteDateMillis) {
        this.favoriteDateMillis = favoriteDateMillis;
    }

    @Override
    @Transient
    public RealmList<Rating> getRatingList() {
        return ratingList;
    }

    @Transient
    public void setRatingList(RealmList<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    @Override
    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}

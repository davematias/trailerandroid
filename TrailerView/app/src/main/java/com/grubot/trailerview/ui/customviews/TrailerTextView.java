package com.grubot.trailerview.ui.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by dave on 01-03-2016.
 */
public class TrailerTextView extends TextView
{
    public TrailerTextView(Context context, AttributeSet attrs)
    {
        super(context,attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Cuprum-Regular.ttf"));
    }
}

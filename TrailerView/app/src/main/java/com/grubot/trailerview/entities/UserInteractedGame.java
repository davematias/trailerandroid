package com.grubot.trailerview.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 15-04-2016.
 */
public class UserInteractedGame extends RealmObject {

    @PrimaryKey
    private long gameId;

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }
}

package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Transient;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 30-04-2016.
 */
public class TraktUserSettings extends RealmObject {

    @PrimaryKey
    private long id = 1;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("private")
    @Expose
    private Boolean _private;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("vip")
    @Expose
    private Boolean vip;

    @SerializedName("vip_ep")
    @Expose
    private Boolean vipEp;

    @SerializedName("joined_at")
    @Expose
    private String joinedAt;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("age")
    @Expose
    private Integer age;

    private String accessToken;

    private String refreshToken;

    private int selectedList = -1;

    @Transient
    private RealmList<TraktUserList> lists;

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The _private
     */
    public Boolean getPrivate() {
        return _private;
    }

    /**
     *
     * @param _private
     * The private
     */
    public void setPrivate(Boolean _private) {
        this._private = _private;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The vip
     */
    public Boolean getVip() {
        return vip;
    }

    /**
     *
     * @param vip
     * The vip
     */
    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    /**
     *
     * @return
     * The vipEp
     */
    public Boolean getVipEp() {
        return vipEp;
    }

    /**
     *
     * @param vipEp
     * The vip_ep
     */
    public void setVipEp(Boolean vipEp) {
        this.vipEp = vipEp;
    }

    /**
     *
     * @return
     * The joinedAt
     */
    public String getJoinedAt() {
        return joinedAt;
    }

    /**
     *
     * @param joinedAt
     * The joined_at
     */
    public void setJoinedAt(String joinedAt) {
        this.joinedAt = joinedAt;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The about
     */
    public String getAbout() {
        return about;
    }

    /**
     *
     * @param about
     * The about
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The age
     */
    public Integer getAge() {
        return age;
    }

    /**
     *
     * @param age
     * The age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return The id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(long id) {
        this.id = id;
    }

    @Transient
    public RealmList<TraktUserList> getLists() {
        return lists;
    }

    @Transient
    public void setLists(RealmList<TraktUserList> lists) {
        this.lists = lists;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public int getSelectedList() {
        return selectedList;
    }

    public void setSelectedList(int selectedList) {
        this.selectedList = selectedList;
    }
}

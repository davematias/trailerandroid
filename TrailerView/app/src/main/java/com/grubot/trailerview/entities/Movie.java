package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grubot.trailerview.TrailerDen;

import org.parceler.Parcel;
import org.parceler.Transient;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.MovieRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

@Parcel(implementations = { MovieRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Movie.class })
public class Movie extends RealmObject implements IVideoData {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("imdbID")
    @Expose
    private String imdbID;

    @SerializedName("traktID")
    @Expose
    private String traktID;

    @SerializedName("plot")
    @Expose
    private String plot;

    @SerializedName("director")
    @Expose
    private String director;

    @SerializedName("cast")
    @Expose
    private String cast;

    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;

    @SerializedName("updateDate")
    @Expose
    private String updateDate;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("poster")
    @Expose
    private String poster;

    @SerializedName("posterMed")
    @Expose
    private String posterMed;

    @SerializedName("posterSm")
    @Expose
    private String posterSm;

    private boolean cacheDirty = false;

    private boolean isFavorite = false;

    private long favoriteDateMillis = 0;

    @Transient
    private RealmList<Rating> ratingList;

    @Transient
    private RealmList<Genre> genres;

    @Transient
    private RealmList<Trailer> trailers;

    @Ignore
    @SerializedName("movie_genres")
    @Expose
    private List<String> movieGenres = new ArrayList<String>();

    @Ignore
    @SerializedName("ratings")
    @Expose
    private List<String> ratings = new ArrayList<String>();

    @SerializedName("slug")
    @Expose
    private String slug;

    /**
     * No args constructor for use in serialization
     *
     */
    public Movie() {
    }

    /**
     *
     * @param id
     * @param title
     * @param releaseDate
     * @param cast
     * @param poster
     * @param movieGenres
     * @param language
     * @param imdbID
     * @param director
     * @param updateDate
     * @param country
     * @param plot
     */
    public Movie(long id, List<String> movieGenres, String title, String imdbID, String plot, String director, String cast, String releaseDate, String updateDate, String language, String country, String poster) {
        this.id = id;
        this.movieGenres = movieGenres;
        this.title = title;
        this.imdbID = imdbID;
        this.plot = plot;
        this.director = director;
        this.cast = cast;
        this.releaseDate = releaseDate;
        this.updateDate = updateDate;
        this.language = language;
        this.country = country;
        this.poster = poster;
    }

    /**
     *
     * @return
     * The id
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The movieGenres
     */
    @Override
    public List<String> getString_genres()
    {
        if(movieGenres == null || movieGenres.size() == 0)
        {
            for (Genre genre:genres) {

                movieGenres.add(genre.getName());
            }
        }

        return movieGenres;
    }

    /**
     *
     * @return
     * The title
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The imdbID
     */

    public String getImdbID() {
        return imdbID;
    }

    /**
     *
     * @param imdbID
     * The imdbID
     */
    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    /**
     *
     * @return
     * The plot
     */
    @Override
    public String getPlot() {
        return plot;
    }

    /**
     *
     * @param plot
     * The plot
     */
    public void setPlot(String plot) {
        this.plot = plot;
    }

    /**
     *
     * @return
     * The director
     */
    @Override
    public String getDirector() {
        return director;
    }

    /**
     *
     * @param director
     * The director
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     *
     * @return
     * The cast
     */
    @Override
    public String getCast() {
        return cast;
    }

    /**
     *
     * @param cast
     * The cast
     */
    public void setCast(String cast) {
        this.cast = cast;
    }

    /**
     *
     * @return
     * The releaseDate
     */
    @Override
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     *
     * @param releaseDate
     * The releaseDate
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     *
     * @return
     * The updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     *
     * @param updateDate
     * The updateDate
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     *
     * @return
     * The language
     */
    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public RealmList<Season> getSeasons() {
        return null;
    }

    @Override
    public String getNetwork() {
        return null;
    }

    @Override
    public String getStudio() {
        return null;
    }

    public List<String> getStrings_ratings() {
        return ratings;
    }

    public void setStrings_ratings(List<String> ratings) {
        this.ratings = ratings;
    }

    @Override
    public RealmList<Platform> getPlatformsList() {
        return null;
    }

    @Override
    public TrailerDen.VIDEO_TYPE getType() {
        return TrailerDen.VIDEO_TYPE.MOVIE;
    }

    /**
     *
     * @param language
     * The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     *
     * @return
     * The country
     */
    @Override
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public Trailer getFirstTrailer() {

        if(getTrailers().size() == 0)
            return null;

        return getTrailers().first();
    }

    @Override
    public Trailer getTrailer(long id) {
        return getTrailers().where().equalTo("id",id).findFirst();
    }

    /**
     *
     * @return
     * The poster
     */
    @Override
    public String getPoster() {
        return poster;
    }

    @Override
    public String getPosterMed() {
        return posterMed;
    }

    @Override
    public String getPosterSm() {
        return posterSm;
    }

    /**
     *
     * @param poster
     * The poster
     */
    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Transient
    @Override
    public RealmList<Trailer> getTrailers() {
        return trailers;
    }

    @Override
    public String getExternalID() {
        return getImdbID();
    }

    @Override
    public String getTraktID() {
        return traktID;
    }

    @Transient
    public void setTrailers(RealmList<Trailer> trailers) {
        this.trailers = trailers;
    }

    @Override
    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    @Override
    public boolean isCacheDirty() {
        return cacheDirty;
    }

    public void setCacheDirty(boolean cacheDirty) {
        this.cacheDirty = cacheDirty;
    }

    @Transient
    public RealmList<Genre> getGenresList() {
        return genres;
    }

    @Transient
    public void setGenres(RealmList<Genre> genres) {
        this.genres = genres;
    }

    @Override
    public long getFavoriteDateMillis() {
        return favoriteDateMillis;
    }

    public void setFavoriteDateMillis(long favoriteDateMillis) {
        this.favoriteDateMillis = favoriteDateMillis;
    }

    public void setPosterMed(String posterMed) {
        this.posterMed = posterMed;
    }

    public void setPosterSm(String posterSm) {
        this.posterSm = posterSm;
    }

    @Override
    @Transient
    public RealmList<Rating> getRatingList() {
        return ratingList;
    }

    @Transient
    public void setRatingList(RealmList<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    public void setTraktID(String traktID) {
        this.traktID = traktID;
    }

    @Override
    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
package com.grubot.trailerview.entities;

import org.parceler.Parcel;

import io.realm.ConfigurationRealmProxy;
import io.realm.GenreRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 10-03-2016.
 */

@Parcel(implementations = { ConfigurationRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Configuration.class })
public class Configuration extends RealmObject {

    @PrimaryKey
    private long id;

    private String name;

    private String value;

    /**
     * No args constructor for use in serialization
     *
     */
    public Configuration() {
    }

    /**
     *
     * @param id
     * @param name
     * @param value
     */
    public Configuration(long id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    /**
     *
     * @return
     * The id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

}

package com.grubot.trailerview.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grubot.trailerview.entities.GenreCollection;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.entities.UserCollection;

import java.util.ArrayList;
import java.util.List;

import io.realm.annotations.Ignore;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 12-04-2016.
 */
public interface UsersAPI {

    @POST("/users/")
    Observable<User> addData(@Body User user);

    @PATCH("/users/{id}/")
    Observable<User> updateData(@Path("id") long id, @Body UserPatchRequest request);

    @GET("/users/")
    Observable<UserCollection> getList(@Query("externalId") String externalId);

    public class UserPatchRequest
    {
        public List<String> trailers_liked;

        public List<String> trailers_disliked;

        public List<String> trailers_viewed;

        public List<String> favorite_games;

        public List<String> favorite_shows;

        public List<String> favorite_movies;
    }
}

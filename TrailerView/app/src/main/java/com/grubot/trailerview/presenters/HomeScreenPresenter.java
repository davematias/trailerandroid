package com.grubot.trailerview.presenters;

import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.ui.fragments.IHomeScreenView;

import java.util.List;

/**
 * Created by dave on 24-02-2016.
 */
public class HomeScreenPresenter extends BasePresenter implements IHomeScreenPresenter {

    private IHomeScreenView view;

    public HomeScreenPresenter(IHomeScreenView view) {
        this.view = view;
    }

    @Override
    public void getFromCache() {
        view.applyCacheListToStack(trailersModel.getTrailerListCache());
    }

    @Override
    public void clearCache() {
        trailersModel.clearTrailerListCache();
    }

    @Override
    public void getTrailers(int page, String order, String types) {

        trailersModel.getTrailers(page, order,types, new ICallBack<List<Trailer>>() {
            @Override
            public void onFailure(Throwable error) {

                view.handleTrailerError(error, order);

            }

            @Override
            public void onSuccess(List<Trailer> arg) {

                view.handleTrailerSuccess(arg, order);
            }
        });
    }

    @Override
    public void removeNonUsedFilters(List<String> currentFilterList) {
        genreModel.clearGenresNotUsed(currentFilterList);
    }

    @Override
    public void destroySubscription() {
        super.dispose();

    }
}

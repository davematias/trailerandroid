package com.grubot.trailerview.models;

import android.speech.RecognitionService;

import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.Trailer;

import java.util.List;

import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by dave on 25-02-2016.
 */
public interface IMoviesModel
{
    Movie getMovieByImdbIDFromDB(String id);

    void updateMovie(Movie movie, boolean isFavorite, boolean isCacheDirty);

    RealmResults<Movie> getFavorites(IChangeObserved observer);

    Movie copyOrUpdate(Movie movie);

    void addToMovie(Movie movie, Trailer trailer);

    RealmResults<Movie> getUnfavorited();

    Observable<MovieCollection> getMovieByImdbIDObservable(String param);

    Observable<MovieCollection> doSearchMovieObservable(String id, int page, boolean returnEmpty);

    Observable<MovieCollection> getMovieBySlugObservable(String param);

    Observable<Movie> getMovieByIdObservable(Long id);

    Movie getMovieByIdFromDb(Long id);

    Movie getMovieBySlugFromDB(String slug);

    List<Observable<Movie>> getMoviesByIdObservableList(List<String> ids);

    void clearResources();

    void closeDB();

}

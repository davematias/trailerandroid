package com.grubot.trailerview.entities;

import org.parceler.Parcel;

import io.realm.PlatformRealmProxy;
import io.realm.RatingRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by david.matias on 09/06/2016.
 */
@Parcel(implementations = { RatingRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Rating.class })
public class Rating extends RealmObject {

    @PrimaryKey
    private String guid;

    private String name;

    private String rating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}

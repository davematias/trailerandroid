package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.PlatformRealmProxy;
import io.realm.RealmObject;
import io.realm.ShowRealmProxy;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dave on 18/05/2016.
 */
@Parcel(implementations = { PlatformRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Platform.class })
public class Platform extends RealmObject {

    @PrimaryKey
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

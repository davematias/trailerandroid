package com.grubot.trailerview.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.models.IChangeObserved;
import com.grubot.trailerview.presenters.FavoritesPresenter;
import com.grubot.trailerview.presenters.IFavoritesPresenter;
import com.grubot.trailerview.ui.adapters.ContentDataAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FavoritesFragment extends Fragment implements IFavoritesView, IMovieView {

    private String TAG = "FavoritesFragment";

    @Bind(R.id.resultsView)
    RecyclerView rView;

    @Bind(R.id.rlEmpty)
    RelativeLayout rlEmpty;

    private IFavoritesPresenter presenter;

    private ContentDataAdapter adapter;

    private List<IVideoData> res;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    public static FavoritesFragment newInstance() {

        return new FavoritesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        TrailerDen.getInstance().loadNativeAd();
        super.onCreate(savedInstanceState);

        presenter = new FavoritesPresenter(this);

        //presenter.syncUserData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, v);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rView.setLayoutManager(llm);

        res = presenter.getFavorites(new IChangeObserved() {
            @Override
            public void onChange() {

                try {

                    if(adapter != null)
                    {
                        adapter.getData().clear();
                        adapter.getData().addAll(presenter.getFavorites(null));
                        adapter.notifyDataSetChanged();

                        if(adapter.getData().size() == 0)
                            rlEmpty.setVisibility(View.VISIBLE);
                        else
                            rlEmpty.setVisibility(View.GONE);
                    }

                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });

        if(res.size() == 0)
            rlEmpty.setVisibility(View.VISIBLE);
        else {
            if(TrailerDen.nativeAd != null) {
                for (int i = 1; i <= res.size(); i += 10) {
                    res.add(i, null);
                }
            }
            rlEmpty.setVisibility(View.GONE);
        }

        adapter = new ContentDataAdapter(getActivity(),res,this, ContentDataAdapter.MODE.FAVORITES);

        rView.setAdapter(adapter);

        return v;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy()
    {
        presenter.destroySubscription();

        super.onDestroy();
    }

    @Override
    public void updateContext(IVideoData videoContext, boolean isFavorite, boolean isCacheDirty) {
        presenter.updateContext(videoContext,isFavorite,isCacheDirty);
    }
}

package com.grubot.trailerview.ui.fragments;

import com.grubot.trailerview.entities.Trailer;

import java.util.List;

/**
 * Created by dave on 24-02-2016.
 */
public interface IHomeScreenView
{
    void handleTrailerError(Throwable error, String mode);

    void handleTrailerSuccess(List<Trailer> result, String mode);

    void applyCacheListToStack(List<Trailer> result);

    void reloadTrailers();

}

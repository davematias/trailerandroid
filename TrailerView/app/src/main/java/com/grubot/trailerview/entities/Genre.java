package com.grubot.trailerview.entities;


import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import io.realm.GenreRealmProxy;
import io.realm.MovieRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@Parcel(implementations = { GenreRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Genre.class })
public class Genre extends RealmObject {

    @SerializedName("name")
    @PrimaryKey
    private String name;

    @SerializedName("target")
    private int target;

    private boolean filterOnTrailer = false;

    /**
     * No args constructor for use in serialization
     *
     */
    public Genre() {
    }

    /**
     *
     * @param name
     */
    public Genre(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public boolean isFilterOnTrailer() {
        return filterOnTrailer;
    }

    public void setFilterOnTrailer(boolean filterOnTrailer) {
        this.filterOnTrailer = filterOnTrailer;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }
}

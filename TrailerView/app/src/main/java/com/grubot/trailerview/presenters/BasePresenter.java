package com.grubot.trailerview.presenters;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.GameCollection;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.ShowCollection;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.TrailerCollection;
import com.grubot.trailerview.entities.TraktUserSettings;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.entities.UserInteractedTrailer;
import com.grubot.trailerview.models.ConfigurationsModel;
import com.grubot.trailerview.models.GamesModel;
import com.grubot.trailerview.models.GenresModel;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.models.IGamesModel;
import com.grubot.trailerview.models.IMoviesModel;
import com.grubot.trailerview.models.IShowsModel;
import com.grubot.trailerview.models.ITrailersModel;
import com.grubot.trailerview.models.IUsersModel;
import com.grubot.trailerview.models.MoviesModel;
import com.grubot.trailerview.models.ShowsModel;
import com.grubot.trailerview.models.TrailersModel;
import com.grubot.trailerview.models.TraktModel;
import com.grubot.trailerview.models.UsersModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 15-03-2016.
 */
public class BasePresenter implements IBasePresenter
{
    protected CompositeSubscription mCompositeSubscription;

    protected IShowsModel showsModel;
    protected IGamesModel gamesModel;
    protected IMoviesModel moviesModel;
    protected ITrailersModel trailersModel;
    protected IUsersModel usersModel;
    protected TraktModel traktModel;
    protected GenresModel genreModel;
    protected ConfigurationsModel configModel;

    public BasePresenter()
    {
        mCompositeSubscription = new CompositeSubscription();
        moviesModel = new MoviesModel(mCompositeSubscription);
        trailersModel = new TrailersModel(mCompositeSubscription);
        usersModel = new UsersModel(mCompositeSubscription);
        showsModel = new ShowsModel(mCompositeSubscription);
        gamesModel = new GamesModel(mCompositeSubscription);
        traktModel = new TraktModel(mCompositeSubscription);
        genreModel = new GenresModel(mCompositeSubscription);
        configModel = new ConfigurationsModel(mCompositeSubscription);
    }

    public void updateContext(IVideoData videoContext, boolean isFavorite, boolean isCacheDirty) {

        TraktUserSettings settings = traktModel.getSavedTraktData();

        if (settings != null && settings.getSelectedList() > -1)
        {
            multiUpdate(videoContext,isFavorite,isCacheDirty);
        }
        else
        {
//// TODO: 16/05/2016 for now this just works for movies, change afterwards
            // TODO: 16/05/2016 add server refresh
            if(isFavorite)
            {
                if(settings != null)
                {
                    traktModel.addItemToList(videoContext.getExternalID(), new ICallBack<Void>() {
                        @Override
                        public void onFailure(Throwable error) {
                            //view.handleFavoriteError(error);
                            // TODO: 16/05/2016 this should reflect the origin of the error, maybe with a custom throwable
                        }

                        @Override
                        public void onSuccess(Void arg) {
                            multiUpdate(videoContext,isFavorite,isCacheDirty);
                        }
                    });
                }
                else
                    multiUpdate(videoContext,isFavorite,isCacheDirty);

            }
            else
            {
                if(settings != null)
                {
                    traktModel.removeItemFromList(videoContext.getExternalID(), new ICallBack<Void>() {
                        @Override
                        public void onFailure(Throwable error) {
                            //view.handleFavoriteError(error);
                            // TODO: 16/05/2016 this should reflect the origin of the error, maybe with a custom throwable
                        }

                        @Override
                        public void onSuccess(Void arg) {
                            multiUpdate(videoContext,isFavorite,isCacheDirty);
                        }
                    });
                }
                else
                    multiUpdate(videoContext,isFavorite,isCacheDirty);

            }
        }
    }

    private void multiUpdate(IVideoData videoContext, boolean isFavorite, boolean isCacheDirty)
    {
        if(videoContext instanceof Movie)
            moviesModel.updateMovie((Movie)videoContext, isFavorite, isCacheDirty);
        else if(videoContext instanceof Show)
            showsModel.updateShow((Show)videoContext, isFavorite, isCacheDirty);
        else if(videoContext instanceof Game)
            gamesModel.updateGame((Game)videoContext, isFavorite, isCacheDirty);
    }

    @Override
    public void syncRemoteAndDb(User referenceUser, ICallBack<Void> callBack)
    {
        try {

            long syncDate = referenceUser.getUpdateDate();

            List<String> cachedViews = referenceUser.getTrailersViewed();
            List<String> cachedLikes = referenceUser.getTrailersLiked();
            List<String> cachedDislikes = referenceUser.getTrailersDisliked();

            User currentUser = usersModel.getCurrentUserData();

            //merge views
            RealmList<UserInteractedTrailer> cachedTrailers = currentUser.getCachedTrailers();
            for (UserInteractedTrailer trailer: cachedTrailers) {

                String stringId = String.valueOf(trailer.getTrailerId());

                if(!cachedViews.contains(stringId) && trailer.isViewed())
                    cachedViews.add(stringId);
            }

            //add views, likes and dislikes to db
            usersModel.addCachedTrailerData(cachedLikes,cachedDislikes,cachedViews);

            List<String> cachedMovies = referenceUser.getFavoriteMovies();
            List<String> missingMovies = new ArrayList<>(referenceUser.getFavoriteMovies());
            RealmResults<Movie> favorites = moviesModel.getFavorites(null);
            RealmResults<Movie> unfavorited = moviesModel.getUnfavorited();

            for (Movie favMovie: favorites) {

                if(!cachedMovies.contains(String.valueOf(favMovie.getId())))
                {
                    if (favMovie.getFavoriteDateMillis() > syncDate)
                        cachedMovies.add(String.valueOf(favMovie.getId()));
                    else
                        moviesModel.updateMovie(favMovie,false,favMovie.isCacheDirty());
                }
                else
                    missingMovies.remove(String.valueOf(favMovie.getId()));
            }

            for (Movie unFavMovie: unfavorited) {

                if (unFavMovie.getFavoriteDateMillis() > syncDate && cachedMovies.contains(unFavMovie.getId()))
                    cachedMovies.remove(String.valueOf(unFavMovie.getId()));
            }



            usersModel.addFavoriteMovieData(cachedMovies);

            //send synced data to server
            usersModel.sendSyncedState(cachedLikes, cachedDislikes, cachedViews, cachedMovies, new ICallBack<Void>() {
                @Override
                public void onFailure(Throwable error) {
                    callBack.onFailure(error);

                }

                @Override
                public void onSuccess(Void arg) {

                    if(missingMovies.size() > 0)
                    {
                        getMissingComponents(missingMovies,callBack);

                        try {
                            registerPushNotifications();
                        }catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                    else
                        callBack.onSuccess(null);
                }
            });

        }catch (Exception ex){
            ex.printStackTrace();
            callBack.onFailure(ex);
        }

    }

    private void registerPushNotifications(){

        //// TODO: 12/07/2016 when user data sync is done register the notifications for each favorite
    }

    @Override
    public User loadCurrentUser() {
        return usersModel.getCurrentUserData();
    }

    @Override
    public void setUserSyncing() {
        usersModel.setSyncing();
    }

    @Override
    public void setSyncError() {
        usersModel.setSyncError();
    }

    @Override
    public void setSyncDone() {
        usersModel.setSyncDone();
    }

    private void getMissingComponents(List<String> favoriteMovies, ICallBack<Void> callBack){

        mCompositeSubscription.add(Observable.zip(
                moviesModel.getMoviesByIdObservableList(favoriteMovies),
                (movieCol) -> insertComponents(movieCol))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<List<Movie>>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();


                        callBack.onFailure(e);
                    }

                    @Override
                    public void onNext(List<Movie> result) {

                        for (Movie mv: result) {
                            moviesModel.copyOrUpdate(mv);
                        }

                        callBack.onSuccess(null);

                    }
                }));

    }

    private List<Movie> insertComponents(Object[] movies){

        List<Movie> downloadedMovies = new ArrayList<>();

        for (Object movie: movies) {

            Movie mv = ((Movie) movie);

            mv.setIsFavorite(true);
            mv.setFavoriteDateMillis(System.currentTimeMillis());
            downloadedMovies.add(mv);
        }

        return downloadedMovies;
    }

    protected Movie assembleMovieData(MovieCollection movies, TrailerCollection trailers)
    {
        RealmList<Trailer> trasa = new RealmList<>();

        Movie mov = movies.getResults().get(0);

        Collections.sort(trailers.getResults(), (a, b) -> a.getTitle().compareTo(b.getTitle()));

        for (Trailer trailer:trailers.getResults())
        {
            trailer.setVideoCache(true);
            trasa.add(trailer);
        }

        mov.setTrailers(trasa);

        return  mov;
    }

    protected Show assembleShowData(ShowCollection result, TrailerCollection trailers)
    {
        RealmList<Trailer> trasa = new RealmList<>();

        Show data = result.getResults().get(0);

        Collections.sort(trailers.getResults(), (a, b) -> a.getTitle().compareTo(b.getTitle()));

        for (Trailer trailer:trailers.getResults())
        {
            trailer.setVideoCache(true);
            trasa.add(trailer);
        }

        data.setTrailers(trasa);

        return data;
    }

    protected Game assembleGameData(GameCollection result, TrailerCollection trailers)
    {
        RealmList<Trailer> trasa = new RealmList<>();

        Game data = result.getResults().get(0);

        Collections.sort(trailers.getResults(), (a, b) -> a.getTitle().compareTo(b.getTitle()));

        for (Trailer trailer:trailers.getResults())
        {
            trailer.setVideoCache(true);
            trasa.add(trailer);
        }

        data.setTrailers(trasa);

        return data;
    }

    @Override
    public void dispose()
    {
        mCompositeSubscription.unsubscribe();
        moviesModel.clearResources();
        trailersModel.clearResources();
        usersModel.clearResources();
        showsModel.clearResources();
        gamesModel.clearResources();
        traktModel.clearResources();
        genreModel.clearResources();
        configModel.clearResources();
    }
}

package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.GameCollection;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.ShowCollection;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 08-01-2016.
 */
public interface GamesAPI
{
    @GET("/games/")
    Observable<GameCollection> getGames(@Query("ordering") String sort);

    @GET("/games/")
    Observable<GameCollection> searchGames(@Query("search") String param,@Query("pageSize") int pageSize);

    @GET("/games/")
    Observable<GameCollection> filterGames(@Query("ordering") String sort, @Query("language") String language, @Query("country") String country, @Query("externalID") String extermalID, @Query("slug") String slug);

    @GET("/games/")
    Observable<GameCollection> searchGamesByGenre(@Query("ordering") String sort, @Query("searchgenre") String genre);

    @GET("/games/{id}")
    Observable<Game> getGame(@Path("id") long id);
}

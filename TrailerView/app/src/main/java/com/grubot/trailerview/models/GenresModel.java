package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.GenreCollection;
import com.grubot.trailerview.network.GenresAPI;
import com.grubot.trailerview.network.ServerManager;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 25-02-2016.
 */
public class GenresModel extends BaseModel implements IGenresModel {

    public GenresModel(CompositeSubscription mCompositeSubscription)
    {
        super(mCompositeSubscription);
    }

    @Override
    public void syncGenres(ICallBack<Void> callBack)
    {
        GenresAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(GenresAPI.class);

        Observable<GenreCollection> call = apiService.getList("name",1, 1000);
        mCompositeSubscription.add(call.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<GenreCollection>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                // cast to retrofit.HttpException to get the response code
                if (e instanceof HttpException) {
                    HttpException response = (HttpException) e;
                    int code = response.code();

                }

                callBack.onFailure(e);
            }

            @Override
            public void onNext(GenreCollection genreCol) {
                if (genreCol.getCount() > 0)
                {
                    asyncTransactions.add(realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm)
                        {
                           /* for (Genre genre: genreCol.getResults())
                            {
                                genre.setFilterOnTrailer(true); //this doesent come from the server so set the default manually
                            }
                            */
                            bgRealm.copyToRealmOrUpdate(genreCol.getResults());
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            // Transaction was a success.
                            callBack.onSuccess(null);
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            // TODO: 10-03-2016  log error
                            callBack.onFailure(error);
                        }
                    }));
                }
            }
        }));

    }

    @Override
    public RealmResults<Genre> getGenres(IChangeObserved observer,List<String> selectedTypes)
    {
        int typeToLoad = -1;

        if(!selectedTypes.contains("2"))
            typeToLoad = 0; //genre type int doest match trailer type
        else if(selectedTypes.size() == 1)
            typeToLoad = 1;

        RealmResults<Genre> genres;

        if(typeToLoad == -1)
            genres = realm.where(Genre.class).findAll();
        else
            genres = realm.where(Genre.class).equalTo("target",2).or().equalTo("target",typeToLoad).findAll();

        if (observer != null)
        {
            // Tell Realm to notify our listener when the customers results
            // have changed (items added, removed, updated, anything of the sort).
            genres.addChangeListener(new RealmChangeListener<RealmResults<Genre>>() {
                @Override
                public void onChange(RealmResults<Genre> element) {
                    observer.onChange();
                }
            });
        }

        return genres;
    }

    @Override
    public void updateGenre(Genre genre, boolean filterOnTrailer) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                genre.setFilterOnTrailer(filterOnTrailer);
            }
        });
    }

    @Override
    public void updateGenres(boolean filterOnTrailer) {

        RealmResults<Genre> genres = realm.where(Genre.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                for (int i = 0; i < genres.size(); i++) {
                    genres.get(i).setFilterOnTrailer(filterOnTrailer);
                }

            }
        });
    }

    @Override
    public void clearGenresNotUsed(List<String> selectedTypes) {

        int typeToDiscard = -1;

        if(!selectedTypes.contains("2"))
            typeToDiscard = 1; //genre type int doest match trailer type
        else if(selectedTypes.size() == 1)
            typeToDiscard = 0;
        else
            return;

        RealmResults<Genre> genres = realm.where(Genre.class).equalTo("target",typeToDiscard).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                for (int i = 0; i < genres.size(); i++) {
                    genres.get(i).setFilterOnTrailer(false);
                }

            }
        });
    }

}

package com.grubot.trailerview.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 15-04-2016.
 */
public class UserInteractedTrailer extends RealmObject {

    @PrimaryKey
    private long trailerId;

    private boolean isViewed;

    private boolean isLiked;

    private boolean isDisliked;

    public long getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(long trailerId) {
        this.trailerId = trailerId;
    }

    public boolean isViewed() {
        return isViewed;
    }

    public void setViewed(boolean viewed) {
        isViewed = viewed;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public boolean isDisliked() {
        return isDisliked;
    }

    public void setDisliked(boolean disliked) {
        isDisliked = disliked;
    }
}

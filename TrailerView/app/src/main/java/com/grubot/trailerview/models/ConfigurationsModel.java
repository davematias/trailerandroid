package com.grubot.trailerview.models;

import android.util.Log;

import com.grubot.trailerview.entities.Configuration;
import com.grubot.trailerview.entities.ConfigurationCollection;
import com.grubot.trailerview.entities.GenreCollection;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.network.ConfigurationsAPI;
import com.grubot.trailerview.network.GenresAPI;
import com.grubot.trailerview.network.ServerManager;
import com.grubot.trailerview.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmResults;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 10-03-2016.
 */
public class ConfigurationsModel extends BaseModel implements IConfigurationsModel
{
    private final String TAG = "ConfigurationsModel";

    public ConfigurationsModel(CompositeSubscription mCompositeSubscription)
    {
        super(mCompositeSubscription);
    }

    @Override
    public void syncConfigurations(ICallBack<Void> callBack)
    {
        ConfigurationsAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(ConfigurationsAPI.class);

        Observable<ConfigurationCollection> call = apiService.getList(null,"name", 1, 1000);
        mCompositeSubscription.add(call.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<ConfigurationCollection>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);
                callBack.onFailure(e);
            }

            @Override
            public void onNext(ConfigurationCollection col) {
                if (col.getCount() > 0) {
                   asyncTransactions.add(realm.executeTransactionAsync(new Realm.Transaction() {
                       @Override
                       public void execute(Realm bgRealm) {
                           bgRealm.copyToRealmOrUpdate(col.getResults());
                       }
                   }, new Realm.Transaction.OnSuccess() {
                       @Override
                       public void onSuccess() {
                           // Transaction was a success.
                           callBack.onSuccess(null);
                       }
                   }, new Realm.Transaction.OnError() {
                       @Override
                       public void onError(Throwable error) {
                           Log.d(TAG,error.toString());
                           callBack.onFailure(error);
                       }
                   }));
                }
                else
                    callBack.onSuccess(null);
            }
        }));
    }

    @Override
    public void syncConfiguration(String name, ICallBack<Configuration> callBack) {
        ConfigurationsAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(ConfigurationsAPI.class);

        Observable<Configuration> call = apiService.get(name);
        mCompositeSubscription.add(call.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Configuration>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

                Configuration res = realm.where(Configuration.class).equalTo("name",name).findFirst();

                if(res == null)
                    callBack.onFailure(e);
                else
                    callBack.onSuccess(res);
            }

            @Override
            public void onNext(Configuration res) {


                    realm.executeTransaction(e ->
                    {
                      callBack.onSuccess(realm.copyToRealmOrUpdate(res));

                    });

            }
        }));
    }

    @Override
    public Configuration getConfiguration(String name)
    {
       return realm.where(Configuration.class).equalTo("name", name).findFirst();
    }

}

package com.grubot.trailerview.presenters;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.models.ICallBack;

/**
 * Created by dave on 25-02-2016.
 */
public interface IVideoScreenPresenter
{
    void loadVideoContext(String externalId, long trailerId, TrailerDen.VIDEO_TYPE contexType);

    void updateContext(IVideoData videoContext, boolean isFavorite, boolean isCacheDirty);

    void trailerAddView(long id);

    void addLike(Trailer t);

    void addDislike(Trailer t);

    //activities and fragments must call this to clear the observable subscripions from rxjava
    void destroySubscription();
}

package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Configuration;
import com.grubot.trailerview.entities.Genre;

import java.util.List;

/**
 * Created by dave on 10-03-2016.
 */
public interface IConfigurationsModel
{
    void syncConfigurations(ICallBack<Void> callBack);

    void syncConfiguration(String name,ICallBack<Configuration> callBack);

    Configuration getConfiguration(String name);

    void clearResources();
}

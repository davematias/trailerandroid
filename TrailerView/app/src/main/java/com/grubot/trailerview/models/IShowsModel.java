package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.ShowCollection;
import com.grubot.trailerview.entities.Trailer;

import java.util.List;

import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by dave on 25-02-2016.
 */
public interface IShowsModel
{
    Show getShowByExternalIDFromDB(String id);

    void updateShow(Show show, boolean isFavorite, boolean isCacheDirty);

    RealmResults<Show> getFavorites(IChangeObserved observer);

    Show copyOrUpdate(Show show);

    void addToShow(Show show, Trailer trailer);

    RealmResults<Show> getUnfavorited();

    Observable<ShowCollection> getShowByImdbIDObservable(String param);

    Observable<ShowCollection> doSearchShowObservable(String id, int page, boolean returnEmpty);

    Observable<ShowCollection> getShowBySlugObservable(String param);

    Show getShowBySlugFromDB(String slug);

    Observable<Show> getShowByIdObservable(Long id);

    Show getShowByIdFromDb(Long id);

    List<Observable<Show>> getShowsByIdObservableList(List<String> ids);

    void clearResources();

    void closeDB();

}

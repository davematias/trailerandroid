package com.grubot.trailerview.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.grubot.trailerview.R;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.models.IGenresModel;
import com.grubot.trailerview.ui.activities.IMainScreenView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmResults;

/**
 * Created by dave on 12-01-2016.
 */
public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.ViewHolder>
{
    private RealmResults<Genre> genres;
    private Context ctx;
    private IMainScreenView view;

    public GenreAdapter(Context ctx, RealmResults<Genre> genres, IMainScreenView view){
        this.genres = genres;
        this.ctx = ctx;
        this.view = view;
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.genre_list_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        viewHolder.genreTitle.setText(genres.get(i).getName());

        viewHolder.cbGenre.setChecked(genres.get(i).isFilterOnTrailer());
        viewHolder.cbGenre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSelectedGenre(genres.get(i),viewHolder.cbGenre);
            }
        });
        viewHolder.rlGenre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSelectedGenre(genres.get(i),viewHolder.cbGenre);
            }
        });

    }

    private void changeSelectedGenre(Genre genre,RadioButton button)
    {
        button.setChecked(!genre.isFilterOnTrailer());
        view.updateGenre(genre, !genre.isFilterOnTrailer());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.rlGenre)
        RelativeLayout rlGenre;

        @Bind(R.id.genreName)
        TextView genreTitle;

        @Bind(R.id.cbGenre)
        RadioButton cbGenre;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

package com.grubot.trailerview.presenters;

import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.ICallBack;

/**
 * Created by dave on 19-04-2016.
 */
public interface IBasePresenter {

    void syncRemoteAndDb(User referenceUser, ICallBack<Void> callBack);

    User loadCurrentUser();

    void setUserSyncing();

    void setSyncError();

    void setSyncDone();

    void dispose();
}

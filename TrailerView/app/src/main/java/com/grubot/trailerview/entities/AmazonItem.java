package com.grubot.trailerview.entities;

/**
 * Created by Sérgio on 23/05/2016.
 */
public class AmazonItem {

    private String type;
    private String link;
    private String title;

    public AmazonItem(String type, String link,String title) {
        this.setType(type);
        this.setLink(link);
        this.setTitle(title);
    }

    public AmazonItem(){}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

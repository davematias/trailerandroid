package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grubot.trailerview.TrailerDen;

import org.parceler.Parcel;
import org.parceler.Transient;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.ShowRealmProxy;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dave on 18/05/2016.
 */
@Parcel(implementations = { ShowRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Show.class })
public class Show extends RealmObject implements IVideoData {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private long id;

    private boolean cacheDirty = false;

    private boolean isFavorite = false;

    private long favoriteDateMillis = 0;

    @Ignore
    @SerializedName("genres")
    @Expose
    private List<String> string_genres = new ArrayList<String>();

    @Ignore
    @SerializedName("seasons")
    @Expose
    private List<String> strings_seasons = new ArrayList<String>();

    @Ignore
    @SerializedName("ratings")
    @Expose
    private List<String> strings_ratings = new ArrayList<>();

    @Transient
    private RealmList<Genre> genresList;

    @Transient
    private RealmList<Trailer> trailers;

    @Transient
    private RealmList<Season> seasonsList;

    @Transient
    private RealmList<Rating> ratingList;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("slug")
    @Expose
    private String slug;

    @SerializedName("imdbID")
    @Expose
    private String imdbID;

    @SerializedName("traktID")
    @Expose
    private String traktID;

    @SerializedName("plot")
    @Expose
    private String plot;

    @SerializedName("director")
    @Expose
    private String director;

    @SerializedName("cast")
    @Expose
    private String cast;

    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;

    @SerializedName("updateDate")
    @Expose
    private String updateDate;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("poster")
    @Expose
    private String poster;

    @SerializedName("posterMed")
    @Expose
    private String posterMed;

    @SerializedName("posterSm")
    @Expose
    private String posterSm;

    @SerializedName("network")
    @Expose
    private String network;

    /**
     *
     * @return
     * The id
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     *
     * @return
     * The title
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The imdbID
     */
    public String getImdbID() {
        return imdbID;
    }

    /**
     *
     * @param imdbID
     * The imdbID
     */
    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    /**
     *
     * @return
     * The plot
     */
    @Override
    public String getPlot() {
        return plot;
    }

    /**
     *
     * @param plot
     * The plot
     */
    public void setPlot(String plot) {
        this.plot = plot;
    }

    /**
     *
     * @return
     * The director
     */
    @Override
    public String getDirector() {
        return director;
    }

    /**
     *
     * @param director
     * The director
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     *
     * @return
     * The cast
     */
    @Override
    public String getCast() {
        return cast;
    }

    /**
     *
     * @param cast
     * The cast
     */
    public void setCast(String cast) {
        this.cast = cast;
    }

    /**
     *
     * @return
     * The releaseDate
     */
    @Override
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     *
     * @param releaseDate
     * The releaseDate
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     *
     * @return
     * The updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     *
     * @param updateDate
     * The updateDate
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     *
     * @return
     * The language
     */
    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public RealmList<Season> getSeasons() {
        return seasonsList;
    }

    @Override
    public TrailerDen.VIDEO_TYPE getType() {
        return TrailerDen.VIDEO_TYPE.SHOW;
    }

    /**
     *
     * @param language
     * The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     *
     * @return
     * The country
     */
    @Override
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public Trailer getFirstTrailer() {

        if(getTrailers().size() == 0)
            return null;

        return getTrailers().first();
    }

    @Override
    public Trailer getTrailer(long id) {
        return getTrailers().where().equalTo("id",id).findFirst();
    }

    /**
     *
     * @return
     * The poster
     */
    @Override
    public String getPoster() {
        return poster;
    }

    /**
     *
     * @param poster
     * The poster
     */
    public void setPoster(String poster) {
        this.poster = poster;
    }

    /**
     *
     * @return
     * The posterMed
     */
    @Override
    public String getPosterMed() {
        return posterMed;
    }

    /**
     *
     * @param posterMed
     * The posterMed
     */
    public void setPosterMed(String posterMed) {
        this.posterMed = posterMed;
    }

    /**
     *
     * @return
     * The posterSm
     */
    @Override
    public String getPosterSm() {
        return posterSm;
    }

    /**
     *
     * @param posterSm
     * The posterSm
     */
    public void setPosterSm(String posterSm) {
        this.posterSm = posterSm;
    }

    /**
     *
     * @return
     * The network
     */
    public String getNetwork() {
        return network;
    }

    @Override
    public String getStudio() {
        return null;
    }

    @Override
    public RealmList<Platform> getPlatformsList() {
        return null;
    }

    /**
     *
     * @param network
     * The network
     */
    public void setNetwork(String network) {
        this.network = network;
    }

    @Override
    public List<String> getString_genres() {
        return string_genres;
    }

    public void setString_genres(List<String> string_genres) {
        this.string_genres = string_genres;
    }

    public List<String> getStrings_ratings() {

        return strings_ratings;
    }

    public void setStrings_ratings(List<String> string_ratings) {
        this.strings_ratings = string_ratings;
    }

    public List<String> getStrings_seasons() {
        return strings_seasons;
    }

    public void setStrings_seasons(List<String> strings_seasons) {
        this.strings_seasons = strings_seasons;
    }

    @Transient
    public RealmList<Genre> getGenresList() {
        return genresList;
    }

    @Transient
    public void setGenresList(RealmList<Genre> genresList) {
        this.genresList = genresList;
    }

    @Transient
    @Override
    public RealmList<Trailer> getTrailers() {
        return trailers;
    }

    @Override
    public String getExternalID() {
        return getImdbID();
    }

    @Override
    public String getTraktID() {
        return traktID;
    }

    @Transient
    public void setTrailers(RealmList<Trailer> trailers) {
        this.trailers = trailers;
    }

    @Transient
    public RealmList<Season> getSeasonsList() {
        return seasonsList;
    }

    @Transient
    public void setSeasonsList(RealmList<Season> seasonsList) {
        this.seasonsList = seasonsList;
    }

    @Override
    public boolean isCacheDirty() {
        return cacheDirty;
    }

    public void setCacheDirty(boolean cacheDirty) {
        this.cacheDirty = cacheDirty;
    }

    @Override
    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public long getFavoriteDateMillis() {
        return favoriteDateMillis;
    }

    public void setFavoriteDateMillis(long favoriteDateMillis) {
        this.favoriteDateMillis = favoriteDateMillis;
    }

    @Override
    @Transient
    public RealmList<Rating> getRatingList() {
        return ratingList;
    }

    @Transient
    public void setRatingList(RealmList<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    public void setTraktID(String traktID) {
        this.traktID = traktID;
    }

    @Override
    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}

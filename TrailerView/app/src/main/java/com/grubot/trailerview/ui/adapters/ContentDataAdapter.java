package com.grubot.trailerview.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.ui.activities.VideoActivity;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.ui.fragments.IMovieView;
import com.grubot.trailerview.utils.Utils;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmList;

/**
 * Created by dave on 12-01-2016.
 */
public class ContentDataAdapter extends RecyclerView.Adapter<ContentDataAdapter.ViewHolder> {
    public IMovieView getView() {
        return view;
    }

    public List<IVideoData> getData() {
        return data;
    }

    public enum MODE {FAVORITES, SEARCH}

    private List<IVideoData> data;
    private Context ctx;
    private MODE mode;
    private IMovieView view;


    public ContentDataAdapter(Context ctx, List<IVideoData> data, IMovieView view, MODE mode) {
        this.data = data;
        this.ctx = ctx;
        this.mode = mode;
        this.view = view;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ViewHolder vh;

        switch (viewType)
        {
            case 0:
                vh = new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.native_ad_view, viewGroup, false),false);
                break;
            case 2:
                vh = new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_header, viewGroup, false),false);
                break;
            default:
                vh = new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_data_list_item, viewGroup, false),true);
                break;
        }

        return vh;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 1;
        if(data.get(position) == null)
        {
            viewType = 0;
        }else if(data.get(position).getTitle().equals(TrailerDen.HEADERTITLE))
        {
            viewType = 2;
        }
        return viewType;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        IVideoData item = data.get(i);

        if (item == null) {
            if (TrailerDen.nativeAd != null) {
                populateContentAdView(TrailerDen.getInstance().nativeAdPicker(), (NativeContentAdView) viewHolder.view.findViewById(R.id.adView));
            }
        }else if(!item.getTitle().equals(TrailerDen.HEADERTITLE))
        {
            viewHolder.tvMovieName.setText(item.getTitle());
            viewHolder.tvMovieDate.setText(Utils.convertTimestampToDate(item.getReleaseDate()));

            if (mode == MODE.SEARCH)
                viewHolder.ibFavorites.setVisibility(View.GONE);
            else {
                viewHolder.ibFavorites.setVisibility(View.VISIBLE);

                if (item.isFavorite())
                    viewHolder.ibFavorites.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.ic_favorite_white_24dp));
                else
                    viewHolder.ibFavorites.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.ic_favorite_border_white_24dp));

                viewHolder.ibFavorites.setOnClickListener(e ->
                {
                    view.updateContext(item, !item.isFavorite(), item.isCacheDirty());

                    FirebaseMessaging.getInstance().unsubscribeFromTopic(item.getId() + "");
                });
            }

            Picasso.with(ctx)
                    .load(item.getPosterSm())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.ivPoster);


            viewHolder.llgenre.removeAllViews();

            RealmList<Genre> genres = item.getGenresList();

            if (genres != null) {
                for (int j = 0; j < genres.size(); j++) {
                    if (j > 1)
                        break;

                    View v = LayoutInflater.from(ctx).inflate(R.layout.movie_genre_list_item, viewHolder.llgenre);

                    TextView tvGenre = ButterKnife.findById(v, R.id.genreName);
                    tvGenre.setText(genres.get(j).getName());
                }
            } else if (item.getString_genres() != null) {
                for (int j = 0; j < item.getString_genres().size(); j++) {
                    if (j > 1)
                        break;

                    View v = LayoutInflater.from(ctx).inflate(R.layout.movie_genre_list_item, viewHolder.llgenre);

                    TextView tvGenre = ButterKnife.findById(v, R.id.genreName);
                    tvGenre.setText(item.getString_genres().get(j));
                }
            }

            viewHolder.view.setOnClickListener(e -> showVideo(item, viewHolder.ivPoster));
        }
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setAdvertiserView(adView.findViewById(R.id.contentad_advertiser));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((TextView) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

        List<NativeAd.Image> images = nativeContentAd.getImages();

        if (images.size() > 0) {

            Picasso.with(ctx)
                    .load(images.get(0).getUri())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into((ImageView) adView.getImageView());

//            ((ImageView) adView.getImageView()).setImageDrawable(images.get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.
        NativeAd.Image logoImage = nativeContentAd.getLogo();

        if (logoImage == null) {
            adView.getLogoView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(logoImage.getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void showVideo(IVideoData currentItem, View poster) {
        Intent intent = new Intent(ctx, VideoActivity.class);

        switch (currentItem.getType()) {
            case MOVIE:
                intent.putExtra(VideoActivity.ACTIVITY_BUNDLE_MOVIE_KEY, Parcels.wrap((Movie) currentItem));
                break;
            case GAME:
                intent.putExtra(VideoActivity.ACTIVITY_BUNDLE_GAME_KEY, Parcels.wrap((Game) currentItem));
                break;
            case SHOW:
                intent.putExtra(VideoActivity.ACTIVITY_BUNDLE_SHOW_KEY, Parcels.wrap((Show) currentItem));
                break;
        }

        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation((Activity) ctx, poster, "poster");

        ctx.startActivity(intent, options.toBundle());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public NativeContentAdView adView;

        @Bind(R.id.tvMovieName)
        TextView tvMovieName;

        @Bind(R.id.tvMovieDate)
        TextView tvMovieDate;

        @Bind(R.id.ivPoster)
        ImageView ivPoster;

        @Bind(R.id.llgenre)
        LinearLayout llgenre;

        @Bind(R.id.ibFavorites)
        ImageButton ibFavorites;

        ViewHolder(View itemView,boolean isNormal) {
            super(itemView);

            view = itemView;
            if(isNormal)
                ButterKnife.bind(this, itemView);
        }

        ViewHolder(NativeContentAdView itemView) {
            super(itemView);
            adView = itemView;
//            adView.setVisibility(View.GONE);
        }
    }
}

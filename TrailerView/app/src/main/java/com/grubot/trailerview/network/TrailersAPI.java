package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.TrailerCollection;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PATCH;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 08-01-2016.
 */
public interface TrailersAPI
{
    @GET("/trailers/")
    Observable<TrailerCollection> getTrailers(@Query("page") int page,@Query("ordering") String sort,@Query("searchgenre") String searchgenre, @Query("types") String types);

    @GET("/trailers/")
    Observable<TrailerCollection> filterTrailers(@Query("ordering") String sort, @Query("title") String title, @Query("releaseDate") String releaseDate,@Query("imdbID") String imdbId, @Query("parentSlug") String parentSlug, @Query("page_size") int pagesize);

    @GET("/trailers/{id}/")
    Observable<Trailer> getTrailer(@Path("id") long id);

    @PATCH("/trailers/{id}/")
    Observable<Trailer> addView(@Path("id") long id, @Body AddViewRequest request);

    @PATCH("/trailers/{id}/")
    Observable<Trailer> addLikes(@Path("id") long id, @Body AddLikesRequest request);

    @PATCH("/trailers/{id}/")
    Observable<Trailer> addDisLikes(@Path("id") long id, @Body AddDisLikesRequest request);

    @PATCH("/trailers/{id}/")
    Observable<Trailer> takeLikes(@Path("id") long id, @Body TakeLikesRequest request);

    @PATCH("/trailers/{id}/")
    Observable<Trailer> takeDisLikes(@Path("id") long id, @Body TakeDisLikesRequest request);

    public class AddViewRequest
    {
        public boolean addView;
    }

    public class AddLikesRequest
    {
        public boolean addLike;
    }

    public class AddDisLikesRequest
    {
        public boolean addDislike;
    }

    public class TakeLikesRequest
    {
        public boolean takeLikes;
    }

    public class TakeDisLikesRequest
    {
        public boolean takeDislikes;
    }
}

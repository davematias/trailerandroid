package com.grubot.trailerview.presenters;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Configuration;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.GameCollection;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.ShowCollection;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.TrailerCollection;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.ui.activities.IVideoScreenView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmList;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dave on 25-02-2016.
 */
public class VideoScreenPresenter extends BasePresenter implements IVideoScreenPresenter {

    private IVideoScreenView view;

    public VideoScreenPresenter(IVideoScreenView view)
    {
        this.view = view;
    }

    @Override
    public void loadVideoContext(String externalId, long trailerId, TrailerDen.VIDEO_TYPE contexType)
    {
        long lastSync = TrailerDen.getInstance().getLongPreference(TrailerDen.PREFERENCE_SYNCDATE);
        configModel.syncConfiguration(TrailerDen.PREFERENCE_SYNCDATE, new ICallBack<Configuration>() {
            @Override
            public void onFailure(Throwable error) {

                loadFromContext(externalId, trailerId, contexType,false);
            }

            @Override
            public void onSuccess(Configuration arg) {

                long currSync = Long.parseLong(arg.getValue());

                if(currSync == lastSync)
                    loadFromContext(externalId, trailerId, contexType,true);
                else
                    loadFromContext(externalId, trailerId, contexType,false);

                TrailerDen.getInstance().saveLongPreference(TrailerDen.PREFERENCE_SYNCDATE,currSync);
            }
        });
    }

    private void loadFromContext(String externalId, long trailerId, TrailerDen.VIDEO_TYPE contexType, boolean useCache)
    {
        if(contexType == TrailerDen.VIDEO_TYPE.MOVIE)
            loadMovie(externalId, trailerId, useCache);
        else if(contexType == TrailerDen.VIDEO_TYPE.SHOW)
            loadShow(externalId, trailerId, useCache);
        else if(contexType == TrailerDen.VIDEO_TYPE.GAME)
            loadGame(externalId, trailerId, useCache);
    }

    private void loadGame(String id, long trailerId, boolean useCache){

        Game game = gamesModel.getByExternalIDFromDB(id);

        if(game != null && (game.getTrailer(trailerId) != null || trailerId == -1)  && useCache)
        {
            view.handleGetContextSuccess(game);
        }
        else
        {
            mCompositeSubscription.add(Observable.zip(
                    gamesModel.getGameByExternalIDObservable(id),
                    trailersModel.getTrailersByImdbIDObservable(id),
                    (resCol, trailerCol) -> assembleGameData(resCol, trailerCol))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Subscriber<Game>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            view.handleGetContextError(e);
                        }

                        @Override
                        public void onNext(Game result) {

                            if(game != null)
                                result.setFavorite(game.isFavorite());

                            for (Trailer trailer: result.getTrailers()) {
                                Trailer cachedTrailer = trailersModel.getTrailerById(trailer.getId());

                                if(cachedTrailer != null)
                                {
                                    trailer.setLiked(cachedTrailer.getLiked());
                                    trailer.setTrailerListCache(cachedTrailer.isTrailerListCache());
                                }

                            }

                            Game realmGame = gamesModel.copyOrUpdate(result);
                            view.handleGetContextSuccess(realmGame);
                        }
                    }));
        }
    }

    private void loadShow(String id, long trailerId, boolean useCache){
        Show sow = showsModel.getShowByExternalIDFromDB(id);

        if(sow != null && (sow.getTrailer(trailerId) != null || trailerId == -1) && useCache)
        {
            view.handleGetContextSuccess(sow);
        }
        else
        {
            mCompositeSubscription.add(Observable.zip(
                    showsModel.getShowByImdbIDObservable(id),
                    trailersModel.getTrailersByImdbIDObservable(id),
                    (resCol, trailerCol) -> assembleShowData(resCol, trailerCol))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Subscriber<Show>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            view.handleGetContextError(e);
                        }

                        @Override
                        public void onNext(Show result) {

                            if(sow != null)
                                result.setFavorite(sow.isFavorite());

                            for (Trailer trailer: result.getTrailers()) {
                                Trailer cachedTrailer = trailersModel.getTrailerById(trailer.getId());

                                if(cachedTrailer != null)
                                {
                                    trailer.setLiked(cachedTrailer.getLiked());
                                    trailer.setTrailerListCache(cachedTrailer.isTrailerListCache());
                                }

                            }

                            Show realmShow = showsModel.copyOrUpdate(result);
                            view.handleGetContextSuccess(realmShow);
                        }
                    }));
        }
    }

    private void loadMovie(String id, long trailerId, boolean useCache){
        Movie mov = moviesModel.getMovieByImdbIDFromDB(id);

        if(mov != null && (mov.getTrailer(trailerId) != null || trailerId == -1) && useCache)
        {
            view.handleGetContextSuccess(mov);
        }
        else
        {
            mCompositeSubscription.add(Observable.zip(
                    moviesModel.getMovieByImdbIDObservable(id),
                    trailersModel.getTrailersByImdbIDObservable(id),
                    (movieCol, trailerCol) -> assembleMovieData(movieCol, trailerCol))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Subscriber<Movie>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            view.handleGetContextError(e);
                        }

                        @Override
                        public void onNext(Movie result) {

                            if(mov != null)
                                result.setIsFavorite(mov.isFavorite());

                            for (Trailer trailer: result.getTrailers()) {
                                Trailer cachedTrailer = trailersModel.getTrailerById(trailer.getId());

                                if(cachedTrailer != null)
                                {
                                    trailer.setLiked(cachedTrailer.getLiked());
                                    trailer.setTrailerListCache(cachedTrailer.isTrailerListCache());
                                }

                            }

                            Movie realmMovie = moviesModel.copyOrUpdate(result);
                            view.handleGetContextSuccess(realmMovie);
                        }
                    }));
        }
    }

    @Override
    public void trailerAddView(long id) {

        trailersModel.addView(id, new ICallBack<Void>() {
            @Override
            public void onFailure(Throwable error) {

            }

            @Override
            public void onSuccess(Void arg) {

            }
        });
    }

    @Override
    public void addLike(Trailer t) {

        if(t.getLiked() != null)
        {
            if(t.getLiked().booleanValue())
            {
                trailersModel.takeLikes(t.getId(), new ICallBack<Void>() {
                    @Override
                    public void onFailure(Throwable error) {

                    }

                    @Override
                    public void onSuccess(Void arg) {

                    }
                });
            }
            else
            {
                trailersModel.takeDislikes(t.getId(), new ICallBack<Void>() {
                    @Override
                    public void onFailure(Throwable error) {

                    }

                    @Override
                    public void onSuccess(Void arg) {

                    }
                });
            }
        }
        else
        {
            trailersModel.addLikes(t.getId(), new ICallBack<Void>() {
                @Override
                public void onFailure(Throwable error) {

                }

                @Override
                public void onSuccess(Void arg) {


                }
            });
        }

        trailersModel.addLikeOnDb(t);
    }

    @Override
    public void addDislike(Trailer t) {

        if(t.getLiked() != null)
        {
            if(t.getLiked().booleanValue())
            {
                trailersModel.takeLikes(t.getId(), new ICallBack<Void>() {
                    @Override
                    public void onFailure(Throwable error) {

                    }

                    @Override
                    public void onSuccess(Void arg) {

                    }
                });
            }
            else
            {
                trailersModel.takeDislikes(t.getId(), new ICallBack<Void>() {
                    @Override
                    public void onFailure(Throwable error) {

                    }

                    @Override
                    public void onSuccess(Void arg) {

                    }
                });
            }
        }
        else
        {
            trailersModel.addDislikes(t.getId(), new ICallBack<Void>() {
                @Override
                public void onFailure(Throwable error) {

                }

                @Override
                public void onSuccess(Void arg) {


                }
            });
        }

        trailersModel.addDislikeOnDb(t);

    }

    @Override
    public void destroySubscription()
    {
        super.dispose();
    }

}

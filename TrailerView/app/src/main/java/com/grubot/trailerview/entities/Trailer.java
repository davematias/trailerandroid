package com.grubot.trailerview.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grubot.trailerview.TrailerDen;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;
import io.realm.TrailerRealmProxy;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

@Parcel(implementations = { TrailerRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Trailer.class })
public class Trailer extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("views")
    @Expose
    private long views;

    @SerializedName("likes")
    @Expose
    private long likes;

    @SerializedName("dislikes")
    @Expose
    private long dislikes;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("link")
    @Expose
    private String link;

    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;

    @SerializedName("hash")
    @Expose
    private String hash;

    @SerializedName("imdbID")
    @Expose
    private String imdbID;

    @SerializedName("poster")
    @Expose
    private String poster;

    @SerializedName("posterMed")
    @Expose
    private String posterMed;

    @SerializedName("posterSm")
    @Expose
    private String posterSm;

    @Ignore
    @SerializedName("genres")
    @Expose
    private List<String> genres = new ArrayList<String>();

    @SerializedName("type")
    @Expose
    private int type = 0;

    @SerializedName("parentSlug")
    @Expose
    private String parentSlug;

    private Boolean liked = null;

    private boolean isTrailerListCache = false;

    private boolean isVideoCache = false;

    /**
     * No args constructor for use in serialization
     *
     */
    public Trailer() {
    }

    /**
     *
     * @param id
     * @param title
     * @param releaseDate
     * @param genres
     * @param hash
     * @param views
     * @param link
     * @param likes
     * @param poster
     * @param imdbID
     * @param dislikes
     */
    public Trailer(long id, long views, long likes, long dislikes, List<String> genres, String title, String link, String releaseDate, String hash, String imdbID, String poster) {
        this.id = id;
        this.views = views;
        this.likes = likes;
        this.dislikes = dislikes;
        this.genres = genres;
        this.title = title;
        this.link = link;
        this.releaseDate = releaseDate;
        this.hash = hash;
        this.imdbID = imdbID;
        this.poster = poster;
    }

    /**
     *
     * @return
     * The id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The views
     */
    public long getViews() {
        return views;
    }

    /**
     *
     * @param views
     * The views
     */
    public void setViews(long views) {
        this.views = views;
    }

    /**
     *
     * @return
     * The likes
     */
    public long getLikes() {
        return likes;
    }

    /**
     *
     * @param likes
     * The likes
     */
    public void setLikes(long likes) {
        this.likes = likes;
    }

    /**
     *
     * @return
     * The dislikes
     */
    public long getDislikes() {
        return dislikes;
    }

    /**
     *
     * @param dislikes
     * The dislikes
     */
    public void setDislikes(long dislikes) {
        this.dislikes = dislikes;
    }

    /**
     *
     * @return
     * The genres
     */
    public List<String> getGenres() {
        return genres;
    }

    /**
     *
     * @param genres
     * The genres
     */
    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     * The releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     *
     * @param releaseDate
     * The releaseDate
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     *
     * @return
     * The hash
     */
    public String getHash() {
        return hash;
    }

    /**
     *
     * @param hash
     * The hash
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     *
     * @return
     * The imdbID
     */
    public String getImdbID() {
        return imdbID;
    }

    /**
     *
     * @param imdbID
     * The imdbID
     */
    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    /**
     *
     * @return
     * The poster
     */
    public String getPoster() {
        return poster;
    }

    /**
     *
     * @param poster
     * The poster
     */
    public void setPoster(String poster) {
        this.poster = poster;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPosterMed() {
        return posterMed;
    }

    public void setPosterMed(String posterMed) {
        this.posterMed = posterMed;
    }

    public String getPosterSm() {
        return posterSm;
    }

    public void setPosterSm(String posterSm) {
        this.posterSm = posterSm;
    }

    public TrailerDen.VIDEO_TYPE getTypeAsEnum()
    {
        switch (getType())
        {
            case 1: return TrailerDen.VIDEO_TYPE.SHOW;
            case 2: return TrailerDen.VIDEO_TYPE.GAME;
            default:return TrailerDen.VIDEO_TYPE.MOVIE;
        }
    }

    public String getTypeAsString()
    {
        switch (getType())
        {
            case 1: return "show";
            case 2: return "game";
            default:return "movie";
        }
    }

    public boolean isTrailerListCache() {
        return isTrailerListCache;
    }

    public void setTrailerListCache(boolean trailerListCache) {
        isTrailerListCache = trailerListCache;
    }

    public boolean isVideoCache() {
        return isVideoCache;
    }

    public void setVideoCache(boolean videoCache) {
        isVideoCache = videoCache;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public String getParentSlug() {
        return parentSlug;
    }

    public void setParentSlug(String parentSlug) {
        this.parentSlug = parentSlug;
    }

    public String getWebLink(){
        return "http://www.trailer-den.com/" + getTypeAsString() + "/"  + parentSlug + "/?trailerID=" + id;
    }
}

package com.grubot.trailerview.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dave on 15-04-2016.
 */
public class UserInteractedMovie extends RealmObject {

    @PrimaryKey
    private long movieId;

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }
}

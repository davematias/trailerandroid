package com.grubot.trailerview.models;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.TraktUserList;
import com.grubot.trailerview.entities.TraktUserSettings;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.network.ServerManager;
import com.grubot.trailerview.network.TraktAPI;
import com.grubot.trailerview.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 22-04-2016.
 */
public class TraktModel extends BaseModel implements ITraktModel {

    private Subscription getAuthCodesubscription;

    public TraktModel(CompositeSubscription mCompositeSubscription) {
        super(mCompositeSubscription);
    }

    @Override
    public void getAccessToken(ICallBack<TraktAPI.AuthCodeResponse> callBack) {

        TraktAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAKT).getRetrofit().create(TraktAPI.class);

        TraktAPI.AuthCodeRequest request = new TraktAPI.AuthCodeRequest();
        request.clientId = TrailerDen.TRAKT_API_KEY;

        Observable<TraktAPI.AuthCodeResponse> call = apiService.getAuthCode(request);
        getAuthCodesubscription =call.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<TraktAPI.AuthCodeResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

                callBack.onFailure(e);
            }

            @Override
            public void onNext(TraktAPI.AuthCodeResponse result) {

                callBack.onSuccess(result);

            }
        });

        mCompositeSubscription.add(getAuthCodesubscription);
    }

    @Override
    public void cancelUserAuthToken() {

        if(getAuthCodesubscription != null && !getAuthCodesubscription.isUnsubscribed())
            getAuthCodesubscription.unsubscribe();

    }

    @Override
    public void pollForUserData(int interval, int expireSeconds, String deviceCode, ICallBack<Void> callBack) {

        TraktAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAKT).getRetrofit().create(TraktAPI.class);

        TraktAPI.AuthTokenRequest request = new TraktAPI.AuthTokenRequest();
        request.clientId = TrailerDen.TRAKT_API_KEY;
        request.clientSecret = TrailerDen.TRAKT_API_SECRET;
        request.code = deviceCode;

        Observable<TraktAPI.PoolTokenResponse> call = apiService.getUserToken(request);

        int maxRequests = expireSeconds / interval;
        final int[] requestCount = new int[1];
        requestCount[0] = 1;

        getAuthCodesubscription =call.observeOn(AndroidSchedulers.mainThread())
                                     .subscribeOn(Schedulers.newThread())
                                     .retryWhen(errors -> errors.flatMap(error -> {

                                            //retry if not max requests
                                            if (requestCount[0] < maxRequests) {

                                                requestCount[0]++;
                                                return Observable.timer(interval, TimeUnit.SECONDS);
                                            }

                                            // For anything else, don't retry
                                            return Observable.error(error);
                                        })
                                    ).subscribe(new Subscriber<TraktAPI.PoolTokenResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

                callBack.onFailure(e);
            }

            @Override
            public void onNext(TraktAPI.PoolTokenResponse result) {

                loadTraktProfile(result,callBack);

            }
        });

    }

    @Override
    public void refreshUserLists(ICallBack<Void> callBack) {

        TraktUserSettings settings = realm.where(TraktUserSettings.class).findFirst();

        ServerManager.setTraktUserId(settings.getAccessToken());
        TraktAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAKT).getRetrofit().create(TraktAPI.class);

        Observable<List<TraktUserList>> call = apiService.getUserLists(settings.getUsername());

        getAuthCodesubscription = call.observeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<TraktUserList>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Utils.logRetrofitError(e);

                        callBack.onFailure(e);
                    }

                    @Override
                    public void onNext(List<TraktUserList> result) {

                        //this works on a different tread so we need a new realm instance
                        Realm auxRealm = Realm.getDefaultInstance();

                        auxRealm.executeTransaction(e ->
                        {
                            TraktUserSettings settings = auxRealm.where(TraktUserSettings.class).findFirst();
                            settings.setSelectedList(0);
                            settings.getLists().deleteAllFromRealm();

                            for (TraktUserList listItem:result) {

                                listItem.setId(listItem.getIds().getTrakt());
                                settings.getLists().add(listItem);
                            }

                        });

                        callBack.onSuccess(null);

                    }
                });

    }

    @Override
    public void refreshToken(ICallBack<Void> callBack) {

        TraktUserSettings settings = realm.where(TraktUserSettings.class).findFirst();

        if (settings != null){

            TraktAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAKT).getRetrofit().create(TraktAPI.class);

            TraktAPI.TokenRefreshRequest request = new TraktAPI.TokenRefreshRequest();
            request.clientId = TrailerDen.TRAKT_API_KEY;
            request.clientSecret = TrailerDen.TRAKT_API_SECRET;
            request.refreshToken = settings.getRefreshToken();


            Observable<TraktAPI.TokenRefreshResponse> call = apiService.getRefreshToken(request);

            getAuthCodesubscription = call.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Subscriber<TraktAPI.TokenRefreshResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                            Utils.logRetrofitError(e);

                            callBack.onFailure(e);
                        }

                        @Override
                        public void onNext(TraktAPI.TokenRefreshResponse result) {

                            ServerManager.setTraktUserId(result.accessToken);

                            realm.executeTransaction(e ->
                            {
                                TraktUserSettings settings = realm.where(TraktUserSettings.class).findFirst();
                                settings.setRefreshToken(result.refreshToken);
                                settings.setAccessToken(result.accessToken);

                            });

                            callBack.onSuccess(null);

                        }
                    });

        }
        else
         callBack.onSuccess(null);

    }

    @Override
    public void clearTraktData() {

        TraktUserSettings current = getSavedTraktData();

        if(current != null)
        {
            realm.executeTransaction(e ->
            {
                current.deleteFromRealm();
            });
        }

    }

    @Override
    public TraktUserSettings getSavedTraktData() {

        return realm.where(TraktUserSettings.class).findFirst();
    }

    @Override
    public RealmResults<TraktUserSettings> getTrakSyncing(IChangeObserved observer) {

        RealmResults<TraktUserSettings> result = realm.where(TraktUserSettings.class).findAll();

        if (result != null && observer != null)
        {
            result.addChangeListener(new RealmChangeListener<RealmResults<TraktUserSettings>>() {
                @Override
                public void onChange(RealmResults<TraktUserSettings> element) {
                    observer.onChange();
                }
            });

        }

        return result;


    }

    @Override
    public RealmResults<TraktUserList> getTraktLists(IChangeObserved observer) {

        RealmResults<TraktUserList> result = realm.where(TraktUserList.class).findAll();

        if (result != null && observer != null)
        {
            result.addChangeListener(new RealmChangeListener<RealmResults<TraktUserList>>() {
                @Override
                public void onChange(RealmResults<TraktUserList> element) {
                    observer.onChange();
                }
            });

        }

        return result;

    }

    @Override
    public void updateTraktUser(TraktUserSettings settings, int selectedList) {

        realm.executeTransaction(e ->
        {
            settings.setSelectedList(selectedList);
        });
    }

    @Override
    public void addItemToList(String imdbId,ICallBack<Void> callBack) {

        refreshToken(new ICallBack<Void>() {
            @Override
            public void onFailure(Throwable error) {
                callBack.onFailure(error);
            }

            @Override
            public void onSuccess(Void arg) {

                TraktAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAKT).getRetrofit().create(TraktAPI.class);

                TraktUserSettings settings = getSavedTraktData();

                TraktAPI.ListItemRequest request = new TraktAPI.ListItemRequest();
                request.movies = new ArrayList<TraktAPI.TraktItem>();
                TraktAPI.TraktItem item = new TraktAPI.TraktItem();
                item.ids = new TraktAPI.TraktId();
                item.ids.imdb = imdbId;
                request.movies.add(item);

                Observable<Void> call = apiService.addItemtoList(settings.getUsername(),settings.getSelectedList(),request);

                mCompositeSubscription.add(call.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.newThread())
                        .subscribe(new Subscriber<Void>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                                Utils.logRetrofitError(e);

                                callBack.onFailure(e);
                            }

                            @Override
                            public void onNext(Void result) {

                                callBack.onSuccess(null);

                            }
                        }));


            }
        });

    }

    @Override
    public void removeItemFromList(String imdbId, ICallBack<Void> callBack) {

        refreshToken(new ICallBack<Void>() {
            @Override
            public void onFailure(Throwable error) {
                callBack.onFailure(error);
            }

            @Override
            public void onSuccess(Void arg) {

                TraktAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAKT).getRetrofit().create(TraktAPI.class);

                TraktUserSettings settings = getSavedTraktData();

                TraktAPI.ListItemRequest request = new TraktAPI.ListItemRequest();
                request.movies = new ArrayList<TraktAPI.TraktItem>();
                TraktAPI.TraktItem item = new TraktAPI.TraktItem();
                item.ids = new TraktAPI.TraktId();
                item.ids.imdb = imdbId;
                request.movies.add(item);

                Observable<Void> call = apiService.removeItemFromList(settings.getUsername(),settings.getSelectedList(),request);

                mCompositeSubscription.add(call.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.newThread())
                        .subscribe(new Subscriber<Void>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                                Utils.logRetrofitError(e);

                                callBack.onFailure(e);
                            }

                            @Override
                            public void onNext(Void result) {

                                callBack.onSuccess(null);

                            }
                        }));


            }
        });

    }

    private void loadTraktProfile(TraktAPI.PoolTokenResponse response, ICallBack<Void> callBack){

        ServerManager.setTraktUserId(response.accessToken);
        TraktAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAKT).getRetrofit().create(TraktAPI.class);

        Observable<TraktAPI.SettingsResponse> call = apiService.getUserSettings();

        getAuthCodesubscription = call.observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.newThread())
                                    .subscribe(new Subscriber<TraktAPI.SettingsResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Utils.logRetrofitError(e);

                        callBack.onFailure(e);
                    }

                    @Override
                    public void onNext(TraktAPI.SettingsResponse result) {

                        result.user.setId(1);
                        result.user.setAccessToken(response.accessToken);
                        result.user.setRefreshToken(response.refreshToken);

                        realm.executeTransaction(e ->
                        {

                            realm.copyToRealmOrUpdate(result.user);

                        });

                        refreshUserLists(callBack);
                    }
                });

    }

    @Override
    public void clearResources()
    {
        super.clearResources();
        cancelUserAuthToken();
    }
}

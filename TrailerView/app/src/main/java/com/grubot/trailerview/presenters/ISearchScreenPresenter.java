package com.grubot.trailerview.presenters;

import android.widget.EditText;

import com.grubot.trailerview.entities.Movie;

/**
 * Created by dave on 25-02-2016.
 */
public interface ISearchScreenPresenter
{
    void bindSearch(EditText searchText);

    //activities and fragments must call this to clear the observable subscripions from rxjava
    void destroySubscription();

    void setSearchFilters(boolean searchMovies, boolean searchGames, boolean searchShows);

}

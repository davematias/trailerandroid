package com.grubot.trailerview.presenters;

import android.widget.EditText;
import android.widget.TextView;

import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.GameCollection;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.ShowCollection;
import com.grubot.trailerview.entities.TraktUserSettings;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.models.IMoviesModel;
import com.grubot.trailerview.models.ITrailersModel;
import com.grubot.trailerview.models.ITraktModel;
import com.grubot.trailerview.models.MoviesModel;
import com.grubot.trailerview.models.TrailersModel;
import com.grubot.trailerview.models.TraktModel;
import com.grubot.trailerview.ui.fragments.IHomeScreenView;
import com.grubot.trailerview.ui.fragments.ISearchView;
import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.HttpException;
import retrofit.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 25-02-2016.
 */
public class SearchScreenPresenter extends BasePresenter implements ISearchScreenPresenter
{
    private ISearchView view;

    public SearchScreenPresenter(ISearchView view)
    {
        this.view = view;
    }

    private boolean searchMovies = true, searchShows = true, searchGames = true;

    private int pageCount = 10;

    @Override
    public void bindSearch(EditText searchText)
    {
        mCompositeSubscription.add(RxTextView.textChanges(searchText)
                .observeOn(Schedulers.io())
                .startWith("")
                .debounce(150, TimeUnit.MILLISECONDS)
                .map(CharSequence::toString)
                .switchMap(new Func1<String, Observable<List<IVideoData>>>() {
                    @Override
                    public Observable<List<IVideoData>> call(String query) {

                        return Observable.zip(
                                gamesModel.doSearchGameObservable(query,pageCount,!searchGames),
                                moviesModel.doSearchMovieObservable(query,pageCount,!searchMovies),
                                showsModel.doSearchShowObservable(query,pageCount,!searchShows),
                                (gameCol, moviesCol, showsCol) -> assembleResults(gameCol, moviesCol, showsCol))
                                .subscribeOn(Schedulers.io()).onErrorResumeNext(throwable ->
                                {
                                    //view.handleSearchError(throwable);
                                    //return Observable.<List<IVideoData>>empty();
                                    return Observable.<List<IVideoData>>just(new ArrayList<>());
                                });


                        /* return moviesModel.doSearchMovieObservable(query).subscribeOn(Schedulers.io()).onErrorResumeNext(throwable ->
                        {
                            view.handleSearchError(throwable);
                            return Observable.<MovieCollection>empty();
                        });
                        */

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<IVideoData>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        view.handleSearchError(e);
                    }

                    @Override
                    public void onNext(List<IVideoData> res) {

                        view.handleSearchSuccess(res);

                    }
                }));
    }

    private List<IVideoData> assembleResults(GameCollection gameCol, MovieCollection movieCol, ShowCollection showCol)
    {
        List<IVideoData> data = new ArrayList<>();

        data.addAll(gameCol.getResults());
        data.addAll(movieCol.getResults());
        data.addAll(showCol.getResults());

        return data;
    }

    @Override
    public void destroySubscription() {

        super.dispose();

    }

    @Override
    public void setSearchFilters(boolean searchMovies, boolean searchGames, boolean searchShows) {

        int sel = 0;

        this.searchGames = searchGames;

        if(searchGames)
            sel++;

        this.searchMovies = searchMovies;

        if(searchMovies)
            sel++;

        this.searchShows = searchShows;

        if(searchShows)
            sel++;

        switch (sel)
        {
            case 1: pageCount = 30;
                break;
            case 2: pageCount = 15;
                break;
            default: pageCount = 10;
                break;
        }

    }
}

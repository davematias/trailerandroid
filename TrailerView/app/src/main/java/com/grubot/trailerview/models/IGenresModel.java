package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.Trailer;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by dave on 25-02-2016.
 */
public interface IGenresModel
{
    void syncGenres(ICallBack<Void> callBack);

    RealmResults<Genre> getGenres(IChangeObserved observer,List<String> selectedTypes);

    void updateGenre(Genre genre, boolean filterOnTrailer);

    void updateGenres(boolean filterOnTrailer);

    void clearGenresNotUsed(List<String> selectedTypes);

    void clearResources();

}

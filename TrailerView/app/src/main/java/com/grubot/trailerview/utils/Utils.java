package com.grubot.trailerview.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.TrailerCollection;
import com.grubot.trailerview.network.ErrorsAPI;
import com.grubot.trailerview.network.ServerManager;
import com.grubot.trailerview.network.TrailersAPI;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.SerialSubscription;

/**
 * Created by dave on 07-04-2016.
 */
public class Utils
{
    private static SerialSubscription tempSub = new SerialSubscription();

    public static String convertTimestampToDate(String timeStamp)
    {
        try {

            long timeS = Long.parseLong(timeStamp);

            Date date = new Date(timeS*1000); // *1000 is to convert seconds to milliseconds
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()); // the format of your date
            //sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating

            return sdf.format(date);

        }catch (Exception ex)
        {
            return timeStamp;
        }
    }

    public static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(date);
        return cal;
    }

    public static void logRetrofitError(Throwable e)
    {
        if (e instanceof HttpException) {
            HttpException response = (HttpException) e;

            try {
                System.out.println(response.response().errorBody().string());
            } catch (IOException err) {
                err.printStackTrace();
            }

        }
    }

    public static boolean hasConnection(Context ctx)
    {
        ConnectivityManager cm =
                (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting());

    }

    public static void reportTrailerError(String id, String type)
    {
        ErrorsAPI api = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(ErrorsAPI.class);

        ErrorsAPI.ReportErrorRequest request = new ErrorsAPI.ReportErrorRequest();
        request.id = id;
        request.type = type;

        Observable<Void> callT = api.reportError(request);

        tempSub.set(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Utils.logRetrofitError(e);

            }

            @Override
            public void onNext(Void result) {

            }
        }));
    }



//    public static void share(Context context, String title, String trailer) {
//        Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
//        sendIntent.setType("text/plain");
//        List<ResolveInfo> activitiesAux = context.getPackageManager().queryIntentActivities(sendIntent, 0);
//        List<ResolveInfo> activities = new ArrayList<>();
//
//        for (ResolveInfo ri : activitiesAux) {
//            if(ri.activityInfo.name.equals("com.google.android.libraries.social.gateway.GatewayActivity"))
//                activities.add(ri);
//            if(ri.activityInfo.name.equals("com.facebook.composer.shareintent.ImplicitShareIntentHandlerDefaultAlias"))
//                activities.add(ri);
//            if(ri.activityInfo.name.equals("com.twitter.android.composer.ComposerActivity"))
//                activities.add(ri);
//        }
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle(((Activity) context).getResources().getText(R.string.share_action_title));
//        final ShareIntentListAdapter adapter = new ShareIntentListAdapter((Activity)context, R.layout.share_list_item, activities.toArray());
//        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                ResolveInfo info = (ResolveInfo) adapter.getItem(which);
//                if (info.activityInfo.packageName.contains("com.facebook.katana"))
//                {
//                    try {
//
////                    ShareLinkContent content = new ShareLinkContent.Builder()
////                            .setContentUrl(Uri.parse(trailer))
////                            .setContentTitle(title)
////                            .build();
////
////                    ShareDialog.show(((Activity) context), content);
//
//                        FacebookSdk.sdkInitialize(getApplicationContext());
//                        callbackManager = CallbackManager.Factory.create();
//                        shareDialog = new ShareDialog(this);
//
//                        if (ShareDialog.canShow(ShareLinkContent.class)) {
//                            ShareLinkContent content = new ShareLinkContent.Builder()
//                            .setContentUrl(Uri.parse(trailer))
//                            .setContentTitle(title)
//                            .build();
//
//                            ShareDialog shareDialog = new ShareDialog(VideoActivity.activity);
//
//                            shareDialog.show(content);
//                        }
//
//                    }catch(Exception e)
//                    {
//                        Log.d("fb","fb");
//                    }
//
//                } else
//                {
//                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//                    intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
//                    intent.setType("text/plain");
//
//                    intent.putExtra(Intent.EXTRA_TEXT, TextUtils.htmlEncode(title + "\n" + trailer + "\n\n" + "https://play.google.com/store/apps/details?id=com.mgd.excuse.excuseshuffler"));
//                    context.startActivity(intent);
//                }
//            }
//        });
//        builder.create().show();
//
//    }
}

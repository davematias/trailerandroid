package com.grubot.trailerview.entities;

import java.util.ArrayList;
import java.util.List;

public class GameCollection {

    private long count;
    private List<Game> results = new ArrayList<Game>();

    /**
     * No args constructor for use in serialization
     *
     */
    public GameCollection() {
    }

    /**
     *
     * @param results
     * @param count
     */
    public GameCollection(long count, List<Game> results) {
        this.count = count;
        this.results = results;
    }

    /**
     *
     * @return
     * The count
     */
    public long getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The results
     */
    public List<Game> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<Game> results) {
        this.results = results;
    }

}

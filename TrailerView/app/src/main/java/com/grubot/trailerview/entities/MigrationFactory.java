package com.grubot.trailerview.entities;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by david.matias on 08/11/2016.
 */

public class MigrationFactory {

  public RealmMigration getMigration(){

      // Example migration adding a new class
      RealmMigration migration = new RealmMigration() {
          @Override
          public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

              // DynamicRealm exposes an editable schema
              RealmSchema schema = realm.getSchema();

              if (oldVersion == 0) {

                  schema.get("Trailer").addField("parentSlug", String.class);

                  schema.get("Show").addField("slug", String.class);

                  schema.get("Movie").addField("slug", String.class);

                  schema.get("Game").addField("slug", String.class);

                  oldVersion++;
              }
          }
      };

      return migration;
  }

}

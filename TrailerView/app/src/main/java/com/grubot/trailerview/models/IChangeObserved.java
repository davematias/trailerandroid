package com.grubot.trailerview.models;

/**
 * Created by dave on 11-03-2016.
 */
public interface IChangeObserved
{
    void onChange();
}

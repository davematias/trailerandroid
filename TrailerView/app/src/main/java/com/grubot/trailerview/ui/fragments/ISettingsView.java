package com.grubot.trailerview.ui.fragments;

/**
 * Created by dave on 01-04-2016.
 */
public interface ISettingsView {

    void handleSignIn(String userName,boolean isFacebook);

    void handleSignOut();

    void syncUser();

    void handleError(String msg);

    void handleTraktError();

    void handleTraktListSyncError();

    void handleTraktSuccess();

    void requestTraktLogin(String url, String code);
}

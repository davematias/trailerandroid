package com.grubot.trailerview.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.utils.CustomTabsActivityHelper;
import com.grubot.trailerview.utils.CustomTabsHelper;
import com.grubot.trailerview.utils.UINotifications;
import com.grubot.trailerview.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dave on 19-04-2016.
 */
public class RatingDialog extends DialogFragment {

    @Bind(R.id.btnRatingYes)
    RelativeLayout btnRatingYes;

    @Bind(R.id.btnRatingNo)
    RelativeLayout btnRatingNo;

    @Bind(R.id.btnRatingNever)
    RelativeLayout btnRatingNever;

//    private static CoordinatorLayout clContainer;
    private static RatingDialog frag;
    private static Activity activity;

    public RatingDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static RatingDialog newInstance(CoordinatorLayout clContainer,Activity activity) {
        RatingDialog.frag = new RatingDialog();
        RatingDialog.activity = activity;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.rating_dialog, container,true);
        ButterKnife.bind(this, v);

        btnRatingYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TrailerDen.getInstance().saveBooleanPreference("showRatingDialog",true);
                activity.finish();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.grubot.trailerview"));
                startActivity(intent);
            }
        });

        btnRatingNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TrailerDen.getInstance().saveIntPreference("appUsesCounter", 0);
                RatingDialog.frag.dismiss();
                activity.finish();
            }
        });

        btnRatingNever.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TrailerDen.getInstance().saveBooleanPreference("showRatingDialog",true);
                RatingDialog.frag.dismiss();
                activity.finish();
            }
        });

        return v;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface iFace)
    {
        super.onDismiss(iFace);
    }

//    private Session createSessionObject() {
//        Properties properties = new Properties();
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.smtp.host", "smtp.gmail.com");
//        properties.put("mail.smtp.port", "587");
//
//        return Session.getInstance(properties, new javax.mail.Authenticator() {
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(TrailerDen.ERRORS_MAIL, TrailerDen.ERRORS_MAIL_PASS);
//            }
//        });
//    }
//
//    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
//        Message message = new MimeMessage(session);
//        message.setFrom(new InternetAddress(TrailerDen.ERRORS_MAIL));
//        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
//        message.setSubject(subject);
//        message.setText(messageBody);
//        return message;
//    }

//    private void sendMail(String email, String subject, String messageBody) {
//            Observable<String> fetchFromAmazon = Observable.create((Observable.OnSubscribe<String>) subscriber -> {
//                try {
//                    Session session = createSessionObject();
//                    Message message = createMessage(email, subject, messageBody, session);
//                    Transport.send(message);
//
//                }catch(Exception ex){
//                    subscriber.onError(ex); // In case there are network errors
//                }
//            });
//
//            fetchFromAmazon.subscribeOn(Schedulers.newThread()) // Create a new Thread
//                    .observeOn(AndroidSchedulers.mainThread()) // Use the UI thread
//                    .subscribe((Action1<String>) link -> {
//                    });
//
//    }
}

package com.grubot.trailerview.models;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmAsyncTask;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 11-03-2016.
 */
public abstract class BaseModel
{
    protected Realm realm;
    protected List<RealmAsyncTask> asyncTransactions;
    protected CompositeSubscription mCompositeSubscription;

    public BaseModel(CompositeSubscription mCompositeSubscription)
    {
        this.mCompositeSubscription = mCompositeSubscription;
        this.realm = Realm.getDefaultInstance();
        asyncTransactions = new ArrayList<>();
    }

    public void clearResources() {

        for (RealmAsyncTask task:asyncTransactions)
        {
            if(!task.isCancelled())
                task.cancel();
        }

        realm.removeAllChangeListeners();

        closeDB();

    }

    public void closeDB()
    {
        realm.close();
    }
}

package com.grubot.trailerview.presenters;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Configuration;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.ui.activities.ISplashScreenView;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by david.matias on 10/11/2016.
 */

public class SplashPresenter extends BasePresenter implements ISplashPresenter {

    private ISplashScreenView view;

    public SplashPresenter(ISplashScreenView view)
    {
        this.view = view;
    }



}

package com.grubot.trailerview.utils;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

/**
 * Created by dave on 10-02-2016.
 */
public class UINotifications
{
    public static Snackbar showAsSnackbar(CoordinatorLayout coordinatorLayout, String message, String actionText, View.OnClickListener onclick)
    {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE);

        if(onclick != null)
            snackbar.setAction(actionText,onclick);

        snackbar.show();

        return snackbar;
    }

    public static Snackbar showAsSnackbar(CoordinatorLayout coordinatorLayout, String message, int lenght)
    {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, lenght);

        snackbar.show();

        return snackbar;
    }

    public static void showAsToast(Context ctx, String message)
    {
        Toast.makeText(ctx,message,Toast.LENGTH_LONG).show();
    }
}

package com.grubot.trailerview.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.ui.activities.VideoActivity;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by david.matias on 30/05/2016.
 */
public class TrailerAdapter extends BaseAdapter {

    private Context mContext;
    private List<Trailer> mContent;

    public TrailerAdapter(Context context, List<Trailer> content) {
        mContext = context;
        mContent = content;
    }

    /*********************************************************************/
    /**************************** BaseAdapter ****************************/
    /*********************************************************************/

    @Override
    public int getCount() {
        if (mContent == null) {
            return 0;
        } else {
            return mContent.size();
        }
    }

    @Override
    public Trailer getItem(int position) {
        return mContent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mContent.get(position).getId();
    }

    public void appendItems(List<Trailer> trailers)
    {
        this.mContent.addAll(trailers);
    }

    public List<Trailer> getTrailers() {
        return mContent;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.trailer_list_item, null, false);
            viewHolder = new ViewHolder();
            viewHolder.imv = ButterKnife.findById(view, R.id.imPoster);
            //viewHolder.adView = ButterKnife.findById(view, R.id.nativeAd);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Trailer trailer = mContent.get(position);
        //if(trailer.getTitle().equals("Advertisement"))
        //{
            //viewHolder.imv.setVisibility(View.GONE);
            //viewHolder.adView.setVisibility(View.VISIBLE);

            //if(trailer.getId() == 1)
             //   populateContentAdView(TrailerDen.nativeAd1, viewHolder.adView);

           // if(trailer.getId() == 2)
          //      populateContentAdView(TrailerDen.nativeAd2, viewHolder.adView);
        //}
        //else
        //{
            viewHolder.imv.setVisibility(View.VISIBLE);
            //viewHolder.adView.setVisibility(View.GONE);

            Picasso.with(mContext)
                    .load(trailer.getPosterSm())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.imv);

        //}

        return view;
    }

   /* private void showVideo(Trailer currentTrailer, View poster)
    {
        Intent intent = new Intent(mContext, VideoActivity.class);
        intent.putExtra(VideoActivity.ACTIVITY_BUNDLE_TRAILER_KEY, Parcels.wrap(currentTrailer));


        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation((Activity)mContext, poster, "poster");

        mContext.startActivity(intent,options.toBundle());
    }
*/

    /*
    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setAdvertiserView(adView.findViewById(R.id.contentad_advertiser));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((TextView) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

        List<NativeAd.Image> images = nativeContentAd.getImages();

        if (images.size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(images.get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.
        NativeAd.Image logoImage = nativeContentAd.getLogo();

        if (logoImage == null) {
            adView.getLogoView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(logoImage.getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }
*/
    private static final class ViewHolder {
        private static ImageView imv;
        //private static NativeContentAdView adView;
    }

}


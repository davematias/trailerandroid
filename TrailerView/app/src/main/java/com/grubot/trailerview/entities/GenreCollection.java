package com.grubot.trailerview.entities;

import com.grubot.trailerview.entities.Genre;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenreCollection
{
    private long count;
    private List<Genre> results = new ArrayList<Genre>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public GenreCollection() {
    }

    /**
     *
     * @param results
     * @param count
     */
    public GenreCollection(long count, List<Genre> results) {
        this.count = count;
        this.results = results;
    }

    /**
     *
     * @return
     * The count
     */
    public long getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The results
     */
    public List<Genre> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<Genre> results) {
        this.results = results;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}

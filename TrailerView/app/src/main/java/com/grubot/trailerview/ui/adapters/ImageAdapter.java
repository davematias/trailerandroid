package com.grubot.trailerview.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.grubot.trailerview.R;
import com.grubot.trailerview.entities.Platform;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sérgio on 14/06/2016.
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private List<Platform> platforms;

    public ImageAdapter(Context c,List<Platform> platforms) {
        mContext = c;
        this.platforms = platforms;
    }

    public int getCount() {
        return platforms.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
//        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imageView.setPadding(8, 8, 8, 8);
//        } else {
//            imageView = (ImageView) convertView;
//        }

        try {
            Picasso.with(mContext).load(mContext.getResources().getIdentifier(platforms.get(position).getName().toLowerCase().replace(" ",""), "drawable", mContext.getPackageName())).into(imageView);
        }catch (Exception e)
        {
            Picasso.with(mContext).load(mContext.getResources().getIdentifier("xbox360", "drawable", mContext.getPackageName())).into(imageView);
        }

//        imageView.setImageResource(mThumbIds.get(platforms.get(position)));
//        imageView.setImageResource(mContext.getResources().getIdentifier(platforms.get(position), "drawable", mContext.getPackageName()));
        return imageView;
    }
}

package com.grubot.trailerview.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import com.grubot.trailerview.R;

/**
 * Created by Sérgio on 08/06/2016.
 */
public class CustomTabsActivityHelper {

    private static CustomTabsClient mClient;
    private static CustomTabsServiceConnection mConnection;
    private static CustomTabsSession mCustomTabsSession;
    private static CustomTabsIntent customTabsIntent = null;

    public static void bindCustomTabsService(Context context) {
        mConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                mClient= customTabsClient;
                mClient.warmup(0L);
                mCustomTabsSession = mClient.newSession(null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mClient = null;
            }
        };

        CustomTabsClient.bindCustomTabsService(context, "com.android.chrome", mConnection);

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder(mCustomTabsSession);
        builder.setToolbarColor(context.getResources().getColor(R.color.colorPrimary)).setShowTitle(true);
        builder.setStartAnimations(context, R.anim.slide_in_right, R.anim.slide_out_left);
        builder.setExitAnimations(context, R.anim.slide_in_left, R.anim.slide_out_right);
        customTabsIntent = builder.build();
    }

    public static void unbindCustomTabsService(Activity activity) {

        try {

            if (mConnection == null) return;
            activity.unbindService(mConnection);
            mClient = null;
            mCustomTabsSession = null;
            mConnection = null;

        }catch (Exception e)
        {
        }


    }

    public static void launchUrl(String url, Activity activity, CoordinatorLayout coordinatorLayout)
    {
        try {

        String packageName = CustomTabsHelper.getPackageNameToUse(activity);

        //If we cant find a package name, it means theres no browser that supports
        //Chrome Custom Tabs installed. So, we fallback to the webview
        if (packageName == null) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } else {
            customTabsIntent.intent.setPackage(packageName);
            customTabsIntent.launchUrl(activity, Uri.parse(url));
        }
        }catch(Exception e)
        {
            UINotifications.showAsSnackbar(coordinatorLayout,activity.getResources().getString(R.string.url_open_failed), Snackbar.LENGTH_LONG);
        }
    }
}

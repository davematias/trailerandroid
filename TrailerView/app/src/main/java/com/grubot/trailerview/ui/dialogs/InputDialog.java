package com.grubot.trailerview.ui.dialogs;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.grubot.trailerview.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dave on 19-04-2016.
 */
public class InputDialog extends DialogFragment implements TextView.OnEditorActionListener {

    private String tag;

    private String inputText;

    @Bind(R.id.etInput)
    EditText etInput;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    public InputDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static InputDialog newInstance(String title, String msg, String tag) {
        InputDialog frag = new InputDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("placeholder", msg);
        args.putString("tag", tag);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.input_dialog, container);
        ButterKnife.bind(this, v);

        String title = getArguments().getString("title", "");
        String placeholder = getArguments().getString("placeholder", "");
        tag = getArguments().getString("tag", "");

        etInput.setOnEditorActionListener(this);
        etInput.setHint(placeholder);

        tvTitle.setText(title);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Show soft keyboard automatically and request focus to field
        etInput.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);


    }

    @Override
    public void onDismiss(DialogInterface iFace)
    {
        returnValue(inputText);
        super.onDismiss(iFace);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        if (EditorInfo.IME_ACTION_DONE == actionId) {

            inputText = v.getText().toString();

            dismiss();

            return true;
        }
        return false;

    }

    public void returnValue(String result)
    {
        // Return input text back to activity through the implemented listener
        IDialogListener listener = (IDialogListener) getTargetFragment();
        listener.onFinishDialog(tag, result);
    }
}

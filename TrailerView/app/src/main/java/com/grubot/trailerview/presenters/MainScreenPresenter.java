package com.grubot.trailerview.presenters;

import android.text.TextUtils;

import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.Configuration;
import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.GenresModel;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.models.IChangeObserved;
import com.grubot.trailerview.models.IGenresModel;
import com.grubot.trailerview.ui.activities.IMainScreenView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dave on 25-02-2016.
 */
public class MainScreenPresenter extends BasePresenter implements IMainScreenPresenter {

    private IMainScreenView view;
    private IGenresModel genres;

    public MainScreenPresenter(IMainScreenView view)
    {
        this.view = view;
        genres = new GenresModel(mCompositeSubscription);

    }

    @Override
    public RealmResults<Genre> getGenres(IChangeObserved observer)
    {
        List<String> selectedTypes = new ArrayList<>();

        String types = TrailerDen.getInstance().getStringPreference(TrailerDen.PREFERENCE_TRAILER_TYPE_SELECTION);

        if(types != null && !types.isEmpty())
            selectedTypes = Arrays.asList(TextUtils.split(types,","));

        return genres.getGenres(observer,selectedTypes);
    }

    @Override
    public void syncGenres() {

        long lastSync = TrailerDen.getInstance().getLongPreference(TrailerDen.PREFERENCE_SYNCDATE);
        configModel.syncConfiguration(TrailerDen.PREFERENCE_SYNCDATE, new ICallBack<Configuration>() {
            @Override
            public void onFailure(Throwable error) {

                view.handleSyncError();
            }

            @Override
            public void onSuccess(Configuration arg) {

                long currSync = Long.parseLong(arg.getValue());

                RealmResults<Genre> res = getGenres(null);

                if(currSync == lastSync && res.size() > 0)
                    view.handleSyncSuccess();
                else
                {
                    genres.syncGenres(new ICallBack<Void>() {
                        @Override
                        public void onFailure(Throwable error) {

                            view.handleSyncError();
                        }

                        @Override
                        public void onSuccess(Void arg) {

                            view.handleSyncSuccess();
                        }
                    });
                }

                TrailerDen.getInstance().saveLongPreference(TrailerDen.PREFERENCE_SYNCDATE,currSync);
            }
        });
    }

    @Override
    public void updateGenres(boolean filterOnTrailer) {

        genres.updateGenres(filterOnTrailer);

    }

    @Override
    public void updateGenre(Genre genre, boolean filterOnTrailer)
    {
        genres.updateGenre(genre,filterOnTrailer);
    }

    @Override
    public User getCurrentUser() {
        return usersModel.getCurrentUserData();
    }

    @Override
    public User copyOrUpdateCurrentUser(User userData) {
        return usersModel.createOrUpdateUser(userData);
    }

    @Override
    public RealmResults<User> getUserSyncing(IChangeObserved observer) {
        return usersModel.getUserSyncing(observer);
    }

    @Override
    public void loadTrailer(long trailerId) {

        long lastSync = TrailerDen.getInstance().getLongPreference(TrailerDen.PREFERENCE_SYNCDATE);
        configModel.syncConfiguration(TrailerDen.PREFERENCE_SYNCDATE, new ICallBack<Configuration>() {
            @Override
            public void onFailure(Throwable error) {

                loadTrailer(trailerId, false);
            }

            @Override
            public void onSuccess(Configuration arg) {

                long currSync = Long.parseLong(arg.getValue());

                if(currSync == lastSync)
                    loadTrailer(trailerId, true);
                else
                    loadTrailer(trailerId, false);

                TrailerDen.getInstance().saveLongPreference(TrailerDen.PREFERENCE_SYNCDATE,currSync);
            }
        });
    }

    private void loadTrailer(long id, boolean useCache){

        Trailer t = trailersModel.getTrailerById(id);

        if(t != null && useCache)
            view.handleTrailerData(t);
        else
        {
            trailersModel.getTrailerById(id, new ICallBack<Trailer>() {
                @Override
                public void onFailure(Throwable error) {
                    view.handleAppLinkLoadError(error);
                }

                @Override
                public void onSuccess(Trailer arg) {
                    view.handleTrailerData(arg);
                }
            });
        }
    }

    @Override
    public void loadVideoData(String slug, TrailerDen.VIDEO_TYPE contexType) {

        long lastSync = TrailerDen.getInstance().getLongPreference(TrailerDen.PREFERENCE_SYNCDATE);
        configModel.syncConfiguration(TrailerDen.PREFERENCE_SYNCDATE, new ICallBack<Configuration>() {
            @Override
            public void onFailure(Throwable error) {

                loadFromContext(slug, contexType,false);
            }

            @Override
            public void onSuccess(Configuration arg) {

                long currSync = Long.parseLong(arg.getValue());

                if(currSync == lastSync)
                    loadFromContext(slug, contexType,true);
                else
                    loadFromContext(slug, contexType,false);

                TrailerDen.getInstance().saveLongPreference(TrailerDen.PREFERENCE_SYNCDATE,currSync);
            }
        });

    }

    private void loadFromContext(String slug, TrailerDen.VIDEO_TYPE contexType, boolean useCache)
    {
        if(contexType == TrailerDen.VIDEO_TYPE.MOVIE)
            loadMovie(slug, useCache);
        else if(contexType == TrailerDen.VIDEO_TYPE.SHOW)
            loadShow(slug, useCache);
        else if(contexType == TrailerDen.VIDEO_TYPE.GAME)
            loadGame(slug, useCache);
    }

    private void loadGame(String slug, boolean useCache){

        Game game = gamesModel.getGameBySlugFromDB(slug);

        if(game != null && useCache)
        {
            view.handleVideoData(game);
        }
        else
        {
            mCompositeSubscription.add(Observable.zip(
                    gamesModel.getGameBySlugObservable(slug),
                    trailersModel.getTrailersByParentSlugDObservable(slug),
                    (resCol, trailerCol) -> assembleGameData(resCol, trailerCol))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Subscriber<Game>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            view.handleAppLinkLoadError(e);
                        }

                        @Override
                        public void onNext(Game result) {

                            if(game != null)
                                result.setFavorite(game.isFavorite());

                            for (Trailer trailer: result.getTrailers()) {
                                Trailer cachedTrailer = trailersModel.getTrailerById(trailer.getId());

                                if(cachedTrailer != null)
                                {
                                    trailer.setLiked(cachedTrailer.getLiked());
                                    trailer.setTrailerListCache(cachedTrailer.isTrailerListCache());
                                }

                            }

                            Game realmGame = gamesModel.copyOrUpdate(result);
                            view.handleVideoData(realmGame);
                        }
                    }));
        }
    }

    private void loadShow(String slug, boolean useCache){
        Show sow = showsModel.getShowBySlugFromDB(slug);

        if(sow != null && useCache)
        {
            view.handleVideoData(sow);
        }
        else
        {
            mCompositeSubscription.add(Observable.zip(
                    showsModel.getShowBySlugObservable(slug),
                    trailersModel.getTrailersByParentSlugDObservable(slug),
                    (resCol, trailerCol) -> assembleShowData(resCol, trailerCol))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Subscriber<Show>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            view.handleAppLinkLoadError(e);
                        }

                        @Override
                        public void onNext(Show result) {

                            if(sow != null)
                                result.setFavorite(sow.isFavorite());

                            for (Trailer trailer: result.getTrailers()) {
                                Trailer cachedTrailer = trailersModel.getTrailerById(trailer.getId());

                                if(cachedTrailer != null)
                                {
                                    trailer.setLiked(cachedTrailer.getLiked());
                                    trailer.setTrailerListCache(cachedTrailer.isTrailerListCache());
                                }

                            }

                            Show realmShow = showsModel.copyOrUpdate(result);
                            view.handleVideoData(realmShow);
                        }
                    }));
        }
    }

    private void loadMovie(String slug, boolean useCache){
        Movie mov = moviesModel.getMovieBySlugFromDB(slug);

        if(mov != null && useCache)
        {
            view.handleVideoData(mov);
        }
        else
        {
            mCompositeSubscription.add(Observable.zip(
                    moviesModel.getMovieBySlugObservable(slug),
                    trailersModel.getTrailersByParentSlugDObservable(slug),
                    (movieCol, trailerCol) -> assembleMovieData(movieCol, trailerCol))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Subscriber<Movie>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            view.handleAppLinkLoadError(e);
                        }

                        @Override
                        public void onNext(Movie result) {

                            if(mov != null)
                                result.setIsFavorite(mov.isFavorite());

                            for (Trailer trailer: result.getTrailers()) {
                                Trailer cachedTrailer = trailersModel.getTrailerById(trailer.getId());

                                if(cachedTrailer != null)
                                {
                                    trailer.setLiked(cachedTrailer.getLiked());
                                    trailer.setTrailerListCache(cachedTrailer.isTrailerListCache());
                                }

                            }

                            Movie realmMovie = moviesModel.copyOrUpdate(result);
                            view.handleVideoData(realmMovie);
                        }
                    }));
        }
    }

    @Override
    public void destroySubscription() {
        super.dispose();
        genres.clearResources();
    }
}

package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.MovieCollection;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by dave on 08-01-2016.
 */
public interface MoviesAPI
{
    @GET("/movies/")
    Observable<MovieCollection> getMovies(@Query("ordering") String sort);

    @GET("/movies/")
    Observable<MovieCollection> filterMovies(@Query("ordering") String sort, @Query("language") String language, @Query("country") String country, @Query("imdbID") String imdbID, @Query("slug") String slug);

    @GET("/movies/")
    Observable<MovieCollection> searchMovie(@Query("search") String search,@Query("pageSize") int pageSize);

    @GET("/movies/")
    Observable<MovieCollection> searchMovieByGenre(@Query("ordering") String sort, @Query("searchgenre") String genre);

    @GET("/movies/{id}")
    Observable<Movie> getMovie(@Path("id") long id);
}

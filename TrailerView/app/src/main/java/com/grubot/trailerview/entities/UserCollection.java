package com.grubot.trailerview.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dave on 13-04-2016.
 */
public class UserCollection {

    private long count;
    private List<User> results = new ArrayList<User>();

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<User> getResults() {
        return results;
    }

    public void setResults(List<User> results) {
        this.results = results;
    }
}

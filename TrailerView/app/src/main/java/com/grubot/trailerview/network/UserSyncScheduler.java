package com.grubot.trailerview.network;

import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.presenters.BasePresenter;
import com.grubot.trailerview.presenters.IBasePresenter;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by dave on 19-04-2016.
 */
public class UserSyncScheduler {

    private static ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(15);
    private static ScheduledFuture<?> t;

    private UserSyncScheduler(){}

    static class SyncUserTask implements Runnable {

        private IBasePresenter presenter = new BasePresenter();

        public void run() {

            try {

//// TODO: 09/06/2016 realm state error here, fix to use realm on this thread
                User currentUser = presenter.loadCurrentUser();

                if(currentUser == null)
                {
                    t.cancel(true);
                    presenter.dispose();
                }
                else
                {
                    if(!currentUser.isSyncing())
                    {
                        presenter.setUserSyncing();

                        presenter.syncRemoteAndDb(currentUser, new ICallBack<Void>() {
                            @Override
                            public void onFailure(Throwable error) {
                                presenter.setSyncError();
                            }

                            @Override
                            public void onSuccess(Void arg) {
                                presenter.setSyncDone();
                            }
                        });
                    }

                }

            }catch (Exception ex){
                ex.printStackTrace();
            }                    ;

        }
    }

    public static void scheduleSync()
    {
        if(t == null)
            t = executor.scheduleAtFixedRate(new SyncUserTask(), 15, 15, TimeUnit.MINUTES);
    }

    public static void cancelSync()
    {
        if(t != null)
            t.cancel(true);
    }

}

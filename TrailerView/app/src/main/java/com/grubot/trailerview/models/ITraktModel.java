package com.grubot.trailerview.models;


import com.grubot.trailerview.entities.TraktUserList;
import com.grubot.trailerview.entities.TraktUserSettings;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.network.TraktAPI;

import io.realm.RealmResults;

/**
 * Created by dave on 22-04-2016.
 */
public interface ITraktModel {

    public enum LIST_ITEM_KIND{
        MOVIES,
        SHOWS
    }

    void getAccessToken(ICallBack<TraktAPI.AuthCodeResponse> callBack);

    void cancelUserAuthToken();

    void pollForUserData(int interval, int expireSeconds, String deviceCode, ICallBack<Void> callBack);

    void refreshUserLists(ICallBack<Void> callBack);

    void refreshToken(ICallBack<Void> callBack);

    void clearTraktData();

    TraktUserSettings getSavedTraktData();

    RealmResults<TraktUserSettings> getTrakSyncing(IChangeObserved observer);

    RealmResults<TraktUserList> getTraktLists(IChangeObserved observer);

    void updateTraktUser(TraktUserSettings settings, int selectedList);

    void addItemToList(String imdbId, ICallBack<Void> callBack);

    void removeItemFromList(String imdbId, ICallBack<Void> callBack);
}

package com.grubot.trailerview.presenters;

import com.grubot.trailerview.entities.Game;
import com.grubot.trailerview.entities.IVideoData;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.models.ICallBack;
import com.grubot.trailerview.models.IChangeObserved;
import com.grubot.trailerview.network.UserSyncScheduler;
import com.grubot.trailerview.ui.fragments.IFavoritesView;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by dave on 16-03-2016.
 */
public class FavoritesPresenter extends BasePresenter implements IFavoritesPresenter {

    private IFavoritesView view;

    private RealmResults<Movie> movies;

    private RealmResults<Show> shows;

    private RealmResults<Game> games;

    public FavoritesPresenter(IFavoritesView view)
    {
        this.view = view;
    }

    @Override
    public List<IVideoData> getFavorites(IChangeObserved observer) {

        List<IVideoData> result = new ArrayList<IVideoData>();

        movies = moviesModel.getFavorites(observer);

        shows = showsModel.getFavorites(observer);

        games = gamesModel.getFavorites(observer);

        for (Movie mov: movies) {
            result.add(mov);
        }

        for (Show sho: shows) {
            result.add(sho);
        }

        for (Game gam: games) {
            result.add(gam);
        }

        return result;

    }

    @Override
    public void destroySubscription() {

        super.dispose();

    }

    @Override
    public void syncUserData() {

        User currentUser = usersModel.getCurrentUserData();

        if(currentUser != null && !currentUser.isSyncing())
        {
            usersModel.setSyncing();

            usersModel.syncUserData(new ICallBack<User>() {
                @Override
                public void onFailure(Throwable error) {

                    usersModel.setSyncError();

                }

                @Override
                public void onSuccess(User arg) {

                    syncRemoteAndDb(arg, new ICallBack<Void>() {
                        @Override
                        public void onFailure(Throwable error) {

                            usersModel.setSyncError();

                        }

                        @Override
                        public void onSuccess(Void arg) {

                            usersModel.setSyncDone();


                        }
                    });

                }
            });
        }
    }


}

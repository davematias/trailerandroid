package com.grubot.trailerview.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dave on 10-03-2016.
 */
public class ConfigurationCollection {

    private long count;

    private List<Configuration> results = new ArrayList<Configuration>();

    /**
     * No args constructor for use in serialization
     *
     */
    public ConfigurationCollection() {
    }

    /**
     *
     * @param results
     * @param count
     */
    public ConfigurationCollection(long count, List<Configuration> results) {
        this.count = count;
        this.results = results;
    }

    /**
     *
     * @return
     * The count
     */
    public long getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The results
     */
    public List<Configuration> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<Configuration> results) {
        this.results = results;
    }

}

package com.grubot.trailerview.ui.customviews;

/**
 * Created by Dave on 30/05/2016.
 */
public interface IStackObserver {

    void stackNavigatedNext(int itemIndex);

    void stackNavigatedPrevious(int itemIndex);

}

package com.grubot.trailerview.utils;

import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by david.matias on 14/07/2016.
 */
public class LinearLayoutWeightAnimationWrapper {

    private View view;

    public LinearLayoutWeightAnimationWrapper(View view) {
        if (view.getLayoutParams() instanceof LinearLayout.LayoutParams) {
            this.view = view;
        } else {
            throw new IllegalArgumentException("The view should have LinearLayout as parent");
        }
    }

    public void setWeight(float weight) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.weight = weight;
        view.setLayoutParams(params);
    }

    public float getWeight() {
        return ((LinearLayout.LayoutParams) view.getLayoutParams()).weight;
    }

}

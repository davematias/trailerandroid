package com.grubot.trailerview.ui.fragments;

import com.grubot.trailerview.entities.IVideoData;

import java.util.List;

/**
 * Created by dave on 25-02-2016.
 */
public interface ISearchView
{
    void handleFavoriteError(Throwable error);

    void handleSearchError(Throwable error);

    void handleSearchSuccess(List<IVideoData> result);

    void showLoadingInterface();
}

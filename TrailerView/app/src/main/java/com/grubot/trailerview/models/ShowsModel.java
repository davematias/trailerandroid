package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.MovieCollection;
import com.grubot.trailerview.entities.Season;
import com.grubot.trailerview.entities.Show;
import com.grubot.trailerview.entities.ShowCollection;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.network.ServerManager;
import com.grubot.trailerview.network.ShowsAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 25-02-2016.
 */
public class ShowsModel extends BaseModel implements IShowsModel
{
    public ShowsModel(CompositeSubscription mCompositeSubscription)
    {
        super(mCompositeSubscription);
    }

    @Override
    public Show getShowByExternalIDFromDB(String id) {
        return realm.where(Show.class).equalTo("imdbID", id).notEqualTo("cacheDirty",true).findFirst();
    }

    @Override
    public void updateShow(Show show, boolean isFavorite, boolean isCacheDirty) {
        realm.executeTransaction(e ->
        {
            show.setFavorite(isFavorite);
            show.setFavoriteDateMillis(System.currentTimeMillis());

            show.setCacheDirty(isCacheDirty);
        });
    }

    @Override
    public RealmResults<Show> getFavorites(IChangeObserved observer) {

        RealmResults<Show> shows = realm.where(Show.class).equalTo("isFavorite",true).findAll();

        if (observer != null)
        {
            // Tell Realm to notify our listener when the customers results
            // have changed (items added, removed, updated, anything of the sort).
            shows.addChangeListener(new RealmChangeListener<RealmResults<Show>>() {
                @Override
                public void onChange(RealmResults<Show> element) {
                    observer.onChange();
                }
            });
        }

        return shows;
    }

    @Override
    public Show copyOrUpdate(Show show) {

        final Show[] result = new Show[1];

        realm.executeTransaction(e ->
        {
            result[0] = realm.copyToRealmOrUpdate(show);

            for (String localGenre: show.getString_genres()) {

                Genre gen = realm.where(Genre.class).equalTo("name",localGenre.toUpperCase()).findFirst();

                if(gen != null)
                    result[0].getGenresList().add(gen);
            }

            for (String localSeason: show.getStrings_seasons()) {

                Season ses = realm.where(Season.class).equalTo("id",localSeason).findFirst();

                if(ses == null){

                    ses = new Season();
                    ses.setId(localSeason);

                    String[] components = localSeason.split(":");
                    ses.setName(components[0]);
                    ses.setExternalId(components[1]);
                }

                result[0].getSeasonsList().add(ses);
            }

            for (String localRating: show.getStrings_ratings()) {

                String[] rating = localRating.split(":");

                if(rating.length == 2)
                {
                    com.grubot.trailerview.entities.Rating rat = new com.grubot.trailerview.entities.Rating();
                    rat.setName(rating[0]);
                    rat.setRating(rating[1]);
                    rat.setGuid(UUID.randomUUID().toString());

                    result[0].getRatingList().add(rat);
                }
            }

        });

        return result[0];

    }

    @Override
    public void addToShow(Show show, Trailer trailer) {

        if(show.getTrailers().contains(trailer))
            return;

        realm.executeTransaction(e ->
        {
            show.getTrailers().add(trailer);
        });

    }

    @Override
    public RealmResults<Show> getUnfavorited() {
        return realm.where(Show.class).equalTo("isFavorite",false).greaterThan("favoriteDateMillis",0).findAll();
    }

    @Override
    public Observable<ShowCollection> getShowByImdbIDObservable(String param) {
        ShowsAPI showService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(ShowsAPI.class);
        return showService.filterShows(null,null,null,param,null);
    }

    @Override
    public Observable<ShowCollection> doSearchShowObservable(String param, int pageCount, boolean returnEmpty) {

        if(returnEmpty)
            return Observable.<ShowCollection>just(new ShowCollection(0, new ArrayList<Show>()));

        ShowsAPI showService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(ShowsAPI.class);
        return showService.searchShows(param,pageCount);
    }

    @Override
    public Observable<ShowCollection> getShowBySlugObservable(String param) {
        ShowsAPI showService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(ShowsAPI.class);
        return showService.filterShows(null,null,null,null,param);
    }

    @Override
    public Show getShowBySlugFromDB(String slug) {
        return realm.where(Show.class).equalTo("slug",slug).findFirst();
    }

    @Override
    public Observable<Show> getShowByIdObservable(Long id) {
        ShowsAPI showService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(ShowsAPI.class);
        return showService.getShow(id);
    }

    @Override
    public Show getShowByIdFromDb(Long id) {
        return realm.where(Show.class).equalTo("id",id).findFirst();
    }

    @Override
    public List<Observable<Show>> getShowsByIdObservableList(List<String> ids) {
        List<Observable<Show>> getRequests = new ArrayList<>();

        for (String id: ids) {

            getRequests.add(getShowByIdObservable(Long.parseLong(id)));
        }

        return getRequests;
    }

}

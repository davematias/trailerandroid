package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.User;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by dave on 07-04-2016.
 */
public interface IUsersModel
{
    User createOrUpdateUser(User currentUser);

    User getCurrentUserData();

    RealmResults<User> getUserSyncing(IChangeObserved observer);

    void clearUserData();

    void clearResources();

    void syncUserData(ICallBack<User> callBack);

    void setSyncError();

    void setSyncing();

    void setSyncDone();

    void addCachedTrailerData(List<String> cachedLikes,List<String> cachedDislikes,List<String> cachedViews);

    void addFavoriteMovieData(List<String> cachedMovies);

    void sendSyncedState(List<String> cachedLikes,List<String> cachedDislikes,List<String> cachedViews,List<String> favoriteMovies, ICallBack<Void> callBack);

}

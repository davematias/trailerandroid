package com.grubot.trailerview.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import com.grubot.trailerview.TrailerDen;

/**
 * Created by dave on 15-02-2016.
 */
public class Animations
{
    public static int mShortAnimationDuration = TrailerDen.getInstance().getResources().getInteger(
            android.R.integer.config_shortAnimTime);

    public static int mMediumAnimationDuration = TrailerDen.getInstance().getResources().getInteger(
            android.R.integer.config_mediumAnimTime);

    public static int mLongAnimationDuration = TrailerDen.getInstance().getResources().getInteger(
            android.R.integer.config_longAnimTime);

    public static void crossFade(View toVisible, View toInvisible)
    {
        crossFade(toVisible,toInvisible,mShortAnimationDuration,null,null);
    }

    public static ViewPropertyAnimator[] crossFade(View toVisible, View toInvisible, int speed,IAnimationEnd toVisibleCallback, IAnimationEnd toInvisibleCallback)
    {
        toVisible.setTag("V");
        toInvisible.setTag("I");

        toVisible.setAlpha(0f);
        toVisible.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        ViewPropertyAnimator anime = toVisible.animate()
                .alpha(1f)
                .setDuration(speed)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                        if(toVisibleCallback != null)
                            toVisibleCallback.onEnd();

                    }
                });

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        ViewPropertyAnimator anime2 = toInvisible.animate()
                .alpha(0f)
                .setDuration(speed)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                        if(toInvisible.getTag() != null && toInvisible.getTag().toString().equals("I"))
                            toInvisible.setVisibility(View.GONE);

                        if(toInvisibleCallback != null)
                            toInvisibleCallback.onEnd();

                    }
                });

        ViewPropertyAnimator[] result = new ViewPropertyAnimator[2];
        result[0] = anime;
        result[1] = anime2;

        return result;
    }

    public static void expand(final View v,IAnimationEnd callback) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };


        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                        if(callback != null)
                            callback.onEnd();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        a.setDuration(mMediumAnimationDuration);
        v.startAnimation(a);
    }

    public static void collapse(final View v,IAnimationEnd callback) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if(callback != null)
                    callback.onEnd();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        a.setDuration(mMediumAnimationDuration);
        v.startAnimation(a);
    }

    public interface IAnimationEnd
    {
        void onEnd();
    }

    public class LinearLayoutWeightAnimationWrapper {

        private View view;

        public LinearLayoutWeightAnimationWrapper(View view) {
            if (view.getLayoutParams() instanceof LinearLayout.LayoutParams) {
                this.view = view;
            } else {
                throw new IllegalArgumentException("The view should have LinearLayout as parent");
            }
        }

        public void setWeight(float weight) {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
            params.weight = weight;
            view.setLayoutParams(params);
        }

        public float getWeight() {
            return ((LinearLayout.LayoutParams) view.getLayoutParams()).weight;
        }
    }
}

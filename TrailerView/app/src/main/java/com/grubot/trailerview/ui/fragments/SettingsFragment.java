package com.grubot.trailerview.ui.fragments;


import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.grubot.trailerview.R;
import com.grubot.trailerview.TrailerDen;
import com.grubot.trailerview.entities.TraktUserList;
import com.grubot.trailerview.entities.TraktUserSettings;
import com.grubot.trailerview.models.IChangeObserved;
import com.grubot.trailerview.presenters.ISettingsPresenter;
import com.grubot.trailerview.presenters.SettingsPresenter;
import com.grubot.trailerview.ui.activities.IMainScreenView;
import com.grubot.trailerview.ui.dialogs.ConfirmDialog;
import com.grubot.trailerview.ui.dialogs.IDialogListener;
import com.grubot.trailerview.ui.dialogs.ProgressDialog;
import com.grubot.trailerview.utils.UINotifications;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements ISettingsView, IDialogListener {

    @Bind(R.id.switchTrakt)
    Switch switchTrakt;

    @Bind(R.id.switchNotifications)
    Switch switchNotifications;

    //@Bind(R.id.sign_in_button)
    //SignInButton signInButton;

    //@Bind(R.id.fblogin_button)
    //LoginButton fbLoginButton;

    @Bind(R.id.tvUserName)
    TextView tvUser;

    @Bind(R.id.btnLogout)
    ImageButton btnLogout;

    @Bind(R.id.rlLogout)
    RelativeLayout rlLogout;

    @Bind(R.id.clContainer)
    CoordinatorLayout clContainer;

    @Bind(R.id.spLists)
    Spinner spLists;

    @Bind(R.id.btnRefresh)
    Button btnRefresh;

    @Bind(R.id.rlTraktLists)
    RelativeLayout rlTraktLists;

    @Bind(R.id.tvLists)
    TextView tvLists;

    private static final String TAG = "SettingsFragment";

    private static final String TRAKT_DIALOG_TAG = "TraktDialog";
    private static final String TRAKT_PROGRESS_DIALOG_TAG = "TraktProgressDialog";
    private static final String TRAKT_PROGRESS_DIALOG_PREPARE_TAG = "TraktProgressDialogPrepare";

    private String currentAuthUrl = "";

    private ISettingsPresenter presenter;

    private CallbackManager callbackManager;

   // private AccessTokenTracker accessTokenTracker;

    private ProgressDialog loadDialogFragment;

    private RealmResults<TraktUserList> traktLists;

    private RealmResults<TraktUserSettings> traktSettings;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new SettingsPresenter(this);

        //FacebookSdk.sdkInitialize(getActivity());

        //callbackManager = CallbackManager.Factory.create();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);

        switchNotifications.setChecked(TrailerDen.getInstance().getBooleanPreference(TrailerDen.PREFERENCE_SHOWNOTIFICATIONS,true));
        switchNotifications.setOnClickListener(e -> changeNotificationsPreferences(switchNotifications.isChecked()));

        //loadTraktProfile();

        spLists.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {

                    if (traktSettings.size() > 0 && traktLists.size() > 0)
                        presenter.updateTraktUser(traktSettings.first(),traktLists.get(position).getId());

                }catch (Exception ex){ex.printStackTrace();}


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnRefresh.setOnClickListener(e -> presenter.refreshUserList());

        //signInButton.setScopes(((MainActivity) getActivity()).getGso().getScopeArray());
        //signInButton.setSize(SignInButton.SIZE_WIDE);

        //signInButton.setOnClickListener(e -> ((IMainScreenView) getActivity()).GoogleSignIn());

        btnLogout.setOnClickListener(e -> ((IMainScreenView) getActivity()).GoogleSignOut());

        //fbLoginButton.setFragment(this);
        //fbLoginButton.setReadPermissions(Arrays.asList("public_profile,user_birthday"));

        // Callback registration
        /*fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

               // Profile profile = Profile.getCurrentProfile();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                User user = new User();
                                user.setLocalId(1);

                                String name = "";

                                try {

                                    String id = object.getString("id");
                                    name = object.getString("name");

                                    String birthday = object.getString("birthday"); // 01/31/1980 format
                                    String gender = object.getString("gender");
                                    String location = object.getString("locale");

                                    user.setName(name);
                                    user.setExternalId(id);
                                    user.setExternalIdType("1");
                                    user.setGender(gender.toUpperCase().substring(0,1));
                                    user.setLocation(location);

                                    if(birthday != null)
                                    {
                                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                                        try {
                                            Date bdate = format.parse(birthday);
                                            user.setAge(Utils.getDiffYears(bdate,new Date()));

                                        } catch (Exception e) {

                                            e.printStackTrace();
                                        }
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                try {

                                    presenter.createOrUpdateUser(user);

                                }catch (Exception e) {
                                    e.printStackTrace();
                                }

                                handleSignIn(name,true);
                                syncUser();
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,locale");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                handleError(exception.getLocalizedMessage());
            }
        });

        */

        /*accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                       AccessToken currentAccessToken) {

                if (currentAccessToken == null)
                {
                  handleSignOut();
                }
            }
        };*/

        /*User u = presenter.loadCurrentUser();

        if(u != null){

            boolean isFacebook = (u.getExternalIdType().equals("1"));

            handleSignIn(u.getName(),isFacebook);
        }
*/
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        //accessTokenTracker.stopTracking();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void changeNotificationsPreferences(boolean allowNotifications)
    {
        if(allowNotifications)
        {
            TrailerDen.getInstance().saveBooleanPreference(TrailerDen.PREFERENCE_SHOWNOTIFICATIONS,true);
        }
        else
        {
            TrailerDen.getInstance().saveBooleanPreference(TrailerDen.PREFERENCE_SHOWNOTIFICATIONS,false);
        }
    }

    private void loadTraktProfile()
    {
        //monitor user settings changes
        traktSettings = presenter.getTrakSyncing(new IChangeObserved() {
            @Override
            public void onChange() {

                try {

                    if(loadDialogFragment != null)
                    {
                        loadDialogFragment.setResultVal(true);
                        loadDialogFragment.dismiss();
                        loadDialogFragment = null;
                    }

                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }



            }
        });

        if(traktSettings.size() > 0)
            rlTraktLists.setVisibility(View.VISIBLE);

        switchTrakt.setChecked(traktSettings.size() > 0);
        switchTrakt.setOnClickListener(e -> handleTraktSwitch(switchTrakt.isChecked()));

        //monitor user lists changes
        traktLists = presenter.getTraktLists(new IChangeObserved() {
            @Override
            public void onChange() {
                checkTraktLists();
            }
        });

        checkTraktLists();
    }

    private void checkTraktLists()
    {
        List<String> lists = new ArrayList<String>();
        List<Integer> listIds = new ArrayList<Integer>();
        ArrayAdapter<String> listNames = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, lists);

        if(traktLists.size() > 0)
        {
            for (TraktUserList traktList: traktLists) {
                lists.add(traktList.getName());
                listIds.add(traktList.getId());
            }
        }

        spLists.setAdapter(listNames);

        if(traktSettings.size() > 0)
        {
            if (traktSettings.first().getSelectedList() > 0)
            {
                int idx = listIds.indexOf(traktSettings.first().getSelectedList());
                spLists.setSelection(idx);
            }
        }

    }

    @Override
    public void handleSignIn(String userName,boolean isFacebook) {

        rlLogout.setVisibility(View.VISIBLE);
        tvUser.setText(userName);
        tvUser.setVisibility(View.VISIBLE);

        if(isFacebook){
            btnLogout.setVisibility(View.GONE);
        }
        else{
            btnLogout.setVisibility(View.VISIBLE);
            //fbLoginButton.setVisibility(View.GONE);
        }

        //signInButton.setVisibility(View.GONE);

    }

    @Override
    public void handleSignOut() {

        //signInButton.setVisibility(View.VISIBLE);
        //fbLoginButton.setVisibility(View.VISIBLE);
        rlLogout.setVisibility(View.GONE);
        tvUser.setVisibility(View.GONE);
        presenter.destroyCurrentUser();

    }

    @Override
    public void syncUser() {
        presenter.syncUserData();
    }

    @Override
    public void handleError(String msg) {
        UINotifications.showAsSnackbar(clContainer,msg, Snackbar.LENGTH_LONG);
    }

    @Override
    public void handleTraktError() {

        if(loadDialogFragment != null)
        {
            loadDialogFragment.setResultVal(false);
            loadDialogFragment.dismiss();
        }

        handleError(getString(R.string.settingsTraktDialogLoadError));
    }

    @Override
    public void handleTraktListSyncError() {

    }

    @Override
    public void handleTraktSuccess() {
        loadDialogFragment.setResultVal(true);
        loadDialogFragment.dismiss();
    }

    @Override
    public void requestTraktLogin(String url, String code) {

        currentAuthUrl = url;

        loadDialogFragment.setResultVal(true);
        loadDialogFragment.dismiss();

        ConfirmDialog traktDialogFragment = ConfirmDialog.newInstance(getString(R.string.settingsTraktDialogTitle), getString(R.string.settingsTraktDialogMsg1) + code, getString(R.string.settingsTraktDialogMsg2), TRAKT_DIALOG_TAG);
        openDialog(traktDialogFragment,"trakt_dialog");

    }

    private void handleTraktSwitch(boolean syncTrakt) {

        if(syncTrakt)
        {
            loadDialogFragment = ProgressDialog.newInstance(getString(R.string.settingsTraktDialogLoadTitle), TRAKT_PROGRESS_DIALOG_PREPARE_TAG);
            openDialog(loadDialogFragment,"load_dialog");

            presenter.getTraktToken();
        }
        else
        {
            rlTraktLists.setVisibility(View.GONE);
            presenter.deleteTraktData();
        }
    }

    @Override
    public void onFinishDialog(String tag, Object result) {

        if(tag.equals(TRAKT_DIALOG_TAG))
        {
            if(((boolean)result)){

                loadDialogFragment = ProgressDialog.newInstance(getString(R.string.settingsTraktDialogTitle), TRAKT_PROGRESS_DIALOG_TAG);
                openDialog(loadDialogFragment,"load_dialog");

                //open browser
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(currentAuthUrl));
                startActivity(i);

                presenter.poolForToken();
            }
            else
                switchTrakt.setChecked(false);
        }
        else if(tag.equals(TRAKT_PROGRESS_DIALOG_TAG)){

            if(((boolean)result))
            {
                rlTraktLists.setVisibility(View.VISIBLE);
            }
            else{
                presenter.cancelTraktToken();
                switchTrakt.setChecked(false);
            }
        }
        else if(tag.equals(TRAKT_PROGRESS_DIALOG_PREPARE_TAG)){

            if(!((boolean)result))
            {
                presenter.cancelTraktToken();
                switchTrakt.setChecked(false);
            }
        }

    }

    private void openDialog(DialogFragment dialog, String dialogTag){

        try {

            FragmentManager fm = getFragmentManager();
            dialog.setTargetFragment(SettingsFragment.this, 300);
            dialog.show(fm, dialogTag);

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}

package com.grubot.trailerview.entities;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.SeasonRealmProxy;
import io.realm.ShowRealmProxy;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dave on 18/05/2016.
 */
@Parcel(implementations = { SeasonRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Season.class })
public class Season extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    private String externalId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}

package com.grubot.trailerview.models;

import com.grubot.trailerview.entities.Genre;
import com.grubot.trailerview.entities.Movie;
import com.grubot.trailerview.entities.Trailer;
import com.grubot.trailerview.entities.User;
import com.grubot.trailerview.entities.UserCollection;
import com.grubot.trailerview.entities.UserInteractedMovie;
import com.grubot.trailerview.entities.UserInteractedTrailer;
import com.grubot.trailerview.network.ServerManager;
import com.grubot.trailerview.network.TrailersAPI;
import com.grubot.trailerview.network.UsersAPI;
import com.grubot.trailerview.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dave on 07-04-2016.
 */
public class UsersModel extends  BaseModel implements IUsersModel {

    public UsersModel(CompositeSubscription mCompositeSubscription) {
        super(mCompositeSubscription);
    }

    @Override
    public User createOrUpdateUser(User currentUser) {

        final User[] result = new User[1];

        realm.executeTransaction(e ->
        {

            result[0] = realm.copyToRealmOrUpdate(currentUser);

        });

        return result[0];
    }

    @Override
    public User getCurrentUserData() {
        return realm.where(User.class).findFirst();
    }

    @Override
    public RealmResults<User> getUserSyncing(IChangeObserved observer) {


        RealmResults<User> users = realm.where(User.class).findAll();

        if (users != null && observer != null)
        {
            // Tell Realm to notify our listener when the customers results
            // have changed (items added, removed, updated, anything of the sort).
            users.addChangeListener(new RealmChangeListener<RealmResults<User>>() {
                @Override
                public void onChange(RealmResults<User> element) {
                    observer.onChange();
                }
            });

        }

        return users;

    }

    @Override
    public void clearUserData() {

        User current = getCurrentUserData();

        if(current != null)
        {
            realm.executeTransaction(e ->
            {
                current.deleteFromRealm();
            });
        }
    }

    @Override
    public void clearResources()
    {
        super.clearResources();

    }


    @Override
    public void syncUserData(ICallBack<User> callBack) {

       UsersAPI apiService = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(UsersAPI.class);

       User currentUser = getCurrentUserData();

       Observable<UserCollection> call = apiService.getList(currentUser.getExternalId());

       mCompositeSubscription.add(call.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<UserCollection>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                callBack.onFailure(e);
            }

            @Override
            public void onNext(UserCollection userResult) {

                if(userResult.getCount() > 0)
                {
                    User remoteUser = userResult.getResults().get(0);

                    User user = getCurrentUserData();

                    realm.executeTransaction(e ->
                    {
                        user.setId(remoteUser.getId());
                    });

                    callBack.onSuccess(remoteUser);
                }
                else
                {
                    Observable<User> callAdd = apiService.addData(gatherAllDataForSync());
                    mCompositeSubscription.add(callAdd.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<User>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                            Utils.logRetrofitError(e);

                            callBack.onFailure(e);
                        }

                        @Override
                        public void onNext(User userResult) {

                            User user = getCurrentUserData();

                            realm.executeTransaction(e ->
                            {
                                user.setId(userResult.getId());
                                user.setLastSyncDateMillis(System.currentTimeMillis());
                            });

                            callBack.onSuccess(null);
                        }
                    }));
                }

            }
        }));

    }

    private User gatherAllDataForSync()
    {
        User dbUser = getCurrentUserData();
        User localUser = realm.copyFromRealm(dbUser);

        realm.executeTransaction(e ->
        {

            RealmResults<Movie> favoriteMovies = realm.where(Movie.class).equalTo("isFavorite",true).findAll();

            RealmResults<Trailer> likedTrailers = realm.where(Trailer.class).equalTo("liked",true).findAll();

            RealmResults<Trailer> dislikedTrailers = realm.where(Trailer.class).equalTo("liked",false).findAll();

            RealmResults<Trailer> viewedTrailers = realm.where(Trailer.class).equalTo("viewed",false).findAll();

            localUser.setFavoriteMovies(new ArrayList<String>());
            localUser.setFavoriteGames(new ArrayList<String>());
            localUser.setFavoriteShows(new ArrayList<String>());
            localUser.setTrailersDisliked(new ArrayList<String>());
            localUser.setTrailersLiked(new ArrayList<String>());
            localUser.setTrailersViewed(new ArrayList<String>());

        /*        if(currentUser.getFavoriteGames().isEmpty())
                        currentUser.getFavoriteGames().add("0");

                    if(currentUser.getFavoriteShows().isEmpty())
                        currentUser.getFavoriteShows().add("0");
        */

            for (Movie fave: favoriteMovies) {
                localUser.getFavoriteMovies().add(String.valueOf(fave.getId()));

                UserInteractedMovie auxMov = new UserInteractedMovie();
                auxMov.setMovieId(fave.getId());

                dbUser.getCachedMovies().add(auxMov);
            }

        /*                  if(currentUser.getFavoriteMovies().isEmpty())
                       currentUser.getFavoriteMovies().add("0");
        */

            HashMap<Long,UserInteractedTrailer> trailerMap = new HashMap<Long, UserInteractedTrailer>();

            for (Trailer liked: likedTrailers) {
                localUser.getTrailersLiked().add(String.valueOf(liked.getId()));

                UserInteractedTrailer auxTrailer = getOrCreateUserTrailer(liked.getId(),trailerMap);

                auxTrailer.setLiked(true);
            }

                    /*if(currentUser.getTrailersLiked().isEmpty())
                       currentUser.getTrailersLiked().add("0");
        */

            for (Trailer disliked: dislikedTrailers) {
                localUser.getTrailersDisliked().add(String.valueOf(disliked.getId()));

                UserInteractedTrailer auxTrailer = getOrCreateUserTrailer(disliked.getId(),trailerMap);

                auxTrailer.setDisliked(true);
            }

            //if(currentUser.getTrailersDisliked().isEmpty())
            //currentUser.getTrailersDisliked().add("0");

            for (Trailer viewed: viewedTrailers) {

                localUser.getTrailersViewed().add(String.valueOf(viewed.getId()));

                UserInteractedTrailer auxTrailer = getOrCreateUserTrailer(viewed.getId(),trailerMap);

                auxTrailer.setViewed(true);
            }

            for (UserInteractedTrailer cacheTrailers: trailerMap.values()) {
                dbUser.getCachedTrailers().add(cacheTrailers);
            }

            //if(currentUser.getTrailersViewed().isEmpty())
            //currentUser.getTrailersViewed().add("0");


        });

        return localUser;
    }

    private UserInteractedTrailer getOrCreateUserTrailer(long id, HashMap<Long,UserInteractedTrailer> list)
    {
        if(list.containsKey(id))
            return list.get(id);
        else
        {
            UserInteractedTrailer trailer = new UserInteractedTrailer();
            trailer.setTrailerId(id);

            list.put(id,trailer);

            return trailer;
        }
    }

    @Override
    public void setSyncError() {

        User user = getCurrentUserData();

        if(user != null)
        {
            realm.executeTransaction(a ->
            {
                user.setSyncing(false);
                user.setHasSyncError(true);
            });
        }

    }

    @Override
    public void setSyncing() {
        User user = getCurrentUserData();

        if(user != null){
            realm.executeTransaction(a ->
            {
                user.setSyncing(true);
                user.setHasSyncError(false);
            });
        }

    }

    @Override
    public void setSyncDone() {

        User user = getCurrentUserData();

        if(user != null){
            realm.executeTransaction(e ->
            {
                user.setSyncing(false);
                user.setHasSyncError(false);
                user.setLastSyncDateMillis(System.currentTimeMillis());
            });
        }

    }

    @Override
    public void addCachedTrailerData(List<String> cachedLikes, List<String> cachedDislikes, List<String> cachedViews) {

        User dbUser = getCurrentUserData();

        HashMap<Long,UserInteractedTrailer> trailerMap = new HashMap<Long, UserInteractedTrailer>();

        for (String liked: cachedLikes) {

            UserInteractedTrailer auxTrailer = getOrCreateUserTrailer(Long.parseLong(liked),trailerMap);

            auxTrailer.setLiked(true);
        }

        for (String disliked: cachedLikes) {
            UserInteractedTrailer auxTrailer = getOrCreateUserTrailer(Long.parseLong(disliked),trailerMap);

            auxTrailer.setDisliked(true);
        }

        for (String viewed: cachedViews) {

            UserInteractedTrailer auxTrailer = getOrCreateUserTrailer(Long.parseLong(viewed),trailerMap);

            auxTrailer.setViewed(true);
        }

        realm.executeTransaction(e ->
        {
            dbUser.getCachedTrailers().deleteAllFromRealm();

            for (UserInteractedTrailer cacheTrailers: trailerMap.values()) {
                dbUser.getCachedTrailers().add(cacheTrailers);
            }

        });

    }

    @Override
    public void addFavoriteMovieData(List<String> cachedMovies) {

        List<UserInteractedMovie> favMovies = new ArrayList<>();

        for (String movie: cachedMovies) {

            UserInteractedMovie mov = new UserInteractedMovie();
            mov.setMovieId(Long.parseLong(movie));

            favMovies.add(mov);
        }

        User dbUser = getCurrentUserData();

        realm.executeTransaction(e ->
        {
            dbUser.getCachedMovies().deleteAllFromRealm();

            for (UserInteractedMovie cacheMovs: favMovies) {
                dbUser.getCachedMovies().add(cacheMovs);
            }

        });

    }

    @Override
    public void sendSyncedState(List<String> cachedLikes,List<String> cachedDislikes,List<String> cachedViews,List<String> favoriteMovies, ICallBack<Void> callBack){
        UsersAPI service = ServerManager.getInstance(ServerManager.MANAGER_TYPE.TRAILERS).getRetrofit().create(UsersAPI.class);

        UsersAPI.UserPatchRequest request = new UsersAPI.UserPatchRequest();

        request.favorite_movies = favoriteMovies;
        request.trailers_disliked = cachedDislikes;
        request.trailers_liked = cachedLikes;
        request.trailers_viewed = cachedViews;

        Observable<User> callT = service.updateData(getCurrentUserData().getId(), request);
        mCompositeSubscription.add(callT.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<User>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Utils.logRetrofitError(e);
                callBack.onFailure(e);
            }

            @Override
            public void onNext(User ueser) {

                callBack.onSuccess(null);
            }
        }));
    }
}

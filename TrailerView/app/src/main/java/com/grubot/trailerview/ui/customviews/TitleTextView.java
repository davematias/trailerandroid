package com.grubot.trailerview.ui.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by dave on 29-02-2016.
 */
public class TitleTextView extends TextView
{
    public TitleTextView(Context context, AttributeSet attrs)
    {
        super(context,attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/Bangers.ttf"));
    }
}
